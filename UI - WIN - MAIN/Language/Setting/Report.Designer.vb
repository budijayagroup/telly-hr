﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'     Runtime Version:4.0.30319.42000
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated.
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On

Imports System

Namespace My.Resources
    
    'This class was auto-generated by the StronglyTypedResourceBuilder
    'class via a tool like ResGen or Visual Studio.
    'To add or remove a member, edit your .ResX file then rerun ResGen
    'with the /str option, or rebuild your VS project.
    '''<summary>
    '''  A strongly-typed resource class, for looking up localized strings, etc.
    '''</summary>
    <Global.System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0"),  _
     Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),  _
     Global.System.Runtime.CompilerServices.CompilerGeneratedAttribute()>  _
    Public Class Report
        
        Private Shared resourceMan As Global.System.Resources.ResourceManager
        
        Private Shared resourceCulture As Global.System.Globalization.CultureInfo
        
        <Global.System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")>  _
        Friend Sub New()
            MyBase.New
        End Sub
        
        '''<summary>
        '''  Returns the cached ResourceManager instance used by this class.
        '''</summary>
        <Global.System.ComponentModel.EditorBrowsableAttribute(Global.System.ComponentModel.EditorBrowsableState.Advanced)>  _
        Public Shared ReadOnly Property ResourceManager() As Global.System.Resources.ResourceManager
            Get
                If Object.ReferenceEquals(resourceMan, Nothing) Then
                    Dim temp As Global.System.Resources.ResourceManager = New Global.System.Resources.ResourceManager("UI.WIN.MAIN.Report", GetType(Report).Assembly)
                    resourceMan = temp
                End If
                Return resourceMan
            End Get
        End Property
        
        '''<summary>
        '''  Overrides the current thread's CurrentUICulture property for all
        '''  resource lookups using this strongly typed resource class.
        '''</summary>
        <Global.System.ComponentModel.EditorBrowsableAttribute(Global.System.ComponentModel.EditorBrowsableState.Advanced)>  _
        Public Shared Property Culture() As Global.System.Globalization.CultureInfo
            Get
                Return resourceCulture
            End Get
            Set
                resourceCulture = value
            End Set
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Date.
        '''</summary>
        Public Shared ReadOnly Property FILTER_DATE() As String
            Get
                Return ResourceManager.GetString("FILTER_DATE", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to From Date.
        '''</summary>
        Public Shared ReadOnly Property FILTER_DATEFROM() As String
            Get
                Return ResourceManager.GetString("FILTER_DATEFROM", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to To Date.
        '''</summary>
        Public Shared ReadOnly Property FILTER_DATETO() As String
            Get
                Return ResourceManager.GetString("FILTER_DATETO", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Current Stock.
        '''</summary>
        Public Shared ReadOnly Property FILTER_ITEM_TYPE_01() As String
            Get
                Return ResourceManager.GetString("FILTER_ITEM_TYPE_01", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to .
        '''</summary>
        Public Shared ReadOnly Property FILTER_ITEM_TYPE_02() As String
            Get
                Return ResourceManager.GetString("FILTER_ITEM_TYPE_02", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Spo.
        '''</summary>
        Public Shared ReadOnly Property FILTER_KDSPO() As String
            Get
                Return ResourceManager.GetString("FILTER_KDSPO", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Month.
        '''</summary>
        Public Shared ReadOnly Property FILTER_MONTH() As String
            Get
                Return ResourceManager.GetString("FILTER_MONTH", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Rekap.
        '''</summary>
        Public Shared ReadOnly Property FILTER_STANDARDCOST_TYPE_01() As String
            Get
                Return ResourceManager.GetString("FILTER_STANDARDCOST_TYPE_01", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Type.
        '''</summary>
        Public Shared ReadOnly Property FILTER_TYPE() As String
            Get
                Return ResourceManager.GetString("FILTER_TYPE", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Master Detail.
        '''</summary>
        Public Shared ReadOnly Property FILTER_TYPE_01() As String
            Get
                Return ResourceManager.GetString("FILTER_TYPE_01", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Master Only.
        '''</summary>
        Public Shared ReadOnly Property FILTER_TYPE_02() As String
            Get
                Return ResourceManager.GetString("FILTER_TYPE_02", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Detail Only.
        '''</summary>
        Public Shared ReadOnly Property FILTER_TYPE_03() As String
            Get
                Return ResourceManager.GetString("FILTER_TYPE_03", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to All.
        '''</summary>
        Public Shared ReadOnly Property FILTER_TYPE_04() As String
            Get
                Return ResourceManager.GetString("FILTER_TYPE_04", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Year.
        '''</summary>
        Public Shared ReadOnly Property FILTER_YEAR() As String
            Get
                Return ResourceManager.GetString("FILTER_YEAR", resourceCulture)
            End Get
        End Property
        
        '''<summary>
        '''  Looks up a localized string similar to Page.
        '''</summary>
        Public Shared ReadOnly Property REPORT_PAGE() As String
            Get
                Return ResourceManager.GetString("REPORT_PAGE", resourceCulture)
            End Get
        End Property
    End Class
End Namespace
