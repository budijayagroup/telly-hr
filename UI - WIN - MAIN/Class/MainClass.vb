﻿Imports System.IO
Imports System.Configuration
Imports System.Text

Module MainClass
    Public Enum FORM_MODE
        FORM_MODE_VIEW = 0
        FORM_MODE_ADD = 1
        FORM_MODE_EDIT = 2
    End Enum

    Public sUserID As String = String.Empty

    Public sTerbilang As Decimal = 0

    Public sCode As String = String.Empty
    Public sStatusSave As String = String.Empty

    Public sStatusItem As String = String.Empty
    Public sItem_L1 As String = String.Empty
    Public sItem_L2 As String = String.Empty
    Public sItem_L3 As String = String.Empty
    Public sItem_L4 As String = String.Empty
    Public sItem_L5 As String = String.Empty
    Public sItem_L6 As String = String.Empty

    Public sVersion As Integer = 0

    Public sFind1 As String = String.Empty
    Public sFind2 As String = String.Empty

    Public sCompany As String = String.Empty
    Public sAddress As String = String.Empty
    Public sCity As String = String.Empty
    Public sPhone As String = String.Empty
    Public sFax As String = String.Empty
    Public sNPWP As String = String.Empty
    Public sEMAIL As String = String.Empty

    Public sKDSERVER As String = String.Empty
    Public sKDDATABASE As String = String.Empty
    Public sKDSERVER_TAX As String = String.Empty
    Public sKDDATABASE_TAX As String = String.Empty
    Public sKDCOMPANY As String = String.Empty
    Public sKDSite As String = String.Empty
    Public sNamaSite As String = String.Empty
    Public sWATERMARK As String = String.Empty

    Public sSavePayment As Boolean = False

#Region "Printing"
    Public Sub pLine_(ByRef oTxtStream As TextWriter, ByVal nSPACE As Integer, ByVal xcCHAR As String)
        Dim cLINE As String

        cLINE = Space(nSPACE)
        cLINE = Replace(cLINE, " ", xcCHAR)
        oTxtStream.WriteLine(cLINE)
    End Sub
    Public Function pSpace_(ByRef cText As String, ByVal nLength As Long, ByVal Alignment As Single) As String
        Dim cResult As String, nTextLength As Long

        nTextLength = Len(cText)
        cResult = cText
        If (nTextLength < nLength) Then
            cResult = cResult & Space(nLength - Len(cResult))
        ElseIf (nTextLength > nLength) Then
            cResult = Mid(cResult, 1, nLength)
            If Len(cResult) < nLength Then
                cResult = cResult & Space(nLength - Len(cResult))
            End If
        End If
        If (nTextLength < nLength) Then
            Select Case Alignment
                Case 1
                    cResult = (Space(nLength - nTextLength) + cText)
                Case 2
                    cResult = (Space((nLength - nTextLength) / 2) + cText)
                Case 3
                    cResult = (cText + Space(nLength - nTextLength))
            End Select
            cResult = cResult + Space(nLength - Len(cResult))
        End If
        pSpace_ = cResult
        pSpace_ = cResult
    End Function
#End Region

#Region "Encrypt / Decrypt"
    Private Const INT_lens As Integer = 1
    Public str As StringBuilder
    Public searchStr As String
    Public b As Integer = 6
    Public p() As Integer = {2, 4, 7, 9, 3, INT_lens}
    Public i As Integer
    Public j As Integer
    Public k As Integer
    Public c As Integer
    Public lens As Integer

    Public Function Encrypt(ByVal inputstr As String) As String
        str = New StringBuilder(inputstr)
        lens = str.Length
        While (lens < b) OrElse (lens Mod b)
            str.Append(" ")
            lens += INT_lens
        End While
        For i As Integer = 0 To ((lens / b) - INT_lens)
            For j As Integer = 0 To (b - INT_lens)
                k = p(j) + 100
                c = (6 * i + j)
                str.Replace(str.Chars(c), Chr(Asc(str.Chars(c)) + k), c, INT_lens)
            Next
        Next
        Return str.ToString
        str = Nothing
    End Function
    Public Function Decrypt(ByVal inputstr As String) As String

        str = New StringBuilder(inputstr)
        lens = str.Length
        While (lens < b) OrElse (lens Mod b)
            str.Append(" ")
            lens += INT_lens
        End While

        For i As Integer = 0 To ((lens / b) - INT_lens)
            For j As Integer = 0 To (b - INT_lens)
                k = p(j) + 100
                c = (6 * i + j)
                str.Replace(str.Chars(c), Chr(Asc(str.Chars(c)) - k), c, INT_lens)
            Next
        Next
        Return str.ToString
        str = Nothing
    End Function
#End Region
End Module
