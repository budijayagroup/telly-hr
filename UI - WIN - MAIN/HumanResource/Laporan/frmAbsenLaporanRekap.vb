Imports DataAccess
Imports UI.WIN.MAIN.My.Resources
Imports System.Linq

Imports DevExpress.XtraPrinting

Imports System.Data
Imports System.Data.SqlClient


Public Class frmAbsenLaporanRekap
    Implements ILanguage

#Region "Function"
    Private Sub Me_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Me.Text = "Absen - Rekap"

        lDATEFROM.Text = Report.FILTER_DATEFROM
        lDATETO.Text = Report.FILTER_DATETO

        cboTYPE.Properties.Items.Clear()
        cboTYPE.Properties.Items.Add("HARIAN")
        cboTYPE.Properties.Items.Add("BULANAN")
        cboTYPE.SelectedIndex = 0

        deDATEFrom.DateTime = Now.AddDays((-Now.Day) + 1)
        deDATETo.DateTime = Now
    End Sub
    Private Sub Me_FormClosed(sender As System.Object, e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        Try
            grv.SaveLayoutToRegistry("HKEY_CURRENT_USER\Software\BUDIJAYAGROUP\MAIN\" & sUserID & "\" & Me.Text)
        Catch oErr As Exception

        End Try
    End Sub
    Public Sub fn_LoadLanguage() Implements ILanguage.fn_LoadLanguage
        Try
            Me.Text = "Absen - Rekap"

            lDATEFROM.Text = Report.FILTER_DATEFROM
            lDATETO.Text = Report.FILTER_DATETO
        Catch oErr As Exception
            MsgBox(Statement.ErrorStatement & vbCrLf & oErr.Message, MsgBoxStyle.Exclamation, Me.Text)
        End Try
    End Sub
    Private Sub fn_LoadSecurity()
        Try
            Dim oOtority As New Setting.clsOtority
            Dim oUser As New Setting.clsUser

            Dim ds = (From x In oOtority.GetDataDetail
                      Join y In oUser.GetData
                     On x.KDOTORITY Equals y.KDOTORITY
                      Where x.MODUL = "ABSEN_R" _
                     And y.KDUSER = sUserID
                      Select x.ISADD, x.ISDELETE, x.ISUPDATE, x.ISPRINT, x.ISVIEW).FirstOrDefault

            Try
                picPrint.Enabled = ds.ISPRINT
                picRefresh.Enabled = ds.ISVIEW

                If ds.ISVIEW = True Then
                    fn_Preview()
                    fn_LoadLanguage()
                End If
            Catch oErr As Exception
                MsgBox(Statement.SecurityNotInstalled, MsgBoxStyle.Exclamation, Me.Text)

                picPrint.Enabled = False
                picRefresh.Enabled = False
            End Try
        Catch oErr As Exception
            MsgBox(Statement.SecurityNotInstalled, MsgBoxStyle.Exclamation, Me.Text)
        End Try
    End Sub
    Private Sub fn_Print()
        Try
            printableComponentLink.Landscape = False
            printableComponentLink.PaperKind = Printing.PaperKind.A4

            Dim phf As PageHeaderFooter = _
        TryCast(printableComponentLink.PageHeaderFooter, PageHeaderFooter)
            phf.Header.Content.Clear()
            phf.Header.Font = New Font("Times New Roman", 14, FontStyle.Bold)
            phf.Header.LineAlignment = BrickAlignment.Center
            phf.Footer.Font = New Font("Times New Roman", 9.75)
            phf.Footer.LineAlignment = BrickAlignment.Far
            phf.Footer.Content.AddRange(New String() _
        {sWATERMARK, "", Report.REPORT_PAGE & " : [Page # of Pages #]"})

            phf.Header.Content.AddRange(New String() _
{"", "Absen - Rekap" & vbCrLf & deDATEFrom.DateTime.ToString("dd/MM/yyyy") & " - " & deDATETo.DateTime.ToString("dd/MM/yyyy"), ""})

            printableComponentLink.Component = grd
            printableComponentLink.CreateDocument()
            printableComponentLink.ShowPreviewDialog(Me)
        Catch oErr As Exception
            MsgBox(Statement.ErrorStatement & vbCrLf & oErr.Message, MsgBoxStyle.Exclamation, Me.Text)
        End Try
    End Sub
    Private Sub fn_Preview()
        Try
            grv.Columns.Clear()
            grd.DataSource = Nothing
            grv.OptionsView.GroupFooterShowMode = DevExpress.XtraGrid.Views.Grid.GroupFooterShowMode.VisibleAlways
            grv1.OptionsView.GroupFooterShowMode = DevExpress.XtraGrid.Views.Grid.GroupFooterShowMode.VisibleAlways
            If cboTYPE.SelectedIndex = 0 Then
                fn_LoadDataHarian()
            Else
                fn_LoadDataBulanan()
            End If



        Catch oErr As Exception
            MsgBox(Statement.ErrorStatement & vbCrLf & oErr.Message, MsgBoxStyle.Exclamation, Me.Text)
        End Try
    End Sub

#Region "Harian"
    Private Sub fn_LoadDataHarian()
        Try
            Dim oConn As New SqlConnection
            Dim oComm As New SqlCommand
            Dim da As SqlDataAdapter
            Dim ds As New DataSet
            Dim SQL As String

            Dim sConn As String = Decrypt(My.Computer.Registry.GetValue("HKEY_CURRENT_USER\Software\BUDIJAYAGROUP\MAIN\", "Database", "").ToString())

            oConn = New SqlConnection(sConn)
            If oConn.State = ConnectionState.Closed Then
                oConn.Open()
            End If

            SQL = "SELECT Nama,Departemen,Telat,Lembur,TotalLembur = Lembur - Telat,UML,HariKerja,Absen = I+A+S,TotalAbsen = I+A+SD+S+H+C,I,A,SD,S,H,C "
            SQL &= "FROM (  "
            SQL &= "SELECT  "
            SQL &= "Nama = A.NAME_DISPLAY  "
            SQL &= ",Departemen = B.MEMO  "
            SQL &= ",Telat = ISNULL((SELECT SUM(AA.LATE) FROM H_ABSEN AS AA  WHERE AA.KDSTAFF = A.KDSTAFF AND CONVERT(VARCHAR(8), AA.DATE, 112) >= '" & deDATEFrom.DateTime.ToString("yyyyMMdd") & "' AND CONVERT(VARCHAR(8), AA.DATE, 112) <= '" & deDATETo.DateTime.ToString("yyyyMMdd") & "' AND AA.DESCRIPTION = 'ABSEN'), 0)  "
            SQL &= ",Lembur = ISNULL((SELECT SUM(AA.OVERTIME) FROM H_ABSEN AS AA  WHERE AA.KDSTAFF = A.KDSTAFF AND CONVERT(VARCHAR(8), AA.DATE, 112) >= '" & deDATEFrom.DateTime.ToString("yyyyMMdd") & "' AND CONVERT(VARCHAR(8), AA.DATE, 112) <= '" & deDATETo.DateTime.ToString("yyyyMMdd") & "'  AND AA.DESCRIPTION = 'ABSEN'), 0)  "
            SQL &= ",UML = ISNULL((SELECT SUM(AA.UML) FROM H_ABSEN AS AA  WHERE AA.KDSTAFF = A.KDSTAFF AND CONVERT(VARCHAR(8), AA.DATE, 112) >= '" & deDATEFrom.DateTime.ToString("yyyyMMdd") & "' AND CONVERT(VARCHAR(8), AA.DATE, 112) <= '" & deDATETo.DateTime.ToString("yyyyMMdd") & "'  AND AA.DESCRIPTION = 'ABSEN'), 0)  "
            SQL &= ",HariKerja = ISNULL((SELECT COUNT(AA.KDABSEN) FROM H_ABSEN AS AA  WHERE AA.KDSTAFF = A.KDSTAFF AND CONVERT(VARCHAR(8), AA.DATE, 112) >= '" & deDATEFrom.DateTime.ToString("yyyyMMdd") & "' AND CONVERT(VARCHAR(8), AA.DATE, 112) <= '" & deDATETo.DateTime.ToString("yyyyMMdd") & "'  AND AA.DESCRIPTION = 'ABSEN' ), 0)  "
            SQL &= ",I = ISNULL((SELECT COUNT(AA.KDABSEN) FROM H_ABSEN AS AA  WHERE AA.KDSTAFF = A.KDSTAFF AND CONVERT(VARCHAR(8), AA.DATE, 112) >= '" & deDATEFrom.DateTime.ToString("yyyyMMdd") & "' AND CONVERT(VARCHAR(8), AA.DATE, 112) <= '" & deDATETo.DateTime.ToString("yyyyMMdd") & "'  AND AA.DESCRIPTION = 'IJIN' ), 0)  "
            SQL &= ",A = ISNULL((SELECT COUNT(AA.KDABSEN) FROM H_ABSEN AS AA  WHERE AA.KDSTAFF = A.KDSTAFF AND CONVERT(VARCHAR(8), AA.DATE, 112) >= '" & deDATEFrom.DateTime.ToString("yyyyMMdd") & "' AND CONVERT(VARCHAR(8), AA.DATE, 112) <= '" & deDATETo.DateTime.ToString("yyyyMMdd") & "'  AND AA.DESCRIPTION = 'ALPHA' ), 0)  "
            SQL &= ",SD = ISNULL((SELECT COUNT(AA.KDABSEN) FROM H_ABSEN AS AA  WHERE AA.KDSTAFF = A.KDSTAFF AND CONVERT(VARCHAR(8), AA.DATE, 112) >= '" & deDATEFrom.DateTime.ToString("yyyyMMdd") & "' AND CONVERT(VARCHAR(8), AA.DATE, 112) <= '" & deDATETo.DateTime.ToString("yyyyMMdd") & "'  AND AA.DESCRIPTION = 'SURATDOKTER' ), 0)  "
            SQL &= ",S = ISNULL((SELECT COUNT(AA.KDABSEN) FROM H_ABSEN AS AA  WHERE AA.KDSTAFF = A.KDSTAFF AND CONVERT(VARCHAR(8), AA.DATE, 112) >= '" & deDATEFrom.DateTime.ToString("yyyyMMdd") & "' AND CONVERT(VARCHAR(8), AA.DATE, 112) <= '" & deDATETo.DateTime.ToString("yyyyMMdd") & "'  AND AA.DESCRIPTION = 'SAKIT' ), 0)  "
            SQL &= ",H = ISNULL((SELECT COUNT(AA.KDABSEN) FROM H_ABSEN AS AA  WHERE AA.KDSTAFF = A.KDSTAFF AND CONVERT(VARCHAR(8), AA.DATE, 112) >= '" & deDATEFrom.DateTime.ToString("yyyyMMdd") & "' AND CONVERT(VARCHAR(8), AA.DATE, 112) <= '" & deDATETo.DateTime.ToString("yyyyMMdd") & "'  AND AA.DESCRIPTION = 'H' ), 0)  "
            SQL &= ",C = ISNULL((SELECT COUNT(AA.KDABSEN) FROM H_ABSEN AS AA  WHERE AA.KDSTAFF = A.KDSTAFF AND CONVERT(VARCHAR(8), AA.DATE, 112) >= '" & deDATEFrom.DateTime.ToString("yyyyMMdd") & "' AND CONVERT(VARCHAR(8), AA.DATE, 112) <= '" & deDATETo.DateTime.ToString("yyyyMMdd") & "'  AND AA.DESCRIPTION = 'CUTI' ), 0)  "
            SQL &= "FROM M_STAFF A INNER JOIN M_CATEGORY AS B ON A.KDCATEGORY = B.KDCATEGORY WHERE A.SALARYSYSTEM = '" & cboTYPE.Text & "' )  AS A   "



            oComm.Connection = oConn
            oComm.CommandText = SQL
            oComm.CommandTimeout = 120
            oComm.CommandType = CommandType.Text

            da = New SqlDataAdapter(oComm)
            da.Fill(ds, "ALL")

            grd.MainView = grv
            grd.DataSource = ds.Tables("ALL")
            grd.ForceInitialize()

            fn_LoadFormatDataHarian()
        Catch oErr As Exception
            MsgBox(Statement.ErrorStatement & vbCrLf & oErr.Message, MsgBoxStyle.Exclamation, Me.Text)
        End Try
    End Sub
    Private Sub fn_LoadFormatDataHarian()
        For iLoop As Integer = 0 To grv.Columns.Count - 1
            If grv.Columns(iLoop).ColumnType.Name = "Decimal" Then
                grv.Columns(iLoop).DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
                grv.Columns(iLoop).DisplayFormat.FormatString = "{0:n2}"
                grv.Columns(iLoop).AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far

                grv.GroupSummary.Add(DevExpress.Data.SummaryItemType.Sum, grv.Columns(iLoop).FieldName, grv.Columns(iLoop),
                                     "{0:n2}")
                grv.Columns(iLoop).SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
                grv.Columns(iLoop).SummaryItem.DisplayFormat = "{0:n2}"
            ElseIf grv.Columns(iLoop).ColumnType.Name = "DateTime" Then
                grv.Columns(iLoop).DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
                grv.Columns(iLoop).DisplayFormat.FormatString = "{0:dd/MM/yyyy}"
            End If
        Next
    End Sub
#End Region


#Region "Bulanan"
    Private Sub fn_LoadDataBulanan()
        Try
            Dim oConn As New SqlConnection
            Dim oComm As New SqlCommand
            Dim da As SqlDataAdapter
            Dim ds As New DataSet
            Dim SQL As String

            Dim sConn As String = Decrypt(My.Computer.Registry.GetValue("HKEY_CURRENT_USER\Software\BUDIJAYAGROUP\MAIN\", "Database", "").ToString())

            oConn = New SqlConnection(sConn)
            If oConn.State = ConnectionState.Closed Then
                oConn.Open()
            End If

            SQL = "SELECT Nama,Departemen,Telat,Lembur,TotalLembur = Lembur - Telat,UML,HariKerja,Absen = I+A+S,TotalAbsen = I+A+SD+S+H+C,I,A,SD,S,H,C "
            SQL &= "FROM (  "
            SQL &= "SELECT  "
            SQL &= "Nama = A.NAME_DISPLAY  "
            SQL &= ",Departemen = B.MEMO  "
            SQL &= ",Telat = ISNULL((SELECT SUM(AA.LATE) FROM H_ABSEN AS AA  WHERE AA.KDSTAFF = A.KDSTAFF AND CONVERT(VARCHAR(8), AA.DATE, 112) >= '" & deDATEFrom.DateTime.ToString("yyyyMMdd") & "' AND CONVERT(VARCHAR(8), AA.DATE, 112) <= '" & deDATETo.DateTime.ToString("yyyyMMdd") & "' AND AA.DESCRIPTION = 'ABSEN'), 0)  "
            SQL &= ",Lembur = ISNULL((SELECT SUM(AA.OVERTIME) FROM H_ABSEN AS AA  WHERE AA.KDSTAFF = A.KDSTAFF AND CONVERT(VARCHAR(8), AA.DATE, 112) >= '" & deDATEFrom.DateTime.ToString("yyyyMMdd") & "' AND CONVERT(VARCHAR(8), AA.DATE, 112) <= '" & deDATETo.DateTime.ToString("yyyyMMdd") & "'  AND AA.DESCRIPTION = 'ABSEN'), 0)  "
            SQL &= ",UML = ISNULL((SELECT SUM(AA.UML) FROM H_ABSEN AS AA  WHERE AA.KDSTAFF = A.KDSTAFF AND CONVERT(VARCHAR(8), AA.DATE, 112) >= '" & deDATEFrom.DateTime.ToString("yyyyMMdd") & "' AND CONVERT(VARCHAR(8), AA.DATE, 112) <= '" & deDATETo.DateTime.ToString("yyyyMMdd") & "'  AND AA.DESCRIPTION = 'ABSEN'), 0)  "
            SQL &= ",HariKerja = ISNULL((SELECT COUNT(AA.KDABSEN) FROM H_ABSEN AS AA  WHERE AA.KDSTAFF = A.KDSTAFF AND CONVERT(VARCHAR(8), AA.DATE, 112) >= '" & deDATEFrom.DateTime.ToString("yyyyMMdd") & "' AND CONVERT(VARCHAR(8), AA.DATE, 112) <= '" & deDATETo.DateTime.ToString("yyyyMMdd") & "'  AND AA.DESCRIPTION = 'ABSEN' ), 0)  "
            SQL &= ",I = ISNULL((SELECT COUNT(AA.KDABSEN) FROM H_ABSEN AS AA  WHERE AA.KDSTAFF = A.KDSTAFF AND CONVERT(VARCHAR(8), AA.DATE, 112) >= '" & deDATEFrom.DateTime.ToString("yyyyMMdd") & "' AND CONVERT(VARCHAR(8), AA.DATE, 112) <= '" & deDATETo.DateTime.ToString("yyyyMMdd") & "'  AND AA.DESCRIPTION = 'IJIN' ), 0)  "
            SQL &= ",A = ISNULL((SELECT COUNT(AA.KDABSEN) FROM H_ABSEN AS AA  WHERE AA.KDSTAFF = A.KDSTAFF AND CONVERT(VARCHAR(8), AA.DATE, 112) >= '" & deDATEFrom.DateTime.ToString("yyyyMMdd") & "' AND CONVERT(VARCHAR(8), AA.DATE, 112) <= '" & deDATETo.DateTime.ToString("yyyyMMdd") & "'  AND AA.DESCRIPTION = 'ALPHA' ), 0)  "
            SQL &= ",SD = ISNULL((SELECT COUNT(AA.KDABSEN) FROM H_ABSEN AS AA  WHERE AA.KDSTAFF = A.KDSTAFF AND CONVERT(VARCHAR(8), AA.DATE, 112) >= '" & deDATEFrom.DateTime.ToString("yyyyMMdd") & "' AND CONVERT(VARCHAR(8), AA.DATE, 112) <= '" & deDATETo.DateTime.ToString("yyyyMMdd") & "'  AND AA.DESCRIPTION = 'SURATDOKTER' ), 0)  "
            SQL &= ",S = ISNULL((SELECT COUNT(AA.KDABSEN) FROM H_ABSEN AS AA  WHERE AA.KDSTAFF = A.KDSTAFF AND CONVERT(VARCHAR(8), AA.DATE, 112) >= '" & deDATEFrom.DateTime.ToString("yyyyMMdd") & "' AND CONVERT(VARCHAR(8), AA.DATE, 112) <= '" & deDATETo.DateTime.ToString("yyyyMMdd") & "'  AND AA.DESCRIPTION = 'SAKIT' ), 0)  "
            SQL &= ",H = ISNULL((SELECT COUNT(AA.KDABSEN) FROM H_ABSEN AS AA  WHERE AA.KDSTAFF = A.KDSTAFF AND CONVERT(VARCHAR(8), AA.DATE, 112) >= '" & deDATEFrom.DateTime.ToString("yyyyMMdd") & "' AND CONVERT(VARCHAR(8), AA.DATE, 112) <= '" & deDATETo.DateTime.ToString("yyyyMMdd") & "'  AND AA.DESCRIPTION = 'H' ), 0)  "
            SQL &= ",C = ISNULL((SELECT COUNT(AA.KDABSEN) FROM H_ABSEN AS AA  WHERE AA.KDSTAFF = A.KDSTAFF AND CONVERT(VARCHAR(8), AA.DATE, 112) >= '" & deDATEFrom.DateTime.ToString("yyyyMMdd") & "' AND CONVERT(VARCHAR(8), AA.DATE, 112) <= '" & deDATETo.DateTime.ToString("yyyyMMdd") & "'  AND AA.DESCRIPTION = 'CUTI' ), 0)  "
            SQL &= "FROM M_STAFF A INNER JOIN M_CATEGORY AS B ON A.KDCATEGORY = B.KDCATEGORY WHERE A.SALARYSYSTEM = '" & cboTYPE.Text & "' )  AS A   "


            oComm.Connection = oConn
            oComm.CommandText = SQL
            oComm.CommandTimeout = 120
            oComm.CommandType = CommandType.Text

            da = New SqlDataAdapter(oComm)
            da.Fill(ds, "ALL")

            grd.MainView = grv
            grd.DataSource = ds.Tables("ALL")
            grd.ForceInitialize()

            fn_LoadFormatDataBulanan()
        Catch oErr As Exception
            MsgBox(Statement.ErrorStatement & vbCrLf & oErr.Message, MsgBoxStyle.Exclamation, Me.Text)
        End Try
    End Sub
    Private Sub fn_LoadFormatDataBulanan()
        For iLoop As Integer = 0 To grv.Columns.Count - 1
            If grv.Columns(iLoop).ColumnType.Name = "Decimal" Then
                grv.Columns(iLoop).DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
                grv.Columns(iLoop).DisplayFormat.FormatString = "{0:n2}"
                grv.Columns(iLoop).AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far

                grv.GroupSummary.Add(DevExpress.Data.SummaryItemType.Sum, grv.Columns(iLoop).FieldName, grv.Columns(iLoop),
                                     "{0:n2}")
                grv.Columns(iLoop).SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Sum
                grv.Columns(iLoop).SummaryItem.DisplayFormat = "{0:n2}"
            ElseIf grv.Columns(iLoop).ColumnType.Name = "DateTime" Then
                grv.Columns(iLoop).DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
                grv.Columns(iLoop).DisplayFormat.FormatString = "{0:dd/MM/yyyy}"
            End If
        Next
    End Sub
#End Region

    Private Sub MasterColumnChooserToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles MasterColumnChooserToolStripMenuItem.Click
        grv.ShowCustomization()
    End Sub
#End Region
#Region "Command Button"
    Private Sub frmMember_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Select Case e.KeyCode
            Case Keys.Escape
                Me.Close()
            Case Keys.P
                If e.Alt = True And picPrint.Enabled = True Then
                    picPrint_Click()
                End If
            Case Keys.R
                If e.Alt = True And picRefresh.Enabled = True Then
                    picRefresh_Click()
                End If
        End Select
    End Sub
    Private Sub picPrint_Click() Handles picPrint.Click
        Try
            fn_Print()
        Catch oErr As Exception
            MsgBox(Statement.ErrorStatement & vbCrLf & oErr.Message, MsgBoxStyle.Exclamation, Me.Text)
        End Try
    End Sub
    Private Sub picRefresh_Click() Handles picRefresh.Click
        fn_LoadSecurity()

        Try
            grv.RestoreLayoutFromRegistry("HKEY_CURRENT_USER\Software\BUDIJAYAGROUP\MAIN\" & sUserID & "\" & Me.Text)
        Catch ex As Exception

        End Try
    End Sub
#End Region
End Class