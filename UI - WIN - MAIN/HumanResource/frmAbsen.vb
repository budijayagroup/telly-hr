﻿Imports System.Threading
Imports DPUruNet
Imports DPUruNet.Constants
Imports System.Drawing.Imaging

Imports DataAccess
Imports UI.WIN.MAIN.My.Resources
Imports System.Linq

Public Class frmAbsen
    Private Const DPFJ_PROBABILITY_ONE As Integer = &H7FFFFFFF
    Private rightIndex As Fmd
    Private rightThumb As Fmd
    Private anyFinger As Fmd
    Private count As Integer

    ''' <summary>
    ''' Initialize the form.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub Identification_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Timer_Clock.Interval = 1 * 1000
        Me.Timer_Clock.Enabled = True
        txtIdentify.Text = String.Empty

        Dim oStaff As New HumanResource.clsStaffJari

        Dim ds = oStaff.GetData

        'For Each iLoop In ds
        '    Try
        '        arrFmds.Add(Importer.ImportFmd(iLoop.J0.ToArray(), Formats.Fmd.ANSI, Formats.Fmd.ANSI).Data)
        '        fmds.Add(Importer.ImportFmd(iLoop.J0.ToArray(), Formats.Fmd.ANSI, Formats.Fmd.ANSI).Data, iLoop.KDSTAFF)
        '    Catch ex As Exception

        '    End Try
        '    Try
        '        arrFmds.Add(Importer.ImportFmd(iLoop.J1.ToArray(), Formats.Fmd.ANSI, Formats.Fmd.ANSI).Data)
        '        fmds.Add(Importer.ImportFmd(iLoop.J1.ToArray(), Formats.Fmd.ANSI, Formats.Fmd.ANSI).Data, iLoop.KDSTAFF)
        '    Catch ex As Exception

        '    End Try
        '    Try
        '        arrFmds.Add(Importer.ImportFmd(iLoop.J2.ToArray(), Formats.Fmd.ANSI, Formats.Fmd.ANSI).Data)
        '        fmds.Add(Importer.ImportFmd(iLoop.J2.ToArray(), Formats.Fmd.ANSI, Formats.Fmd.ANSI).Data, iLoop.KDSTAFF)
        '    Catch ex As Exception

        '    End Try
        '    Try
        '        arrFmds.Add(Importer.ImportFmd(iLoop.J3.ToArray(), Formats.Fmd.ANSI, Formats.Fmd.ANSI).Data)
        '        fmds.Add(Importer.ImportFmd(iLoop.J3.ToArray(), Formats.Fmd.ANSI, Formats.Fmd.ANSI).Data, iLoop.KDSTAFF)
        '    Catch ex As Exception

        '    End Try
        '    Try
        '        arrFmds.Add(Importer.ImportFmd(iLoop.J4.ToArray(), Formats.Fmd.ANSI, Formats.Fmd.ANSI).Data)
        '        fmds.Add(Importer.ImportFmd(iLoop.J4.ToArray(), Formats.Fmd.ANSI, Formats.Fmd.ANSI).Data, iLoop.KDSTAFF)
        '    Catch ex As Exception

        '    End Try
        '    Try
        '        arrFmds.Add(Importer.ImportFmd(iLoop.J5.ToArray(), Formats.Fmd.ANSI, Formats.Fmd.ANSI).Data)
        '        fmds.Add(Importer.ImportFmd(iLoop.J5.ToArray(), Formats.Fmd.ANSI, Formats.Fmd.ANSI).Data, iLoop.KDSTAFF)
        '    Catch ex As Exception

        '    End Try
        '    Try
        '        arrFmds.Add(Importer.ImportFmd(iLoop.J6.ToArray(), Formats.Fmd.ANSI, Formats.Fmd.ANSI).Data)
        '        fmds.Add(Importer.ImportFmd(iLoop.J6.ToArray(), Formats.Fmd.ANSI, Formats.Fmd.ANSI).Data, iLoop.KDSTAFF)
        '    Catch ex As Exception

        '    End Try
        '    Try
        '        arrFmds.Add(Importer.ImportFmd(iLoop.J7.ToArray(), Formats.Fmd.ANSI, Formats.Fmd.ANSI).Data)
        '        fmds.Add(Importer.ImportFmd(iLoop.J7.ToArray(), Formats.Fmd.ANSI, Formats.Fmd.ANSI).Data, iLoop.KDSTAFF)
        '    Catch ex As Exception

        '    End Try
        '    Try
        '        arrFmds.Add(Importer.ImportFmd(iLoop.J8.ToArray(), Formats.Fmd.ANSI, Formats.Fmd.ANSI).Data)
        '        fmds.Add(Importer.ImportFmd(iLoop.J8.ToArray(), Formats.Fmd.ANSI, Formats.Fmd.ANSI).Data, iLoop.KDSTAFF)
        '    Catch ex As Exception

        '    End Try
        '    Try
        '       arrFmds.Add(Importer.ImportFmd(iLoop.J9.ToArray(), Formats.Fmd.ANSI, Formats.Fmd.ANSI).Data)
        '        fmds.Add(Importer.ImportFmd(iLoop.J9.ToArray(), Formats.Fmd.ANSI, Formats.Fmd.ANSI).Data, iLoop.KDSTAFF)
        '    Catch ex As Exception

        '    End Try
        'Next

        'If Not OpenReader() Then
        '    Me.Close()
        'End If

        'If Not StartCaptureAsync(AddressOf Me.OnCaptured) Then
        '    Me.Close()
        'End If
    End Sub
    ''' <summary>
    ''' Handler for when a fingerprint is captured.
    ''' </summary>
    ''' <param name="captureResult">contains info and data on the fingerprint capture</param>
    Public Sub OnCaptured(ByVal captureResult As CaptureResult)
        Try
            ' Check capture quality and throw an error if bad.
            If Not CheckCaptureResult(captureResult) Then Return

            Dim resultConversion As DataResult(Of Fmd) = FeatureExtraction.CreateFmdFromFid(captureResult.Data, Formats.Fmd.ANSI)

            If resultConversion.ResultCode <> Constants.ResultCode.DP_SUCCESS Then
                Reset = True
                Throw New Exception("" & resultConversion.ResultCode.ToString())
            End If

            anyFinger = resultConversion.Data

            Dim thresholdScore As Integer = DPFJ_PROBABILITY_ONE * 1 / 100000

            For iLoop As Integer = 0 To fmds.Count - 1
                Dim listLoop As New List(Of Fmd)
                listLoop.Add(fmds.Keys.ElementAt(iLoop))

                Dim identifyResult = Comparison.Identify(anyFinger, 0, listLoop, thresholdScore, 2)

                If identifyResult.ResultCode <> Constants.ResultCode.DP_SUCCESS Then
                    Reset = True
                    Throw New Exception("" & identifyResult.ResultCode.ToString())
                End If

                If identifyResult.Indexes.Length > 0 Then
                    SendMessage(Action.SendMessage, "Identifikasi berhasil!")

                    Dim KDSTAFF = fmds.Item(fmds.Keys.ElementAt(iLoop))
                    Dim oStaff = New HumanResource.clsStaffJari

                    If fn_Save(KDSTAFF) = True Then
                        Dim oAbsen As New HumanResource.clsAbsen

                        Dim dsAbsen = oAbsen.GetData(KDSTAFF).FirstOrDefault(Function(x) x.DATE.ToString("yyyyMMdd") = Now.ToString("yyyyMMdd"))

                        SendMessage(Action.SendMessage,
                                    "Absen" & vbTab & vbTab & ": " & Now.ToString("dd/MM/yyyy") & vbCrLf &
                                    "Staff" & vbTab & vbTab & ": " & oStaff.GetData(KDSTAFF).NAME_DISPLAY & vbCrLf &
                                    "Masuk" & vbTab & vbTab & ": " & dsAbsen.JAMMASUK & vbCrLf &
                                    "Keluar" & vbTab & vbTab & ": " & dsAbsen.JAMKELUAR)
                        wait(1)
                        SendMessage(Action.SendMessage, "")

                    End If
                    Exit Sub
                End If
            Next

            SendMessage(Action.SendMessage, "Identifikasi tidak berhasil!")
        Catch ex As Exception
            SendMessage(Action.SendMessage, "Error:  " & ex.Message)
        End Try
    End Sub

    Private Sub wait(ByVal seconds As Integer)
        For i As Integer = 0 To seconds * 100
            System.Threading.Thread.Sleep(10)
            Application.DoEvents()
        Next
    End Sub


    Private Function fn_Save(ByVal sKDSTAFF As String) As Boolean
        Try
            Dim oAbsen As New HumanResource.clsAbsen

            ' ***** HEADER *****
            Dim ds = oAbsen.GetStructureHeader
            With ds
                Try
                    .KDABSEN =  oAbsen.GetData(sKDSTAFF).FirstOrDefault(Function(x) x.DATE.ToString("yyyyMMdd") = Now.ToString("yyyyMMdd")).KDABSEN
                Catch ex As Exception
                    .KDABSEN = String.Empty
                End Try

                .DATE = IIf(Now.Hour < 7, Now.AddDays(-1).ToShortDateString(), Now.ToShortDateString())

                Try
                    .JAMMASUK = oAbsen.GetData(sKDSTAFF).FirstOrDefault(Function(x) x.DATE.ToString("yyyyMMdd") = Now.ToString("yyyyMMdd")).JAMMASUK
                Catch ex As Exception
                    .JAMMASUK = Now.ToString("HH:mm")
                End Try

                If .KDABSEN = String.Empty Then
                    .JAMKELUAR = ""
                ElseIf Now.ToString("MM/dd/yyyy HH:mm:ss ") > Now.ToString("MM/dd/yyyy") & " 09:00:00 " Then
                    .JAMKELUAR = Now.ToString("HH:mm")
                Else
                    .JAMKELUAR = ""
                End If

                .DESCRIPTION = "ABSEN"
                .KDSTAFF = sKDSTAFF

                Try
                    .LATE = oAbsen.GetData(sKDSTAFF).FirstOrDefault(Function(x) x.DATE.ToString("yyyyMMdd") = Now.ToString("yyyyMMdd")).LATE
                Catch ex As Exception
                    Dim time1 As DateTime = Now.ToString("MM/dd/yyyy HH:mm:ss ")
                    Dim time2 As DateTime = Now.ToString("MM/dd/yyyy") & " 08:01:00 "
                    If time1 >= time2 Then
                        Dim DIFF = time1 - time2
                        .LATE = DIFF.TotalMinutes / 60
                    Else
                        .LATE = CDec(0)
                    End If
                End Try

                'Try
                '    Dim oStaff As New HumanResource.clsStaff
                '    Dim sOverTime As Decimal = oAbsen.GetData(sKDSTAFF).FirstOrDefault(Function(x) x.DATE.ToString("yyyyMMdd") = Now.ToString("yyyyMMdd")).OVERTIME

                '    If sOverTime = 0 Or sOverTime > 0 Then

                '        Dim day As Integer = Date.Today.DayOfWeek
                '        Select Case day
                '            Case "1", "2", "3", "4"
                '                If oStaff.GetData(sKDSTAFF).POSITION = "STAFF OFFICE" Then

                '                    If Now.ToString("MM/dd/yyyy HH:mm:ss ") >= Now.ToString("MM/dd/yyyy") & " 16:50:00 " And Now.ToString("MM/dd/yyyy HH:mm:ss ") < Now.ToString("MM/dd/yyyy") & " 17:20:00 " Then
                '                        .OVERTIME = CDec(1)
                '                    ElseIf Now.ToString("MM/dd/yyyy HH:mm:ss ") >= Now.ToString("MM/dd/yyyy") & " 17:20:00 " And Now.ToString("MM/dd/yyyy HH:mm:ss ") < Now.ToString("MM/dd/yyyy") & " 17:45:00 " Then
                '                        .OVERTIME = CDec(1.5)
                '                    ElseIf Now.ToString("MM/dd/yyyy HH:mm:ss ") >= Now.ToString("MM/dd/yyyy") & " 17:45:00 " And Now.ToString("MM/dd/yyyy HH:mm:ss ") < Now.ToString("MM/dd/yyyy") & " 18:45:00 " Then
                '                        .OVERTIME = CDec(2)
                '                    ElseIf Now.ToString("MM/dd/yyyy HH:mm:ss ") >= Now.ToString("MM/dd/yyyy") & " 18:45:00 " And Now.ToString("MM/dd/yyyy HH:mm:ss ") < Now.ToString("MM/dd/yyyy") & " 19:20:00 " Then
                '                        .OVERTIME = CDec(2.5)
                '                    ElseIf Now.ToString("MM/dd/yyyy HH:mm:ss ") >= Now.ToString("MM/dd/yyyy") & " 19:20:00 " And Now.ToString("MM/dd/yyyy HH:mm:ss ") < Now.ToString("MM/dd/yyyy") & " 19:45:00 " Then
                '                        .OVERTIME = CDec(3)
                '                    ElseIf Now.ToString("MM/dd/yyyy HH:mm:ss ") >= Now.ToString("MM/dd/yyyy") & " 19:45:00 " And Now.ToString("MM/dd/yyyy HH:mm:ss ") < Now.ToString("MM/dd/yyyy") & " 20:45:00 " Then
                '                        .OVERTIME = CDec(3.5)
                '                    ElseIf Now.ToString("MM/dd/yyyy HH:mm:ss ") >= Now.ToString("MM/dd/yyyy") & " 20:45:00 " And Now.ToString("MM/dd/yyyy HH:mm:ss ") < Now.ToString("MM/dd/yyyy") & " 21:30:00 " Then
                '                        .OVERTIME = CDec(4)
                '                    ElseIf Now.ToString("MM/dd/yyyy HH:mm:ss ") >= Now.ToString("MM/dd/yyyy") & " 21:30:00 " And Now.ToString("MM/dd/yyyy HH:mm:ss ") < Now.ToString("MM/dd/yyyy") & " 22:00:00 " Then
                '                        .OVERTIME = CDec(4.5)
                '                    ElseIf Now.ToString("MM/dd/yyyy HH:mm:ss ") >= Now.ToString("MM/dd/yyyy") & " 22:00:00 " And Now.ToString("MM/dd/yyyy HH:mm:ss ") < Now.ToString("MM/dd/yyyy") & " 22:30:00 " Then
                '                        .OVERTIME = CDec(5)
                '                    ElseIf Now.ToString("MM/dd/yyyy HH:mm:ss ") >= Now.ToString("MM/dd/yyyy") & " 22:30:00 " And Now.ToString("MM/dd/yyyy HH:mm:ss ") < Now.ToString("MM/dd/yyyy") & " 23:00:00 " Then
                '                        .OVERTIME = CDec(5.5)
                '                    ElseIf Now.ToString("MM/dd/yyyy HH:mm:ss ") >= Now.ToString("MM/dd/yyyy") & " 23:00:00 " And Now.ToString("dd/MM/yyyy HH:mm:ss ") < Now.ToString("MM/dd/yyyy") & " 23:30:00 " Then
                '                        .OVERTIME = CDec(6)
                '                    ElseIf Now.ToString("MM/dd/yyyy HH:mm:ss ") >= Now.ToString("MM/dd/yyyy") & " 23:30:00 " And Now.ToString("MM/dd/yyyy HH:mm:ss ") <= Now.ToString("MM/dd/yyyy") & " 23:59:59 " Then
                '                        .OVERTIME = CDec(6.5)
                '                    Else
                '                        .OVERTIME = CDec(0)
                '                    End If

                '                Else

                '                    If Now.ToString("MM/dd/yyyy HH:mm:ss ") >= Now.ToString("MM/dd/yyyy") & " 16:50:00 " And Now.ToString("MM/dd/yyyy HH:mm:ss ") < Now.ToString("MM/dd/yyyy") & " 17:45:00 " Then
                '                        .OVERTIME = CDec(1.5)
                '                    ElseIf Now.ToString("MM/dd/yyyy HH:mm:ss ") >= Now.ToString("MM/dd/yyyy") & " 17:45:00 " And Now.ToString("MM/dd/yyyy HH:mm:ss ") < Now.ToString("MM/dd/yyyy") & " 18:45:00 " Then
                '                        .OVERTIME = CDec(2.5)
                '                    ElseIf Now.ToString("MM/dd/yyyy HH:mm:ss ") >= Now.ToString("MM/dd/yyyy") & " 18:45:00 " And Now.ToString("MM/dd/yyyy HH:mm:ss ") < Now.ToString("MM/dd/yyyy") & " 19:20:00 " Then
                '                        .OVERTIME = CDec(3.5)
                '                    ElseIf Now.ToString("MM/dd/yyyy HH:mm:ss ") >= Now.ToString("MM/dd/yyyy") & " 19:20:00 " And Now.ToString("MM/dd/yyyy HH:mm:ss ") < Now.ToString("MM/dd/yyyy") & " 19:45:00 " Then
                '                        .OVERTIME = CDec(4.5)
                '                    ElseIf Now.ToString("MM/dd/yyyy HH:mm:ss ") >= Now.ToString("MM/dd/yyyy") & " 19:45:00 " And Now.ToString("MM/dd/yyyy HH:mm:ss ") < Now.ToString("MM/dd/yyyy") & " 20:45:00 " Then
                '                        .OVERTIME = CDec(5.5)
                '                    ElseIf Now.ToString("MM/dd/yyyy HH:mm:ss ") >= Now.ToString("MM/dd/yyyy") & " 20:45:00 " And Now.ToString("MM/dd/yyyy HH:mm:ss ") < Now.ToString("MM/dd/yyyy") & " 21:30:00 " Then
                '                        .OVERTIME = CDec(6.5)
                '                    ElseIf Now.ToString("MM/dd/yyyy HH:mm:ss ") >= Now.ToString("MM/dd/yyyy") & " 21:30:00 " And Now.ToString("MM/dd/yyyy HH:mm:ss ") < Now.ToString("MM/dd/yyyy") & " 22:00:00 " Then
                '                        .OVERTIME = CDec(7.5)
                '                    ElseIf Now.ToString("MM/dd/yyyy HH:mm:ss ") >= Now.ToString("MM/dd/yyyy") & " 22:00:00 " And Now.ToString("MM/dd/yyyy HH:mm:ss ") < Now.ToString("MM/dd/yyyy") & " 22:30:00 " Then
                '                        .OVERTIME = CDec(8.5)
                '                    ElseIf Now.ToString("MM/dd/yyyy HH:mm:ss ") >= Now.ToString("MM/dd/yyyy") & " 22:30:00 " And Now.ToString("MM/dd/yyyy HH:mm:ss ") < Now.ToString("MM/dd/yyyy") & " 23:00:00 " Then
                '                        .OVERTIME = CDec(9.5)
                '                    ElseIf Now.ToString("MM/dd/yyyy HH:mm:ss ") >= Now.ToString("MM/dd/yyyy") & " 23:00:00 " And Now.ToString("MM/dd/yyyy HH:mm:ss ") < Now.ToString("MM/dd/yyyy") & " 23:30:00 " Then
                '                        .OVERTIME = CDec(10.5)
                '                    ElseIf Now.ToString("MM/dd/yyyy HH:mm:ss ") >= Now.ToString("MM/dd/yyyy") & " 23:30:00 " And Now.ToString("MM/dd/yyyy HH:mm:ss ") <= Now.ToString("MM/dd/yyyy") & " 23:59:59 " Then
                '                        .OVERTIME = CDec(11.5)
                '                    Else
                '                        .OVERTIME = CDec(0)
                '                    End If
                '                End If

                '            Case "5"

                '                If oStaff.GetData(sKDSTAFF).POSITION = "STAFF OFFICE" Then

                '                    If Now.ToString("MM/dd/yyyy HH:mm:ss ") >= Now.ToString("MM/dd/yyyy") & " 17:20:00 " And Now.ToString("MM/dd/yyyy HH:mm:ss ") < Now.ToString("MM/dd/yyyy") & " 17:45:00 " Then
                '                        .OVERTIME = CDec(1)
                '                    ElseIf Now.ToString("MM/dd/yyyy HH:mm:ss ") >= Now.ToString("MM/dd/yyyy") & " 17:45:00 " And Now.ToString("MM/dd/yyyy HH:mm:ss ") < Now.ToString("MM/dd/yyyy") & " 18:45:00 " Then
                '                        .OVERTIME = CDec(1.5)
                '                    ElseIf Now.ToString("MM/dd/yyyy HH:mm:ss ") >= Now.ToString("MM/dd/yyyy") & " 18:45:00 " And Now.ToString("MM/dd/yyyy HH:mm:ss ") < Now.ToString("MM/dd/yyyy") & " 19:20:00 " Then
                '                        .OVERTIME = CDec(2)
                '                    ElseIf Now.ToString("MM/dd/yyyy HH:mm:ss ") >= Now.ToString("MM/dd/yyyy") & " 19:20:00 " And Now.ToString("MM/dd/yyyy HH:mm:ss ") < Now.ToString("MM/dd/yyyy") & " 19:45:00 " Then
                '                        .OVERTIME = CDec(2.5)
                '                    ElseIf Now.ToString("MM/dd/yyyy HH:mm:ss ") >= Now.ToString("MM/dd/yyyy") & " 19:45:00 " And Now.ToString("MM/dd/yyyy HH:mm:ss ") < Now.ToString("MM/dd/yyyy") & " 20:45:00 " Then
                '                        .OVERTIME = CDec(3)
                '                    ElseIf Now.ToString("MM/dd/yyyy HH:mm:ss ") >= Now.ToString("MM/dd/yyyy") & " 20:45:00 " And Now.ToString("MM/dd/yyyy HH:mm:ss ") < Now.ToString("MM/dd/yyyy") & " 21:30:00 " Then
                '                        .OVERTIME = CDec(3.5)
                '                    ElseIf Now.ToString("MM/dd/yyyy HH:mm:ss ") >= Now.ToString("MM/dd/yyyy") & " 21:30:00 " And Now.ToString("MM/dd/yyyy HH:mm:ss ") < Now.ToString("MM/dd/yyyy") & " 22:00:00 " Then
                '                        .OVERTIME = CDec(4)
                '                    ElseIf Now.ToString("MM/dd/yyyy HH:mm:ss ") >= Now.ToString("MM/dd/yyyy") & " 22:00:00 " And Now.ToString("MM/dd/yyyy HH:mm:ss ") < Now.ToString("MM/dd/yyyy") & " 22:30:00 " Then
                '                        .OVERTIME = CDec(4.5)
                '                    ElseIf Now.ToString("MM/dd/yyyy HH:mm:ss ") >= Now.ToString("MM/dd/yyyy") & " 22:30:00 " And Now.ToString("MM/dd/yyyy HH:mm:ss ") < Now.ToString("MM/dd/yyyy") & " 23:00:00 " Then
                '                        .OVERTIME = CDec(5)
                '                    ElseIf Now.ToString("MM/dd/yyyy HH:mm:ss ") >= Now.ToString("MM/dd/yyyy") & " 23:00:00 " And Now.ToString("MM/dd/yyyy HH:mm:ss ") < Now.ToString("MM/dd/yyyy") & " 23:30:00 " Then
                '                        .OVERTIME = CDec(5.5)
                '                    ElseIf Now.ToString("MM/dd/yyyy HH:mm:ss ") >= Now.ToString("MM/dd/yyyy") & " 23:30:00 " And Now.ToString("MM/dd/yyyy HH:mm:ss ") <= Now.ToString("MM/dd/yyyy") & " 23:59:59 " Then
                '                        .OVERTIME = CDec(6)
                '                    Else
                '                        .OVERTIME = CDec(0)
                '                    End If

                '                Else

                '                    If Now.ToString("MM/dd/yyyy HH:mm:ss ") >= Now.ToString("MM/dd/yyyy") & " 17:20:00 " And Now.ToString("MM/dd/yyyy HH:mm:ss ") < Now.ToString("MM/dd/yyyy") & " 18:45:00 " Then
                '                        .OVERTIME = CDec(1.5)
                '                    ElseIf Now.ToString("MM/dd/yyyy HH:mm:ss ") >= Now.ToString("MM/dd/yyyy") & " 18:45:00 " And Now.ToString("MM/dd/yyyy HH:mm:ss ") < Now.ToString("MM/dd/yyyy") & " 19:20:00 " Then
                '                        .OVERTIME = CDec(2.5)
                '                    ElseIf Now.ToString("MM/dd/yyyy HH:mm:ss ") >= Now.ToString("MM/dd/yyyy") & " 19:20:00 " And Now.ToString("MM/dd/yyyy HH:mm:ss ") < Now.ToString("MM/dd/yyyy") & " 19:45:00 " Then
                '                        .OVERTIME = CDec(3.5)
                '                    ElseIf Now.ToString("MM/dd/yyyy HH:mm:ss ") >= Now.ToString("MM/dd/yyyy") & " 19:45:00 " And Now.ToString("MM/dd/yyyy HH:mm:ss ") < Now.ToString("MM/dd/yyyy") & " 20:45:00 " Then
                '                        .OVERTIME = CDec(4.5)
                '                    ElseIf Now.ToString("MM/dd/yyyy HH:mm:ss ") >= Now.ToString("MM/dd/yyyy") & " 20:45:00 " And Now.ToString("MM/dd/yyyy HH:mm:ss ") < Now.ToString("MM/dd/yyyy") & " 21:30:00 " Then
                '                        .OVERTIME = CDec(5.5)
                '                    ElseIf Now.ToString("MM/dd/yyyy HH:mm:ss ") >= Now.ToString("MM/dd/yyyy") & " 21:30:00 " And Now.ToString("MM/dd/yyyy HH:mm:ss ") < Now.ToString("MM/dd/yyyy") & " 22:00:00 " Then
                '                        .OVERTIME = CDec(6.5)
                '                    ElseIf Now.ToString("MM/dd/yyyy HH:mm:ss ") >= Now.ToString("MM/dd/yyyy") & " 22:00:00 " And Now.ToString("MM/dd/yyyy HH:mm:ss ") < Now.ToString("MM/dd/yyyy") & " 22:30:00 " Then
                '                        .OVERTIME = CDec(7.5)
                '                    ElseIf Now.ToString("MM/dd/yyyy HH:mm:ss ") >= Now.ToString("MM/dd/yyyy") & " 22:30:00 " And Now.ToString("MM/dd/yyyy HH:mm:ss ") < Now.ToString("MM/dd/yyyy") & " 23:00:00 " Then
                '                        .OVERTIME = CDec(8.5)
                '                    ElseIf Now.ToString("MM/dd/yyyy HH:mm:ss ") >= Now.ToString("MM/dd/yyyy") & " 23:00:00 " And Now.ToString("MM/dd/yyyy HH:mm:ss ") < Now.ToString("MM/dd/yyyy") & " 23:30:00 " Then
                '                        .OVERTIME = CDec(9.5)
                '                    ElseIf Now.ToString("MM/dd/yyyy HH:mm:ss ") >= Now.ToString("MM/dd/yyyy") & " 23:30:00 " And Now.ToString("MM/dd/yyyy HH:mm:ss ") <= Now.ToString("MM/dd/yyyy") & " 23:59:59 " Then
                '                        .OVERTIME = CDec(10.5)
                '                    Else
                '                        .OVERTIME = CDec(0)
                '                    End If
                '                End If
                '            Case "6"
                '                If oStaff.GetData(sKDSTAFF).POSITION = "STAFF OFFICE" Then

                '                    If Now.ToString("MM/dd/yyyy HH:mm:ss ") >= Now.ToString("MM/dd/yyyy") & " 13:50:00 " And Now.ToString("MM/dd/yyyy HH:mm:ss ") < Now.ToString("MM/dd/yyyy") & " 14:20:00 " Then
                '                        .OVERTIME = CDec(1)
                '                    ElseIf Now.ToString("MM/dd/yyyy HH:mm:ss ") >= Now.ToString("MM/dd/yyyy") & " 14:20:00 " And Now.ToString("MM/dd/yyyy HH:mm:ss ") < Now.ToString("MM/dd/yyyy") & " 14:45:00 " Then
                '                        .OVERTIME = CDec(1.5)
                '                    ElseIf Now.ToString("MM/dd/yyyy HH:mm:ss ") >= Now.ToString("MM/dd/yyyy") & " 14:45:00 " And Now.ToString("MM/dd/yyyy HH:mm:ss ") < Now.ToString("MM/dd/yyyy") & " 15:45:00 " Then
                '                        .OVERTIME = CDec(2)
                '                    ElseIf Now.ToString("MM/dd/yyyy HH:mm:ss ") >= Now.ToString("MM/dd/yyyy") & " 15:45:00 " And Now.ToString("MM/dd/yyyy HH:mm:ss ") < Now.ToString("MM/dd/yyyy") & " 16:20:00 " Then
                '                        .OVERTIME = CDec(2.5)
                '                    ElseIf Now.ToString("MM/dd/yyyy HH:mm:ss ") >= Now.ToString("MM/dd/yyyy") & " 16:20:00 " And Now.ToString("MM/dd/yyyy HH:mm:ss ") < Now.ToString("MM/dd/yyyy") & " 16:45:00 " Then
                '                        .OVERTIME = CDec(3)
                '                    ElseIf Now.ToString("MM/dd/yyyy HH:mm:ss ") >= Now.ToString("MM/dd/yyyy") & " 16:45:00 " And Now.ToString("MM/dd/yyyy HH:mm:ss ") < Now.ToString("MM/dd/yyyy") & " 17:45:00 " Then
                '                        .OVERTIME = CDec(3.5)
                '                    ElseIf Now.ToString("MM/dd/yyyy HH:mm:ss ") >= Now.ToString("MM/dd/yyyy") & " 17:45:00 " And Now.ToString("MM/dd/yyyy HH:mm:ss ") < Now.ToString("MM/dd/yyyy") & " 18:30:00 " Then
                '                        .OVERTIME = CDec(4)
                '                    ElseIf Now.ToString("MM/dd/yyyy HH:mm:ss ") >= Now.ToString("MM/dd/yyyy") & " 18:30:00 " And Now.ToString("MM/dd/yyyy HH:mm:ss ") < Now.ToString("MM/dd/yyyy") & " 19:00:00 " Then
                '                        .OVERTIME = CDec(4.5)
                '                    ElseIf Now.ToString("MM/dd/yyyy HH:mm:ss ") >= Now.ToString("MM/dd/yyyy") & " 19:00:00 " And Now.ToString("MM/dd/yyyy HH:mm:ss ") < Now.ToString("MM/dd/yyyy") & " 19:30:00 " Then
                '                        .OVERTIME = CDec(5)
                '                    ElseIf Now.ToString("MM/dd/yyyy HH:mm:ss ") >= Now.ToString("MM/dd/yyyy") & " 19:30:00 " And Now.ToString("MM/dd/yyyy HH:mm:ss ") < Now.ToString("MM/dd/yyyy") & " 20:00:00 " Then
                '                        .OVERTIME = CDec(5.5)
                '                    ElseIf Now.ToString("MM/dd/yyyy HH:mm:ss ") >= Now.ToString("MM/dd/yyyy") & " 20:00:00 " And Now.ToString("MM/dd/yyyy HH:mm:ss ") < Now.ToString("MM/dd/yyyy") & " 20:30:00 " Then
                '                        .OVERTIME = CDec(6)
                '                    ElseIf Now.ToString("MM/dd/yyyy HH:mm:ss ") >= Now.ToString("MM/dd/yyyy") & " 20:30:00 " And Now.ToString("MM/dd/yyyy HH:mm:ss ") < Now.ToString("MM/dd/yyyy") & " 21:00:00 " Then
                '                        .OVERTIME = CDec(6.5)

                '                    ElseIf Now.ToString("MM/dd/yyyy HH:mm:ss ") >= Now.ToString("MM/dd/yyyy") & " 21:00:00 " And Now.ToString("MM/dd/yyyy HH:mm:ss ") < Now.ToString("MM/dd/yyyy") & " 21:30:00 " Then
                '                        .OVERTIME = CDec(7)
                '                    ElseIf Now.ToString("MM/dd/yyyy HH:mm:ss ") >= Now.ToString("MM/dd/yyyy") & " 21:30:00 " And Now.ToString("MM/dd/yyyy HH:mm:ss ") < Now.ToString("MM/dd/yyyy") & " 22:00:00 " Then
                '                        .OVERTIME = CDec(7.5)
                '                    ElseIf Now.ToString("MM/dd/yyyy HH:mm:ss ") >= Now.ToString("MM/dd/yyyy") & " 22:00:00 " And Now.ToString("MM/dd/yyyy HH:mm:ss ") < Now.ToString("MM/dd/yyyy") & " 22:30:00 " Then
                '                        .OVERTIME = CDec(8)
                '                    ElseIf Now.ToString("MM/dd/yyyy HH:mm:ss ") >= Now.ToString("MM/dd/yyyy") & " 22:30:00 " And Now.ToString("MM/dd/yyyy HH:mm:ss ") < Now.ToString("MM/dd/yyyy") & " 23:00:00 " Then
                '                        .OVERTIME = CDec(8.5)
                '                    ElseIf Now.ToString("MM/dd/yyyy HH:mm:ss ") >= Now.ToString("MM/dd/yyyy") & " 23:00:00 " And Now.ToString("MM/dd/yyyy HH:mm:ss ") < Now.ToString("MM/dd/yyyy") & " 23:30:00 " Then
                '                        .OVERTIME = CDec(9)
                '                    ElseIf Now.ToString("MM/dd/yyyy HH:mm:ss ") >= Now.ToString("MM/dd/yyyy") & " 23:30:00 " And Now.ToString("MM/dd/yyyy HH:mm:ss ") <= Now.ToString("MM/dd/yyyy") & " 23:59:59 " Then
                '                        .OVERTIME = CDec(9.5)
                '                    Else
                '                        .OVERTIME = CDec(0)
                '                    End If

                '                Else

                '                    If Now.ToString("MM/dd/yyyy HH:mm:ss ") >= Now.ToString("MM/dd/yyyy") & " 13:50:00 " And Now.ToString("MM/dd/yyyy HH:mm:ss ") < Now.ToString("MM/dd/yyyy") & " 14:45:00 " Then
                '                        .OVERTIME = CDec(1.5)
                '                    ElseIf Now.ToString("MM/dd/yyyy HH:mm:ss ") >= Now.ToString("MM/dd/yyyy") & " 14:45:00 " And Now.ToString("MM/dd/yyyy HH:mm:ss ") < Now.ToString("MM/dd/yyyy") & " 15:45:00 " Then
                '                        .OVERTIME = CDec(2.5)
                '                    ElseIf Now.ToString("MM/dd/yyyy HH:mm:ss ") >= Now.ToString("MM/dd/yyyy") & " 15:45:00 " And Now.ToString("MM/dd/yyyy HH:mm:ss ") < Now.ToString("MM/dd/yyyy") & " 16:20:00 " Then
                '                        .OVERTIME = CDec(3.5)
                '                    ElseIf Now.ToString("MM/dd/yyyy HH:mm:ss ") >= Now.ToString("MM/dd/yyyy") & " 16:20:00 " And Now.ToString("MM/dd/yyyy HH:mm:ss ") < Now.ToString("MM/dd/yyyy") & " 16:45:00 " Then
                '                        .OVERTIME = CDec(4.5)
                '                    ElseIf Now.ToString("MM/dd/yyyy HH:mm:ss ") >= Now.ToString("MM/dd/yyyy") & " 16:45:00 " And Now.ToString("MM/dd/yyyy HH:mm:ss ") < Now.ToString("MM/dd/yyyy") & " 17:45:00 " Then
                '                        .OVERTIME = CDec(5.5)
                '                    ElseIf Now.ToString("MM/dd/yyyy HH:mm:ss ") >= Now.ToString("MM/dd/yyyy") & " 17:45:00 " And Now.ToString("MM/dd/yyyy HH:mm:ss ") < Now.ToString("MM/dd/yyyy") & " 18:30:00 " Then
                '                        .OVERTIME = CDec(6.5)
                '                    ElseIf Now.ToString("MM/dd/yyyy HH:mm:ss ") >= Now.ToString("MM/dd/yyyy") & " 18:30:00 " And Now.ToString("MM/dd/yyyy HH:mm:ss ") < Now.ToString("MM/dd/yyyy") & " 19:00:00 " Then
                '                        .OVERTIME = CDec(7.5)
                '                    ElseIf Now.ToString("MM/dd/yyyy HH:mm:ss ") >= Now.ToString("MM/dd/yyyy") & " 19:00:00 " And Now.ToString("MM/dd/yyyy HH:mm:ss ") < Now.ToString("MM/dd/yyyy") & " 19:30:00 " Then
                '                        .OVERTIME = CDec(8.5)
                '                    ElseIf Now.ToString("MM/dd/yyyy HH:mm:ss ") >= Now.ToString("MM/dd/yyyy") & " 19:30:00 " And Now.ToString("MM/dd/yyyy HH:mm:ss ") < Now.ToString("MM/dd/yyyy") & " 20:00:00 " Then
                '                        .OVERTIME = CDec(9.5)
                '                    ElseIf Now.ToString("MM/dd/yyyy HH:mm:ss ") >= Now.ToString("MM/dd/yyyy") & " 20:00:00 " And Now.ToString("MM/dd/yyyy HH:mm:ss ") < Now.ToString("MM/dd/yyyy") & " 20:30:00 " Then
                '                        .OVERTIME = CDec(10.5)
                '                    ElseIf Now.ToString("MM/dd/yyyy HH:mm:ss ") >= Now.ToString("MM/dd/yyyy") & " 20:30:00 " And Now.ToString("MM/dd/yyyy HH:mm:ss ") < Now.ToString("MM/dd/yyyy") & " 21:00:00 " Then
                '                        .OVERTIME = CDec(11.5)

                '                    ElseIf Now.ToString("MM/dd/yyyy HH:mm:ss ") >= Now.ToString("MM/dd/yyyy") & " 21:00:00 " And Now.ToString("MM/dd/yyyy HH:mm:ss ") < Now.ToString("MM/dd/yyyy") & " 21:30:00 " Then
                '                        .OVERTIME = CDec(12.5)
                '                    ElseIf Now.ToString("MM/dd/yyyy HH:mm:ss ") >= Now.ToString("MM/dd/yyyy") & " 21:30:00 " And Now.ToString("MM/dd/yyyy HH:mm:ss ") < Now.ToString("MM/dd/yyyy") & " 22:00:00 " Then
                '                        .OVERTIME = CDec(13.5)
                '                    ElseIf Now.ToString("MM/dd/yyyy HH:mm:ss ") >= Now.ToString("MM/dd/yyyy") & " 22:00:00 " And Now.ToString("MM/dd/yyyy HH:mm:ss ") < Now.ToString("MM/dd/yyyy") & " 22:30:00 " Then
                '                        .OVERTIME = CDec(14.5)
                '                    ElseIf Now.ToString("MM/dd/yyyy HH:mm:ss ") >= Now.ToString("MM/dd/yyyy") & " 22:30:00 " And Now.ToString("MM/dd/yyyy HH:mm:ss ") < Now.ToString("MM/dd/yyyy") & " 23:00:00 " Then
                '                        .OVERTIME = CDec(15.5)
                '                    ElseIf Now.ToString("MM/dd/yyyy HH:mm:ss ") >= Now.ToString("MM/dd/yyyy") & " 23:00:00 " And Now.ToString("MM/dd/yyyy HH:mm:ss ") < Now.ToString("MM/dd/yyyy") & " 23:30:00 " Then
                '                        .OVERTIME = CDec(16.5)
                '                    ElseIf Now.ToString("MM/dd/yyyy HH:mm:ss ") >= Now.ToString("MM/dd/yyyy") & " 23:30:00 " And Now.ToString("MM/dd/yyyy HH:mm:ss ") <= Now.ToString("MM/dd/yyyy") & " 23:59:59 " Then
                '                        .OVERTIME = CDec(17.5)
                '                    Else
                '                        .OVERTIME = CDec(0)
                '                    End If
                '                End If

                '        End Select
                '    Else
                '        .OVERTIME = CDec(0)
                '    End If

                'Catch ex As Exception
                '    .OVERTIME = CDec(0)
                'End Try

                Try
                    Dim sUML As Decimal = oAbsen.GetData(sKDSTAFF).FirstOrDefault(Function(x) x.DATE.ToString("yyyyMMdd") = Now.ToString("yyyyMMdd")).UML
                    Dim day As Integer = Date.Today.DayOfWeek
                    Dim time1 As DateTime = Now.ToString("MM/dd/yyyy HH:mm:ss ")
                    Dim time2 As DateTime
                    Select Case day
                        Case "1", "2", "3", "4", "5"
                            time2 = Now.ToString("MM/dd/yyyy") & " 18:00:00 "
                        Case "6"
                            time2 = Now.ToString("MM/dd/yyyy") & " 15:00:00 "
                    End Select

                    If sUML = 0 Or sUML > 0 Then
                        If time1 >= time2 Then
                            .UML = CDec(1)
                        Else
                            .UML = CDec(0)
                        End If
                    Else
                        .UML = CDec(0)
                    End If

                Catch ex As Exception
                    .UML = CDec(0)

                End Try

            End With

            If ds.KDABSEN = String.Empty Then
                Try
                    fn_Save = oAbsen.InsertData(ds,sKDSite)
                Catch ex As Exception
                    MsgBox("Simpan Data : " & vbCrLf & ex.Message, MsgBoxStyle.Exclamation, Me.Text)
                End Try
            ElseIf ds.KDABSEN <> String.Empty Then
                Try
                    fn_Save = oAbsen.UpdateData(ds)
                Catch ex As Exception
                    MsgBox("Simpan Data : " & vbCrLf & ex.Message, MsgBoxStyle.Exclamation, Me.Text)
                End Try
            End If
        Catch oErr As Exception
            MsgBox("Simpan Data : " & vbCrLf & oErr.Message, MsgBoxStyle.Exclamation, Me.Text)
            fn_Save = False
        End Try
    End Function




    Private Sub btnBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBack.Click
        Me.Close()
    End Sub

    Private Sub Identification_Closed(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Closed
        CancelCaptureAndCloseReader(AddressOf Me.OnCaptured)
    End Sub

#Region "SendMessage"
    Public Enum Action
        SendMessage
    End Enum
    Private Delegate Sub SendMessageCallback(ByVal action As Action, ByVal payload As String)
    Private Sub SendMessage(ByVal action As Action, ByVal payload As String)
        On Error Resume Next

        If Me.txtIdentify.InvokeRequired Then
            Dim d As New SendMessageCallback(AddressOf SendMessage)
            Me.Invoke(d, New Object() {action, payload})
        Else
            Select Case action
                Case action.SendMessage
                    txtIdentify.Text = payload
                    txtIdentify.SelectionStart = txtIdentify.TextLength
                    txtIdentify.ScrollToCaret()
            End Select
        End If
    End Sub
#End Region

#Region "Reader"
    Dim arrFmds As New List(Of Fmd)
    Private fmds As Dictionary(Of Fmd, String) = New Dictionary(Of Fmd, String)
    Public Property Reset() As Boolean
        Get
            Return _reset
        End Get
        Set(ByVal value As Boolean)
            _reset = value
        End Set
    End Property
    Private _reset As Boolean
    Public Property CurrentReader() As Reader
        Get
            Return _currentReader
        End Get
        Set(ByVal value As Reader)
            _currentReader = value
            'SendMessage(Action.UpdateReaderState, value)
        End Set
    End Property
    Private _currentReader As Reader

    Public Function OpenReader() As Boolean

        _currentReader = ReaderCollection.GetReaders().FirstOrDefault()

        Reset = False
        Dim result As Constants.ResultCode = Constants.ResultCode.DP_DEVICE_FAILURE

        result = _currentReader.Open(Constants.CapturePriority.DP_PRIORITY_COOPERATIVE)

        If result <> Constants.ResultCode.DP_SUCCESS Then
            MessageBox.Show("Error:  " & result.ToString())
            Reset = True
            Return False
        End If

        Return True
    End Function
    Public Function StartCaptureAsync(ByVal OnCaptured As Reader.CaptureCallback) As Boolean
        AddHandler _currentReader.On_Captured, OnCaptured

        If Not CaptureFingerAsync() Then
            Return False
        End If

        Return True
    End Function
    Public Function CaptureFingerAsync() As Boolean
        Try
            GetStatus()

            Dim captureResult = _currentReader.CaptureAsync(Formats.Fid.ANSI, _
                                                   CaptureProcessing.DP_IMG_PROC_DEFAULT, _
                                                    _currentReader.Capabilities.Resolutions(0))

            If captureResult <> ResultCode.DP_SUCCESS Then
                Reset = True
                Throw New Exception("" + captureResult.ToString())
            End If

            Return True
        Catch ex As Exception
            MessageBox.Show("Error:  " & ex.Message)
            Return False
        End Try
    End Function
    Public Sub GetStatus()
        Dim result = _currentReader.GetStatus()

        If (result <> ResultCode.DP_SUCCESS) Then
            If CurrentReader IsNot Nothing Then
                Reset = True
                Throw New Exception("" & result.ToString())
            End If
        End If

        If (_currentReader.Status.Status = ReaderStatuses.DP_STATUS_BUSY) Then
            Thread.Sleep(50)
        ElseIf (_currentReader.Status.Status = ReaderStatuses.DP_STATUS_NEED_CALIBRATION) Then
            _currentReader.Calibrate()
        ElseIf (_currentReader.Status.Status <> ReaderStatuses.DP_STATUS_READY) Then
            Throw New Exception("Reader Status - " & CurrentReader.Status.Status.ToString())
        End If
    End Sub
    Public Function CheckCaptureResult(ByVal captureResult As CaptureResult) As Boolean
        If captureResult.Data Is Nothing Then
            If captureResult.ResultCode <> Constants.ResultCode.DP_SUCCESS Then
                Reset = True
                Throw New Exception("" & captureResult.ResultCode.ToString())
            End If

            If captureResult.Quality <> Constants.CaptureQuality.DP_QUALITY_CANCELED Then
                Throw New Exception("Quality - " & captureResult.Quality.ToString())
            End If
            Return False
        End If
        Return True
    End Function
    Public Function CreateBitmap(ByVal bytes As [Byte](), ByVal width As Integer, ByVal height As Integer) As Bitmap
        Dim rgbBytes As Byte() = New Byte(bytes.Length * 3 - 1) {}

        For i As Integer = 0 To bytes.Length - 1
            rgbBytes((i * 3)) = bytes(i)
            rgbBytes((i * 3) + 1) = bytes(i)
            rgbBytes((i * 3) + 2) = bytes(i)
        Next
        Dim bmp As New Bitmap(width, height, PixelFormat.Format24bppRgb)

        Dim data As BitmapData = bmp.LockBits(New Rectangle(0, 0, bmp.Width, bmp.Height), ImageLockMode.[WriteOnly], PixelFormat.Format24bppRgb)

        For i As Integer = 0 To bmp.Height - 1
            Dim p As New IntPtr(data.Scan0.ToInt64() + data.Stride * i)
            System.Runtime.InteropServices.Marshal.Copy(rgbBytes, i * bmp.Width * 3, p, bmp.Width * 3)
        Next

        bmp.UnlockBits(data)

        Return bmp
    End Function
    Public Sub CancelCaptureAndCloseReader(ByVal OnCaptured As Reader.CaptureCallback)
        If _currentReader IsNot Nothing Then
            ' Dispose of reader handle and unhook reader events.
            CurrentReader.Dispose()

            If (Reset) Then
                CurrentReader = Nothing
            End If
        End If
    End Sub

    Private Sub Timer_Clock_Tick(sender As Object, e As EventArgs) Handles Timer_Clock.Tick
        ' Update clock label Format (HH:MM:SS)
        Me.Lbl_Clock.Text = Now.ToString("HH:mm:ss")
        Me.Lbl_Date.Text = Now.ToLongDateString
    End Sub



#End Region
End Class