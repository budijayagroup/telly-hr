﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmStaff
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.barManager = New DevExpress.XtraBars.BarManager(Me.components)
        Me.barTop = New DevExpress.XtraBars.Bar()
        Me.btnSaveNew = New DevExpress.XtraBars.BarButtonItem()
        Me.btnSaveClose = New DevExpress.XtraBars.BarButtonItem()
        Me.btnClose = New DevExpress.XtraBars.BarButtonItem()
        Me.barDockControlTop = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlBottom = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlLeft = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlRight = New DevExpress.XtraBars.BarDockControl()
        Me.progressBarSave = New DevExpress.XtraEditors.Repository.RepositoryItemMarqueeProgressBar()
        Me.progressSave = New DevExpress.XtraEditors.Repository.RepositoryItemMarqueeProgressBar()
        Me.txtNAME_DISPLAY = New DevExpress.XtraEditors.TextEdit()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.txtMessage = New System.Windows.Forms.TextBox()
        Me.picFinger = New System.Windows.Forms.PictureBox()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.btn0 = New DevExpress.XtraEditors.SimpleButton()
        Me.btn1 = New DevExpress.XtraEditors.SimpleButton()
        Me.btn2 = New DevExpress.XtraEditors.SimpleButton()
        Me.btn3 = New DevExpress.XtraEditors.SimpleButton()
        Me.btn4 = New DevExpress.XtraEditors.SimpleButton()
        Me.btn5 = New DevExpress.XtraEditors.SimpleButton()
        Me.btn6 = New DevExpress.XtraEditors.SimpleButton()
        Me.btn7 = New DevExpress.XtraEditors.SimpleButton()
        Me.btn8 = New DevExpress.XtraEditors.SimpleButton()
        Me.btn9 = New DevExpress.XtraEditors.SimpleButton()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.txtKDSTAFF = New DevExpress.XtraEditors.TextEdit()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Panel3 = New System.Windows.Forms.Panel()
        CType(Me.barManager,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.progressBarSave,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.progressSave,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.txtNAME_DISPLAY.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.picFinger,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.txtKDSTAFF.Properties,System.ComponentModel.ISupportInitialize).BeginInit
        Me.Panel1.SuspendLayout
        Me.Panel2.SuspendLayout
        Me.Panel3.SuspendLayout
        Me.SuspendLayout
        '
        'barManager
        '
        Me.barManager.AllowQuickCustomization = false
        Me.barManager.Bars.AddRange(New DevExpress.XtraBars.Bar() {Me.barTop})
        Me.barManager.DockControls.Add(Me.barDockControlTop)
        Me.barManager.DockControls.Add(Me.barDockControlBottom)
        Me.barManager.DockControls.Add(Me.barDockControlLeft)
        Me.barManager.DockControls.Add(Me.barDockControlRight)
        Me.barManager.Form = Me
        Me.barManager.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.btnSaveNew, Me.btnClose, Me.btnSaveClose})
        Me.barManager.MainMenu = Me.barTop
        Me.barManager.MaxItemId = 8
        Me.barManager.RepositoryItems.AddRange(New DevExpress.XtraEditors.Repository.RepositoryItem() {Me.progressBarSave, Me.progressSave})
        '
        'barTop
        '
        Me.barTop.BarName = "Main menu"
        Me.barTop.DockCol = 0
        Me.barTop.DockRow = 0
        Me.barTop.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom
        Me.barTop.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.btnSaveNew), New DevExpress.XtraBars.LinkPersistInfo(Me.btnSaveClose), New DevExpress.XtraBars.LinkPersistInfo(Me.btnClose)})
        Me.barTop.OptionsBar.DrawDragBorder = false
        Me.barTop.OptionsBar.MultiLine = true
        Me.barTop.OptionsBar.UseWholeRow = true
        Me.barTop.Text = "Main menu"
        '
        'btnSaveNew
        '
        Me.btnSaveNew.Caption = "F2 - Save && New"
        Me.btnSaveNew.Id = 2
        Me.btnSaveNew.Name = "btnSaveNew"
        '
        'btnSaveClose
        '
        Me.btnSaveClose.Caption = "F3 - Save && Close"
        Me.btnSaveClose.Id = 5
        Me.btnSaveClose.Name = "btnSaveClose"
        '
        'btnClose
        '
        Me.btnClose.Caption = "F12 - Close"
        Me.btnClose.Id = 3
        Me.btnClose.Name = "btnClose"
        '
        'barDockControlTop
        '
        Me.barDockControlTop.CausesValidation = false
        Me.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top
        Me.barDockControlTop.Location = New System.Drawing.Point(0, 0)
        Me.barDockControlTop.Size = New System.Drawing.Size(515, 0)
        '
        'barDockControlBottom
        '
        Me.barDockControlBottom.CausesValidation = false
        Me.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.barDockControlBottom.Location = New System.Drawing.Point(0, 523)
        Me.barDockControlBottom.Size = New System.Drawing.Size(515, 22)
        '
        'barDockControlLeft
        '
        Me.barDockControlLeft.CausesValidation = false
        Me.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left
        Me.barDockControlLeft.Location = New System.Drawing.Point(0, 0)
        Me.barDockControlLeft.Size = New System.Drawing.Size(0, 523)
        '
        'barDockControlRight
        '
        Me.barDockControlRight.CausesValidation = false
        Me.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right
        Me.barDockControlRight.Location = New System.Drawing.Point(515, 0)
        Me.barDockControlRight.Size = New System.Drawing.Size(0, 523)
        '
        'progressBarSave
        '
        Me.progressBarSave.Name = "progressBarSave"
        Me.progressBarSave.Stopped = true
        '
        'progressSave
        '
        Me.progressSave.Name = "progressSave"
        Me.progressSave.Paused = true
        '
        'txtNAME_DISPLAY
        '
        Me.txtNAME_DISPLAY.Location = New System.Drawing.Point(92, 53)
        Me.txtNAME_DISPLAY.MenuManager = Me.barManager
        Me.txtNAME_DISPLAY.Name = "txtNAME_DISPLAY"
        Me.txtNAME_DISPLAY.Size = New System.Drawing.Size(360, 20)
        Me.txtNAME_DISPLAY.TabIndex = 1
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(52, 56)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(34, 13)
        Me.LabelControl1.TabIndex = 6
        Me.LabelControl1.Text = "Nama :"
        '
        'txtMessage
        '
        Me.txtMessage.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!)
        Me.txtMessage.Location = New System.Drawing.Point(246, 11)
        Me.txtMessage.Multiline = true
        Me.txtMessage.Name = "txtMessage"
        Me.txtMessage.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtMessage.Size = New System.Drawing.Size(237, 156)
        Me.txtMessage.TabIndex = 11
        Me.txtMessage.TabStop = false
        '
        'picFinger
        '
        Me.picFinger.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.picFinger.Location = New System.Drawing.Point(18, 11)
        Me.picFinger.Name = "picFinger"
        Me.picFinger.Size = New System.Drawing.Size(204, 156)
        Me.picFinger.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.picFinger.TabIndex = 12
        Me.picFinger.TabStop = false
        '
        'btnCancel
        '
        Me.btnCancel.Enabled = false
        Me.btnCancel.Location = New System.Drawing.Point(411, 173)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(72, 20)
        Me.btnCancel.TabIndex = 13
        Me.btnCancel.TabStop = false
        Me.btnCancel.Text = "Cancel"
        '
        'btn0
        '
        Me.btn0.Appearance.ForeColor = System.Drawing.Color.White
        Me.btn0.Appearance.Options.UseForeColor = true
        Me.btn0.Appearance.Options.UseTextOptions = true
        Me.btn0.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.btn0.Location = New System.Drawing.Point(42, 92)
        Me.btn0.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat
        Me.btn0.LookAndFeel.UseDefaultLookAndFeel = false
        Me.btn0.Name = "btn0"
        Me.btn0.Size = New System.Drawing.Size(75, 71)
        Me.btn0.TabIndex = 18
        Me.btn0.TabStop = false
        Me.btn0.Text = "Jempol Kanan"
        '
        'btn1
        '
        Me.btn1.Appearance.ForeColor = System.Drawing.Color.White
        Me.btn1.Appearance.Options.UseForeColor = true
        Me.btn1.Appearance.Options.UseTextOptions = true
        Me.btn1.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.btn1.Location = New System.Drawing.Point(123, 92)
        Me.btn1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat
        Me.btn1.LookAndFeel.UseDefaultLookAndFeel = false
        Me.btn1.Name = "btn1"
        Me.btn1.Size = New System.Drawing.Size(75, 71)
        Me.btn1.TabIndex = 18
        Me.btn1.TabStop = false
        Me.btn1.Text = "Telunjuk Kanan"
        '
        'btn2
        '
        Me.btn2.Appearance.ForeColor = System.Drawing.Color.White
        Me.btn2.Appearance.Options.UseForeColor = true
        Me.btn2.Appearance.Options.UseTextOptions = true
        Me.btn2.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.btn2.Location = New System.Drawing.Point(204, 92)
        Me.btn2.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat
        Me.btn2.LookAndFeel.UseDefaultLookAndFeel = false
        Me.btn2.Name = "btn2"
        Me.btn2.Size = New System.Drawing.Size(75, 71)
        Me.btn2.TabIndex = 18
        Me.btn2.TabStop = false
        Me.btn2.Text = "Tengah Kanan"
        '
        'btn3
        '
        Me.btn3.Appearance.ForeColor = System.Drawing.Color.White
        Me.btn3.Appearance.Options.UseForeColor = true
        Me.btn3.Appearance.Options.UseTextOptions = true
        Me.btn3.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.btn3.Location = New System.Drawing.Point(284, 92)
        Me.btn3.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat
        Me.btn3.LookAndFeel.UseDefaultLookAndFeel = false
        Me.btn3.Name = "btn3"
        Me.btn3.Size = New System.Drawing.Size(75, 71)
        Me.btn3.TabIndex = 18
        Me.btn3.TabStop = false
        Me.btn3.Text = "Manis Kanan"
        '
        'btn4
        '
        Me.btn4.Appearance.ForeColor = System.Drawing.Color.White
        Me.btn4.Appearance.Options.UseForeColor = true
        Me.btn4.Appearance.Options.UseTextOptions = true
        Me.btn4.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.btn4.Location = New System.Drawing.Point(366, 92)
        Me.btn4.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat
        Me.btn4.LookAndFeel.UseDefaultLookAndFeel = false
        Me.btn4.Name = "btn4"
        Me.btn4.Size = New System.Drawing.Size(75, 71)
        Me.btn4.TabIndex = 18
        Me.btn4.TabStop = false
        Me.btn4.Text = "Kelingking Kanan"
        '
        'btn5
        '
        Me.btn5.Appearance.ForeColor = System.Drawing.Color.White
        Me.btn5.Appearance.Options.UseForeColor = true
        Me.btn5.Appearance.Options.UseTextOptions = true
        Me.btn5.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.btn5.Location = New System.Drawing.Point(42, 14)
        Me.btn5.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat
        Me.btn5.LookAndFeel.UseDefaultLookAndFeel = false
        Me.btn5.Name = "btn5"
        Me.btn5.Size = New System.Drawing.Size(75, 71)
        Me.btn5.TabIndex = 18
        Me.btn5.TabStop = false
        Me.btn5.Text = "Jempol Kiri"
        '
        'btn6
        '
        Me.btn6.Appearance.ForeColor = System.Drawing.Color.White
        Me.btn6.Appearance.Options.UseForeColor = true
        Me.btn6.Appearance.Options.UseTextOptions = true
        Me.btn6.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.btn6.Location = New System.Drawing.Point(123, 14)
        Me.btn6.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat
        Me.btn6.LookAndFeel.UseDefaultLookAndFeel = false
        Me.btn6.Name = "btn6"
        Me.btn6.Size = New System.Drawing.Size(75, 71)
        Me.btn6.TabIndex = 18
        Me.btn6.TabStop = false
        Me.btn6.Text = "Telunjuk Kiri"
        '
        'btn7
        '
        Me.btn7.Appearance.ForeColor = System.Drawing.Color.White
        Me.btn7.Appearance.Options.UseForeColor = true
        Me.btn7.Appearance.Options.UseTextOptions = true
        Me.btn7.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.btn7.Location = New System.Drawing.Point(204, 14)
        Me.btn7.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat
        Me.btn7.LookAndFeel.UseDefaultLookAndFeel = false
        Me.btn7.Name = "btn7"
        Me.btn7.Size = New System.Drawing.Size(75, 71)
        Me.btn7.TabIndex = 18
        Me.btn7.TabStop = false
        Me.btn7.Text = "Tengah Kiri"
        '
        'btn8
        '
        Me.btn8.Appearance.ForeColor = System.Drawing.Color.White
        Me.btn8.Appearance.Options.UseForeColor = true
        Me.btn8.Appearance.Options.UseTextOptions = true
        Me.btn8.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.btn8.Location = New System.Drawing.Point(284, 14)
        Me.btn8.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat
        Me.btn8.LookAndFeel.UseDefaultLookAndFeel = false
        Me.btn8.Name = "btn8"
        Me.btn8.Size = New System.Drawing.Size(75, 71)
        Me.btn8.TabIndex = 18
        Me.btn8.TabStop = false
        Me.btn8.Text = "Manis Kiri"
        '
        'btn9
        '
        Me.btn9.Appearance.ForeColor = System.Drawing.Color.White
        Me.btn9.Appearance.Options.UseForeColor = true
        Me.btn9.Appearance.Options.UseTextOptions = true
        Me.btn9.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap
        Me.btn9.Location = New System.Drawing.Point(366, 14)
        Me.btn9.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat
        Me.btn9.LookAndFeel.UseDefaultLookAndFeel = false
        Me.btn9.Name = "btn9"
        Me.btn9.Size = New System.Drawing.Size(75, 71)
        Me.btn9.TabIndex = 18
        Me.btn9.TabStop = false
        Me.btn9.Text = "Kelingking Kiri"
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(25, 30)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(61, 13)
        Me.LabelControl3.TabIndex = 37
        Me.LabelControl3.Text = "Kode Staff  :"
        '
        'txtKDSTAFF
        '
        Me.txtKDSTAFF.Location = New System.Drawing.Point(92, 27)
        Me.txtKDSTAFF.MenuManager = Me.barManager
        Me.txtKDSTAFF.Name = "txtKDSTAFF"
        Me.txtKDSTAFF.Size = New System.Drawing.Size(360, 20)
        Me.txtKDSTAFF.TabIndex = 38
        '
        'Panel1
        '
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.txtKDSTAFF)
        Me.Panel1.Controls.Add(Me.txtNAME_DISPLAY)
        Me.Panel1.Controls.Add(Me.LabelControl3)
        Me.Panel1.Controls.Add(Me.LabelControl1)
        Me.Panel1.Location = New System.Drawing.Point(12, 12)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(492, 100)
        Me.Panel1.TabIndex = 43
        '
        'Panel2
        '
        Me.Panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel2.Controls.Add(Me.txtMessage)
        Me.Panel2.Controls.Add(Me.picFinger)
        Me.Panel2.Controls.Add(Me.btnCancel)
        Me.Panel2.Location = New System.Drawing.Point(12, 122)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(492, 204)
        Me.Panel2.TabIndex = 44
        '
        'Panel3
        '
        Me.Panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel3.Controls.Add(Me.btn8)
        Me.Panel3.Controls.Add(Me.btn0)
        Me.Panel3.Controls.Add(Me.btn5)
        Me.Panel3.Controls.Add(Me.btn9)
        Me.Panel3.Controls.Add(Me.btn1)
        Me.Panel3.Controls.Add(Me.btn4)
        Me.Panel3.Controls.Add(Me.btn6)
        Me.Panel3.Controls.Add(Me.btn2)
        Me.Panel3.Controls.Add(Me.btn3)
        Me.Panel3.Controls.Add(Me.btn7)
        Me.Panel3.Location = New System.Drawing.Point(12, 332)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(492, 185)
        Me.Panel3.TabIndex = 45
        '
        'frmStaff
        '
        Me.Appearance.BackColor = System.Drawing.Color.FromArgb(CType(CType(235,Byte),Integer), CType(CType(236,Byte),Integer), CType(CType(239,Byte),Integer))
        Me.Appearance.Options.UseBackColor = true
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6!, 13!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(515, 545)
        Me.Controls.Add(Me.Panel3)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.barDockControlLeft)
        Me.Controls.Add(Me.barDockControlRight)
        Me.Controls.Add(Me.barDockControlBottom)
        Me.Controls.Add(Me.barDockControlTop)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.KeyPreview = true
        Me.Name = "frmStaff"
        Me.ShowIcon = false
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        CType(Me.barManager,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.progressBarSave,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.progressSave,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.txtNAME_DISPLAY.Properties,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.picFinger,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.txtKDSTAFF.Properties,System.ComponentModel.ISupportInitialize).EndInit
        Me.Panel1.ResumeLayout(false)
        Me.Panel1.PerformLayout
        Me.Panel2.ResumeLayout(false)
        Me.Panel2.PerformLayout
        Me.Panel3.ResumeLayout(false)
        Me.ResumeLayout(false)
        Me.PerformLayout

End Sub
    Friend WithEvents barManager As DevExpress.XtraBars.BarManager
    Friend WithEvents barTop As DevExpress.XtraBars.Bar
    Friend WithEvents barDockControlTop As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlBottom As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlLeft As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlRight As DevExpress.XtraBars.BarDockControl
    Friend WithEvents btnSaveNew As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnClose As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents btnSaveClose As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents progressBarSave As DevExpress.XtraEditors.Repository.RepositoryItemMarqueeProgressBar
    Friend WithEvents progressSave As DevExpress.XtraEditors.Repository.RepositoryItemMarqueeProgressBar
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtNAME_DISPLAY As DevExpress.XtraEditors.TextEdit
    Private WithEvents txtMessage As System.Windows.Forms.TextBox
    Private WithEvents picFinger As System.Windows.Forms.PictureBox
    Private WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents btn0 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btn1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btn2 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btn4 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btn3 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btn9 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btn8 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btn7 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btn6 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents btn5 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents txtKDSTAFF As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Panel1 As Panel
    Friend WithEvents Panel2 As Panel
    Friend WithEvents Panel3 As Panel
End Class
