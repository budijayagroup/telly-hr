﻿Imports DataAccess
Imports UI.WIN.MAIN.My.Resources
Imports System.Linq
Imports System.Drawing
Imports System.Threading
Imports DevExpress.XtraSplashScreen

Public Class FrmImportExcel
    Implements ILanguage

    Private oFormMode As FORM_MODE = FORM_MODE.FORM_MODE_VIEW
    Private oStaff As New HumanResource.clsStaffJari
    Private oImport As New Setting.clsImport 
    Private sNoId As String
    
   
    
#Region "Function"
    Private Sub Me_Load() Handles Me.Load
       
        fn_LoadSecurity()

        Try
            grv.RestoreLayoutFromRegistry("HKEY_CURRENT_USER\Software\BUDIJAYAGROUP\MAIN\" & sUserID & "\" & Me.Text)
        Catch ex As Exception

        End Try
    End Sub
    Private Sub Me_FormClosed(sender As System.Object, e As System.Windows.Forms.FormClosedEventArgs) Handles MyBase.FormClosed
        Try
            grv.SaveLayoutToRegistry("HKEY_CURRENT_USER\Software\BUDIJAYAGROUP\MAIN\" & sUserID & "\" & Me.Text)
        Catch ex As Exception

        End Try
    End Sub
    Private Sub fn_LoadSecurity()
        Try
            Dim oOtority As New Setting.clsOtority
            Dim oUser As New Setting.clsUser

            Dim ds = (From x In oOtority.GetDataDetail
                      Join y In oUser.GetData
                     On x.KDOTORITY Equals y.KDOTORITY
                      Where x.MODUL = "EXCEL" _
                     And y.KDUSER = sUserID
                      Select x.ISADD, x.ISDELETE, x.ISPRINT, x.ISVIEW).FirstOrDefault

            Try
               
                picImport.Enabled = ds.ISADD
                picDelete.Enabled = ds.ISDELETE
                picPrint.Enabled  = ds.ISPRINT
                
                If ds.ISVIEW = True Then
                    picSave.Enabled = false
                    fn_LoadData()
                    fn_LoadLanguage()
                End If
            Catch oErr As Exception
                MsgBox(Statement.SecurityNotInstalled, MsgBoxStyle.Exclamation, Me.Text)
                picDelete.Enabled = False
              
            End Try
        Catch oErr As Exception
            MsgBox(Statement.SecurityNotInstalled, MsgBoxStyle.Exclamation, Me.Text)
        End Try
    End Sub
    Public Sub fn_LoadLanguage() Implements ILanguage.fn_LoadLanguage
        Try
            Me.Text = Import.TITLE 
            grv.Columns("KDSTAFF").Caption = Import.KDSTAFF
            grv.Columns("NAME_DISPLAY").Caption = Import.NAME_DISPLAY 
            grv.Columns("ISACTIVE").Caption = Import.ISACTIVE
            ColumnChooserToolStripMenuItem.Text = Caption.ColumnChooser

        Catch oErr As Exception
            MsgBox(Statement.ErrorStatement & vbCrLf & oErr.Message, MsgBoxStyle.Exclamation, Me.Text)
        End Try
    End Sub
    Private Sub fn_LoadData()
        Try
            Dim ds = From x In oImport.GetData _
                     Select x.KDSTAFF, x.NAME_DISPLAY, x.ISACTIVE
            grd.DataSource = ds.ToList

            fn_LoadFormatData()
        Catch oErr As Exception
            MsgBox(Statement.ErrorStatement & vbCrLf & oErr.Message, MsgBoxStyle.Exclamation, Me.Text)
        End Try
    End Sub

    Private Sub fn_LoadFormatData()
        For iLoop As Integer = 0 To grv.Columns.Count - 1
            If grv.Columns(iLoop).ColumnType.Name = "Decimal" Then
                grv.Columns(iLoop).DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric
                grv.Columns(iLoop).DisplayFormat.FormatString = "{0:n2}"
                grv.Columns(iLoop).AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far
            ElseIf grv.Columns(iLoop).ColumnType.Name = "DateTime" Then
                grv.Columns(iLoop).DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime
                grv.Columns(iLoop).DisplayFormat.FormatString = "{0:dd/MM/yyyy}"
            End If
        Next
        grv.Columns("ISACTIVE").Visible = False


    End Sub
    Private Sub ColumnChooserToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles ColumnChooserToolStripMenuItem.Click
        grv.ShowCustomization()
    End Sub
    Private Function fn_DeleteData(ByVal sKDSTAFF As String) As Boolean
        Try
            oImport.DeleteData(sKDSTAFF)
            fn_DeleteData = True
        Catch oErr As Exception
            fn_DeleteData = False
            MsgBox(Statement.ErrorStatement & vbCrLf & oErr.Message, MsgBoxStyle.Exclamation, Me.Text)
        End Try
    End Function
#End Region
#Region "Command Button"
    Private Sub frmMember_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Select Case e.KeyCode
     
            Case Keys.D
                If e.Alt = True And picDelete.Enabled = True Then
                    picDelete_Click
                End If
            Case Keys.I
                If e.AlT = True And picImport.Enabled  = True Then
                    picImport_Click 
                End If
            Case Keys.P 
                If e.AlT = True And picPrint.Enabled   = True Then
                    picPrint_Click
                End If
            Case Keys.R 
                If e.AlT = True And picRefresh.Enabled    = True Then
                    picRefresh_Click 
                End If 
                 
        End Select
    End Sub
    
    
    Private Sub picDelete_Click() Handles picDelete.Click
        If grv.GetFocusedRowCellValue("KDSTAFF") Is Nothing Then
            Exit Sub
        End If
        If MsgBox(Statement.DeleteQuestion, MsgBoxStyle.Exclamation + MsgBoxStyle.YesNo, Me.Text) = MsgBoxResult.No Then Exit Sub
        If fn_DeleteData(grv.GetFocusedRowCellValue("KDSTAFF")) = False Then
            MsgBox(Statement.DeleteFail, MsgBoxStyle.Exclamation, Me.Text)
            Exit Sub
        End If
        MsgBox(Statement.DeleteSuccess, MsgBoxStyle.Information, Me.Text)
        fn_LoadSecurity()
    End Sub


    Private Sub picImport_Click() Handles picImport.Click
        Dim MyConnection As System.Data.OleDb.OleDbConnection
        Dim DtSet As System.Data.DataSet
        Dim MyCommand As System.Data.OleDb.OleDbDataAdapter
        Dim cmdGetCountRow As System.Data.OleDb.OleDbCommand 
        Dim dReader As System.Data.OleDb.OleDbDataReader 

        Dim fBrowse As New OpenFileDialog
        With fBrowse
            .Filter = "Excel files(*.xlsx)|*.xlsx|All files (*.*)|*.*"
            .FilterIndex = 1
            .Title = "Import data from Excel file"
        End With
        If fBrowse.ShowDialog() = Windows.Forms.DialogResult.OK Then
            Dim fname As String
            fname = fBrowse.FileName
            MyConnection = New System.Data.OleDb.OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0; Data Source='" & fname & " '; " & "Extended Properties=Excel 8.0;")

            Try
                  SplashScreenManager.ShowForm(Me, GetType(frmLoading), True, True, False)
                  MyConnection.Open
                  cmdGetCountRow = New System.Data.OleDb.OleDbCommand("SELECT COUNT(KDSTAFF) AS COUNTRECORD From [Sheet1$]",MyConnection)
                  dReader = cmdGetCountRow.ExecuteReader
                  dReader.Read
            Catch ex As Exception

            Finally
                Thread.Sleep(1000)
                SplashScreenManager.CloseForm(False)

            End Try
            If MsgBox(Import.QSCOUNTREC & dReader("COUNTRECORD") & ". " & Import.QSIMPORT, MsgBoxStyle.Exclamation + MsgBoxStyle.YesNo, Me.Text) = MsgBoxResult.No Then Exit Sub
            MyConnection.Close()


            Try
                SplashScreenManager.ShowForm(Me, GetType(frmLoading), True, True, False)
                MyConnection.Open 
                MyCommand = New System.Data.OleDb.OleDbDataAdapter("select * from [Sheet1$]", MyConnection)
                DtSet = New System.Data.DataSet
                MyCommand.Fill(DtSet,"[Sheet1$]")
                grd.DataSource = DtSet.Tables(0)
                MyConnection.Close()

            Catch ex As Exception

            Finally
                Thread.Sleep(100)
                SplashScreenManager.CloseForm(False)
                picSave.Enabled = True
            End Try

        End If
    End Sub

   
 Private Function fn_save(ByVal sTYPE As Integer) As Boolean
       
        If sTYPE = 1 Then
            'Import to M_STAFF
            Try
                Dim arrDetail = oImport.GetStructureDetailList

                For i As Integer = 0 To grv.RowCount - 1
                    Dim dsDetail = oImport.GetStructureDetail

                    With dsDetail
                        .DATECREATED = Now
                        .DATEUPDATED = Now
                        .KDSTAFF = grv.GetRowCellValue(i, "KDSTAFF")
                        .NAME_DISPLAY = grv.GetRowCellValue(i, "NAME_DISPLAY")

                       End With
                    
                    arrDetail.Add(dsDetail)
                 TextEdit1.Text =  i + 1
                Next i
                Try
                    fn_Save = oImport.ImportData_Staff(arrDetail)
                    fn_Save = True
                Catch oErr As Exception
                     MsgBox(Statement.ErrorStatement & vbCrLf & oErr.Message, MsgBoxStyle.Exclamation, Me.Text)
                End Try
             
            fn_LoadSecurity  
        Catch oErr As Exception
            MsgBox(Statement.ErrorStatement & vbCrLf & oErr.Message, MsgBoxStyle.Exclamation, Me.Text)
            fn_Save = False
        End Try

     ElseIf sTYPE = 2 then
            'Import to M_STAFF_JARI
            Try
                Dim arrDetail = oStaff.GetStructureDetailList
                For  i As Integer  = 0 To grv.RowCount - 1
                    Dim dsDetail = oStaff.GetStructureDetail
                    With dsDetail
                       
                        .DATECREATED = Now
                        .DATEUPDATED = Now
                        .KDSTAFF = grv.GetRowCellValue(i, "KDSTAFF")
                        .NAME_DISPLAY = grv.GetRowCellValue(i, "NAME_DISPLAY")
                    
                       End With
                    arrDetail.Add(dsDetail)
                    TextEdit1.Text = i + 1
                Next i
                Try
                    fn_Save = oImport.ImportData_StaffJari(arrDetail)
                    fn_Save = True
                Catch oErr As Exception
                     MsgBox(Statement.ErrorStatement & vbCrLf & oErr.Message, MsgBoxStyle.Exclamation, Me.Text)
                End Try
             
           
        Catch oErr As Exception
            MsgBox(Statement.ErrorStatement & vbCrLf & oErr.Message, MsgBoxStyle.Exclamation, Me.Text)
            fn_Save = False
        End Try

     End if
    End Function

    Private Sub picSave_Click(sender As Object, e As EventArgs) Handles picSave.Click
      
          Try
            SplashScreenManager.ShowForm(Me, GetType(frmLoading), True, True, False)

            If  fn_Save(1) And fn_Save(2) Then
                MsgBox("Data berhasil diimport", MsgBoxStyle.Information, "Informasi")
            End If
        Catch ex As Exception

            Finally
                
                SplashScreenManager.CloseForm(False)

            End Try

    End Sub
   
  

    Private Sub picRefresh_Click() Handles picRefresh.Click
        fn_LoadSecurity 
    End Sub

    Private Sub picPrint_Click() Handles picPrint.Click

    End Sub

    



#End Region
End Class