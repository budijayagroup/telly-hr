﻿Imports DataAccess
Imports UI.WIN.MAIN.My.Resources
Imports System.Linq

Public Class frmPermitAbsen
#Region "Declaration"
    Private oFormMode As FORM_MODE = FORM_MODE.FORM_MODE_VIEW
    Private sNoId As String
    Private isLoad As Boolean = False
    Private oAbsen As New HumanResource.clsAbsen
#End Region
#Region "Function"
    Public Sub LoadMe(ByVal FormMode As Integer, Optional ByVal NoId As String = "")
        oFormMode = FormMode
        sNoId = NoId
    End Sub
    Private Sub Form_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        fn_ChangeFormState()
        fn_LoadLanguage()
        isLoad = True
    End Sub
    Public Sub fn_LoadLanguage()
        Try
            Me.Text = "Permit Absent"

            btnSaveNew.Caption = Caption.FormSaveNew
            btnSaveClose.Caption = Caption.FormSaveClose
            btnClose.Caption = Caption.FormClose
        Catch oErr As Exception
            MsgBox(Statement.ErrorStatement & vbCrLf & oErr.Message, MsgBoxStyle.Exclamation, Me.Text)
        End Try
    End Sub
    Private Sub Form_Closing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        sCode = txtKDABSEN.Text.Trim.ToUpper
    End Sub
    Private Sub fn_ChangeFormState()
        fn_LoadKDSTAFF()
        cbDESCRIPTION.Properties.Items.Clear()
        cbDESCRIPTION.Properties.Items.Add("IJIN")
        cbDESCRIPTION.Properties.Items.Add("ALPHA")
        cbDESCRIPTION.Properties.Items.Add("SURATDOKTER")
        cbDESCRIPTION.Properties.Items.Add("SAKIT")
        cbDESCRIPTION.Properties.Items.Add("H")
        cbDESCRIPTION.Properties.Items.Add("CUTI")

        Select Case oFormMode

            Case FORM_MODE.FORM_MODE_VIEW
                fn_ViewMode(True)
                fn_LoadData()
            Case FORM_MODE.FORM_MODE_ADD
                fn_ViewMode(False)
                fn_EmptyMe()
            Case FORM_MODE.FORM_MODE_EDIT
                fn_ViewMode(False)
                fn_LoadData()
            Case Else
                fn_ViewMode(True)
        End Select
    End Sub
    Private Sub fn_ViewMode(ByVal Status As Boolean)
        btnSaveNew.Enabled = Not Status
        btnSaveClose.Enabled = Not Status

        grdKDSTAFF.Properties.ReadOnly = Status
        cbDESCRIPTION.Properties.ReadOnly = Status
        deDATE.Properties.ReadOnly = Status
    End Sub
    Private Sub fn_EmptyMe()
        txtKDABSEN.Text = "<--- AUTO --->"
        cbDESCRIPTION.SelectedIndex = 0
        deDATE.DateTime = Now

    End Sub
    Private Sub fn_LoadData()
        Try
            Dim ds = oAbsen.GetData2(sNoId)
            With ds
                txtKDABSEN.Text = .KDABSEN
                deDATE.DateTime = .DATE
                grdKDSTAFF.EditValue = .KDSTAFF
                cbDESCRIPTION.Text = .DESCRIPTION
            End With
        Catch oErr As Exception
            MsgBox(Statement.ErrorStatement & vbCrLf & oErr.Message, MsgBoxStyle.Exclamation, Me.Text)
        End Try
    End Sub
    Private Function fn_Validate() As Boolean
        Try
            fn_Validate = True

            If grdKDSTAFF.Text = String.Empty Then
                grdKDSTAFF.ErrorIconAlignment = ErrorIconAlignment.MiddleRight
                grdKDSTAFF.ErrorText = Statement.ErrorRequired

                grdKDSTAFF.Focus()
                fn_Validate = False
                Exit Function
            End If


            If oFormMode = FORM_MODE.FORM_MODE_ADD Then
                If oAbsen.IsExist(deDATE.DateTime.ToString("dd/MM/yyyy"), grdKDSTAFF.EditValue) = True Then
                    deDATE.ErrorIconAlignment = ErrorIconAlignment.MiddleRight
                    deDATE.ErrorText = Statement.ErrorRegistered
                    deDATE.Focus()
                    fn_Validate = False
                    Exit Function
                End If
            Else
                If deDATE.DateTime.ToString("dd/MM/yyyy") <> oAbsen.GetData2(sNoId).DATE And grdKDSTAFF.EditValue <> oAbsen.GetData2(sNoId).KDSTAFF Then

                    If oAbsen.IsExist(deDATE.DateTime, grdKDSTAFF.EditValue) = True Then
                        deDATE.ErrorIconAlignment = ErrorIconAlignment.MiddleRight
                        deDATE.ErrorText = Statement.ErrorRegistered
                        deDATE.Focus()
                        fn_Validate = False
                        Exit Function
                    End If
                End If
            End If
        Catch oErr As Exception
            MsgBox(Statement.ErrorStatement & vbCrLf & oErr.Message, MsgBoxStyle.Exclamation, Me.Text)
        End Try
    End Function
    Private Function fn_Save() As Boolean
        Try
            ' ***** HEADER *****
            Dim ds = oAbsen.GetStructureHeader
            With ds
                .DATE = deDATE.DateTime
                .KDABSEN = sNoId
                .DESCRIPTION = cbDESCRIPTION.Text.Trim.ToUpper
                .KDSTAFF = IIf(String.IsNullOrEmpty(grdKDSTAFF.EditValue), String.Empty, grdKDSTAFF.EditValue)
                .JAMMASUK = ""
                .JAMKELUAR = ""
                .LATE = CDec(0)
                .OVERTIME = CDec(0)
                .UML = CDec(0)

            End With

            If oFormMode = FORM_MODE.FORM_MODE_ADD Then
                Try
                    fn_Save = oAbsen.InsertData(ds,sKDSite)
                Catch oErr As Exception
                    MsgBox(Statement.ErrorStatement & vbCrLf & oErr.Message, MsgBoxStyle.Exclamation, Me.Text)
                End Try
            ElseIf oFormMode = FORM_MODE.FORM_MODE_EDIT Then
                Try
                    fn_Save = oAbsen.UpdateData(ds)
                Catch oErr As Exception
                    MsgBox(Statement.ErrorStatement & vbCrLf & oErr.Message, MsgBoxStyle.Exclamation, Me.Text)
                End Try
            End If
        Catch oErr As Exception
            MsgBox(Statement.ErrorStatement & vbCrLf & oErr.Message, MsgBoxStyle.Exclamation, Me.Text)
            fn_Save = False
        End Try
    End Function
#End Region
#Region "Command Button"
    Private Sub frmItem_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles MyBase.KeyDown
        Select Case e.KeyCode
            Case Keys.F12
                btnClose_Click()
            Case Keys.F2
                If btnSaveNew.Enabled = True Then
                    btnSaveNew_Click()
                End If
            Case Keys.F3
                If btnSaveClose.Enabled = True Then
                    btnSaveClose_Click()
                End If
        End Select
    End Sub
    Private Sub btnSaveNew_Click() Handles btnSaveNew.ItemClick
        If fn_Validate() = False Then Exit Sub
        If MsgBox(Statement.SaveQuestion, MsgBoxStyle.Exclamation + MsgBoxStyle.YesNo, Me.Text) = MsgBoxResult.No Then Exit Sub
        If fn_Save() = False Then
            MsgBox(Statement.SaveFail, MsgBoxStyle.Exclamation, Me.Text)
        Else
            MsgBox(Statement.SaveSuccess, MsgBoxStyle.Information, Me.Text)
            sStatusSave = "NEW"
            Me.Close()
        End If
    End Sub
    Private Sub btnSaveClose_Click() Handles btnSaveClose.ItemClick
        If fn_Validate() = False Then Exit Sub
        If MsgBox(Statement.SaveQuestion, MsgBoxStyle.Exclamation + MsgBoxStyle.YesNo, Me.Text) = MsgBoxResult.No Then Exit Sub
        If fn_Save() = False Then
            MsgBox(Statement.SaveFail, MsgBoxStyle.Exclamation, Me.Text)
        Else
            MsgBox(Statement.SaveSuccess, MsgBoxStyle.Information, Me.Text)
            Me.Close()
        End If
    End Sub
    Private Sub btnClose_Click() Handles btnClose.ItemClick
        Me.Close()
    End Sub
#End Region
#Region "Lookup / Event"
    Private Sub fn_LoadKDSTAFF()
        Dim oSTAFF As New HumanResource.clsStaffJari
        Try
            grdKDSTAFF.Properties.DataSource = oSTAFF.GetData.Where(Function(x) x.ISACTIVE = True).ToList()
            grdKDSTAFF.Properties.ValueMember = "KDSTAFF"
            grdKDSTAFF.Properties.DisplayMember = "NAME_DISPLAY"
        Catch oErr As Exception
            MsgBox(Statement.ErrorStatement & vbCrLf & oErr.Message, MsgBoxStyle.Exclamation, Me.Text)
        End Try
    End Sub
#End Region
End Class