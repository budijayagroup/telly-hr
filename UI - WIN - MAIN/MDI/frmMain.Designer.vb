﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain
    Inherits DevExpress.XtraEditors.XtraForm

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.barManager = New DevExpress.XtraBars.BarManager(Me.components)
        Me.Bar2 = New DevExpress.XtraBars.Bar()
        Me.mnuFile = New DevExpress.XtraBars.BarSubItem()
        Me.mnuFileLanguage = New DevExpress.XtraBars.BarSubItem()
        Me.mnuFileLanguageIndonesian = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuFileLanguageEnglish = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuFileChangePassword = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuFileLogOut = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuFileExit = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuHumanResource = New DevExpress.XtraBars.BarSubItem()
        Me.mnuHumanResourceStaff = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuHumanResourceAbsent = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuHumanResourcePermitAbsent = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuHumanResourceReport = New DevExpress.XtraBars.BarSubItem()
        Me.mnuHumanResourceReportRekap = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuHumanResourceImportDataStaff = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuSetting = New DevExpress.XtraBars.BarSubItem()
        Me.mnuSettingDatabase = New DevExpress.XtraBars.BarSubItem()
        Me.mnuSettingDatabaseBackup = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuSettingDatabaseRestore = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuSettingDatabaseConnection = New DevExpress.XtraBars.BarSubItem()
        Me.mnuSettingDatabaseConnectionUser = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuSettingUser = New DevExpress.XtraBars.BarSubItem()
        Me.mnuSettingUserUser = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuSettingUserOtority = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem5 = New DevExpress.XtraBars.BarButtonItem()
        Me.Bar3 = New DevExpress.XtraBars.Bar()
        Me.statusKDUSER = New DevExpress.XtraBars.BarStaticItem()
        Me.statusDATE = New DevExpress.XtraBars.BarStaticItem()
        Me.statusLANGUAGE = New DevExpress.XtraBars.BarStaticItem()
        Me.barDockControlTop = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlBottom = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlLeft = New DevExpress.XtraBars.BarDockControl()
        Me.barDockControlRight = New DevExpress.XtraBars.BarDockControl()
        Me.mnuReference = New DevExpress.XtraBars.BarSubItem()
        Me.mnuReferenceVendor = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuReferenceCustomer = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuReferenceWarehouse = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuReferenceCOA = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuReferenceUOM = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuReferenceItem_L1 = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuReferenceItem_L2 = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuReferenceItem_L3 = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuReferenceItem_L4 = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuReferenceItem_L5 = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuReferenceItem_L6 = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuReferenceItem = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuReferenceItemMaterial = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuReferencePaymentType = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuReferencePaymentDetail = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuReferencePackingMethod = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuReferenceColors = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuReferenceCarton = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuReferenceProcess = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuReferenceRate = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuReferenceDepartment = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem1 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem4 = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuPurchasing = New DevExpress.XtraBars.BarSubItem()
        Me.mnuPurchasingPPB = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuPurchasingPPBM = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuPurchasingPurchaseOrder = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuPurchasingPurchaseOrderMaklun = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuPurchasingBPB = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuPurchasingBPBR = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuPurchasingReport = New DevExpress.XtraBars.BarSubItem()
        Me.mnuPurchasingReportPurchaseOrder = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuPurchasingReportPurchaseInvoice = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuPurchasingReportPurchaseReturn = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuSales = New DevExpress.XtraBars.BarSubItem()
        Me.mnuSPO = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuSalesSalesOrder = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuSalesSalesInvoice = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuSalesSalesReturn = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuSalesReport = New DevExpress.XtraBars.BarSubItem()
        Me.mnuSalesReportSPO = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuSalesReportSalesOrder = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuSalesReportSalesInvoice = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuSalesReportSalesReturn = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuAccounting = New DevExpress.XtraBars.BarSubItem()
        Me.mnuAccountingJournal = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuAccountingRecalculate = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuAccountingReport = New DevExpress.XtraBars.BarSubItem()
        Me.mnuAccountingReportLedger = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuAccountingReportBalanceSheet = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuAccountingReportProfitLossStatement = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuPurchasingPurchaseInvoice = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuPurchasingPurchaseReturn = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuInventory = New DevExpress.XtraBars.BarSubItem()
        Me.mnuInventoryReportMonitoringItem = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuInventoryReqMaterial = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuInventoryMPBG = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuInventoryBPBG = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuInventoryProduction = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuInventoryMutation = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuInventoryOpname = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuInventoryReport = New DevExpress.XtraBars.BarSubItem()
        Me.mnuInventoryReportMutation = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuInventoryReportOpname = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuInventoryReportStandardCost = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuFinance = New DevExpress.XtraBars.BarSubItem()
        Me.mnuFinanceCashBank = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuFinanceCashIn = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuFinanceCashOut = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuFinanceReport = New DevExpress.XtraBars.BarSubItem()
        Me.mnuFinanceReportCashBank = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuFinanceReportCashIn = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuFinanceReportCashOut = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuFinanceReportReceiveables = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuFinanceReportPayables = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem2 = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem3 = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuAccountingReportJournal = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuHumanResourceCategory = New DevExpress.XtraBars.BarButtonItem()
        Me.BarButtonItem6 = New DevExpress.XtraBars.BarButtonItem()
        Me.mnuPurchasingPurchasePO = New DevExpress.XtraBars.BarButtonItem()
        Me.xtraTab = New DevExpress.XtraTabbedMdi.XtraTabbedMdiManager(Me.components)
        Me.fileDialogSave = New System.Windows.Forms.SaveFileDialog()
        Me.fileDialogOpen = New System.Windows.Forms.OpenFileDialog()
        CType(Me.barManager,System.ComponentModel.ISupportInitialize).BeginInit
        CType(Me.xtraTab,System.ComponentModel.ISupportInitialize).BeginInit
        Me.SuspendLayout
        '
        'barManager
        '
        Me.barManager.Bars.AddRange(New DevExpress.XtraBars.Bar() {Me.Bar2, Me.Bar3})
        Me.barManager.DockControls.Add(Me.barDockControlTop)
        Me.barManager.DockControls.Add(Me.barDockControlBottom)
        Me.barManager.DockControls.Add(Me.barDockControlLeft)
        Me.barManager.DockControls.Add(Me.barDockControlRight)
        Me.barManager.Form = Me
        Me.barManager.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.mnuFile, Me.mnuFileExit, Me.mnuReference, Me.mnuReferenceCustomer, Me.mnuFileLanguage, Me.mnuFileLanguageIndonesian, Me.mnuFileLanguageEnglish, Me.mnuReferenceCOA, Me.mnuSetting, Me.BarButtonItem1, Me.mnuSettingDatabase, Me.mnuSettingDatabaseBackup, Me.mnuSettingDatabaseRestore, Me.BarButtonItem4, Me.mnuReferenceVendor, Me.mnuReferenceWarehouse, Me.mnuReferenceUOM, Me.mnuReferenceItem_L1, Me.mnuReferenceItem_L2, Me.mnuReferenceItem_L3, Me.mnuReferenceItem_L4, Me.mnuReferenceItem_L5, Me.mnuReferenceItem_L6, Me.mnuReferencePaymentType, Me.mnuReferenceItem, Me.mnuPurchasing, Me.mnuPurchasingPurchaseOrder, Me.mnuPurchasingReport, Me.mnuPurchasingReportPurchaseOrder, Me.mnuSales, Me.mnuSalesSalesOrder, Me.mnuAccounting, Me.mnuAccountingJournal, Me.mnuPurchasingPurchaseInvoice, Me.mnuPurchasingPurchaseReturn, Me.mnuPurchasingReportPurchaseInvoice, Me.mnuPurchasingReportPurchaseReturn, Me.mnuSalesSalesInvoice, Me.mnuSalesSalesReturn, Me.mnuSalesReport, Me.mnuSalesReportSalesOrder, Me.mnuSalesReportSalesInvoice, Me.mnuSalesReportSalesReturn, Me.mnuInventory, Me.mnuInventoryMutation, Me.mnuInventoryOpname, Me.mnuInventoryReport, Me.mnuInventoryReportMutation, Me.mnuInventoryReportOpname, Me.mnuFinance, Me.mnuFinanceCashIn, Me.mnuFinanceCashOut, Me.mnuFinanceReport, Me.mnuFinanceReportCashIn, Me.mnuFinanceReportCashOut, Me.mnuSettingUser, Me.mnuSettingUserUser, Me.mnuSettingUserOtority, Me.BarButtonItem2, Me.BarButtonItem3, Me.mnuSettingDatabaseConnection, Me.mnuSettingDatabaseConnectionUser, Me.mnuAccountingReport, Me.mnuAccountingReportJournal, Me.mnuAccountingReportLedger, Me.mnuAccountingReportBalanceSheet, Me.mnuAccountingReportProfitLossStatement, Me.mnuInventoryReportMonitoringItem, Me.mnuFileLogOut, Me.mnuFileChangePassword, Me.mnuFinanceReportReceiveables, Me.mnuFinanceReportPayables, Me.statusKDUSER, Me.statusDATE, Me.statusLANGUAGE, Me.mnuAccountingRecalculate, Me.BarButtonItem5, Me.mnuFinanceCashBank, Me.mnuFinanceReportCashBank, Me.mnuHumanResource, Me.mnuHumanResourceStaff, Me.mnuHumanResourceAbsent, Me.mnuHumanResourceCategory, Me.mnuReferencePackingMethod, Me.mnuSPO, Me.mnuReferenceColors, Me.BarButtonItem6, Me.mnuPurchasingPPB, Me.mnuReferenceCarton, Me.mnuReferencePaymentDetail, Me.mnuReferenceItemMaterial, Me.mnuPurchasingBPB, Me.mnuPurchasingPurchasePO, Me.mnuInventoryBPBG, Me.mnuInventoryMPBG, Me.mnuPurchasingBPBR, Me.mnuInventoryProduction, Me.mnuInventoryReqMaterial, Me.mnuReferenceProcess, Me.mnuInventoryReportStandardCost, Me.mnuReferenceRate, Me.mnuReferenceDepartment, Me.mnuPurchasingPPBM, Me.mnuPurchasingPurchaseOrderMaklun, Me.mnuHumanResourceReport, Me.mnuHumanResourceReportRekap, Me.mnuHumanResourcePermitAbsent, Me.mnuSalesReportSPO, Me.mnuHumanResourceImportDataStaff})
        Me.barManager.MainMenu = Me.Bar2
        Me.barManager.MaxItemId = 125
        Me.barManager.StatusBar = Me.Bar3
        '
        'Bar2
        '
        Me.Bar2.BarName = "Main menu"
        Me.Bar2.DockCol = 0
        Me.Bar2.DockRow = 0
        Me.Bar2.DockStyle = DevExpress.XtraBars.BarDockStyle.Top
        Me.Bar2.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuFile), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuHumanResource), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.None, false, Me.mnuSetting, false)})
        Me.Bar2.OptionsBar.AllowQuickCustomization = false
        Me.Bar2.OptionsBar.DrawDragBorder = false
        Me.Bar2.OptionsBar.MultiLine = true
        Me.Bar2.OptionsBar.UseWholeRow = true
        Me.Bar2.Text = "Main menu"
        '
        'mnuFile
        '
        Me.mnuFile.Caption = "File"
        Me.mnuFile.Id = 0
        Me.mnuFile.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuFileLanguage), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuFileChangePassword, true), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuFileLogOut, true), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuFileExit)})
        Me.mnuFile.Name = "mnuFile"
        '
        'mnuFileLanguage
        '
        Me.mnuFileLanguage.Caption = "Language"
        Me.mnuFileLanguage.Id = 4
        Me.mnuFileLanguage.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuFileLanguageIndonesian), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuFileLanguageEnglish)})
        Me.mnuFileLanguage.Name = "mnuFileLanguage"
        '
        'mnuFileLanguageIndonesian
        '
        Me.mnuFileLanguageIndonesian.Caption = "Indonesian"
        Me.mnuFileLanguageIndonesian.Id = 5
        Me.mnuFileLanguageIndonesian.Name = "mnuFileLanguageIndonesian"
        '
        'mnuFileLanguageEnglish
        '
        Me.mnuFileLanguageEnglish.Caption = "English"
        Me.mnuFileLanguageEnglish.Id = 6
        Me.mnuFileLanguageEnglish.Name = "mnuFileLanguageEnglish"
        '
        'mnuFileChangePassword
        '
        Me.mnuFileChangePassword.Caption = "Change Password"
        Me.mnuFileChangePassword.Id = 77
        Me.mnuFileChangePassword.Name = "mnuFileChangePassword"
        '
        'mnuFileLogOut
        '
        Me.mnuFileLogOut.Caption = "Log Out"
        Me.mnuFileLogOut.Id = 76
        Me.mnuFileLogOut.Name = "mnuFileLogOut"
        '
        'mnuFileExit
        '
        Me.mnuFileExit.Caption = "Exit"
        Me.mnuFileExit.Id = 1
        Me.mnuFileExit.Name = "mnuFileExit"
        '
        'mnuHumanResource
        '
        Me.mnuHumanResource.Caption = "Human Resource"
        Me.mnuHumanResource.Id = 95
        Me.mnuHumanResource.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuHumanResourceStaff), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuHumanResourceAbsent, true), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuHumanResourcePermitAbsent), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuHumanResourceReport, true), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuHumanResourceImportDataStaff)})
        Me.mnuHumanResource.Name = "mnuHumanResource"
        '
        'mnuHumanResourceStaff
        '
        Me.mnuHumanResourceStaff.Caption = "Finger"
        Me.mnuHumanResourceStaff.Id = 96
        Me.mnuHumanResourceStaff.Name = "mnuHumanResourceStaff"
        '
        'mnuHumanResourceAbsent
        '
        Me.mnuHumanResourceAbsent.Caption = "Absent"
        Me.mnuHumanResourceAbsent.Id = 97
        Me.mnuHumanResourceAbsent.Name = "mnuHumanResourceAbsent"
        '
        'mnuHumanResourcePermitAbsent
        '
        Me.mnuHumanResourcePermitAbsent.Caption = "Absent Permit"
        Me.mnuHumanResourcePermitAbsent.Id = 122
        Me.mnuHumanResourcePermitAbsent.Name = "mnuHumanResourcePermitAbsent"
        '
        'mnuHumanResourceReport
        '
        Me.mnuHumanResourceReport.Caption = "Report"
        Me.mnuHumanResourceReport.Id = 120
        Me.mnuHumanResourceReport.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuHumanResourceReportRekap)})
        Me.mnuHumanResourceReport.Name = "mnuHumanResourceReport"
        '
        'mnuHumanResourceReportRekap
        '
        Me.mnuHumanResourceReportRekap.Caption = "Absent"
        Me.mnuHumanResourceReportRekap.Id = 121
        Me.mnuHumanResourceReportRekap.Name = "mnuHumanResourceReportRekap"
        '
        'mnuHumanResourceImportDataStaff
        '
        Me.mnuHumanResourceImportDataStaff.Caption = "Import Data Staff"
        Me.mnuHumanResourceImportDataStaff.Id = 124
        Me.mnuHumanResourceImportDataStaff.Name = "mnuHumanResourceImportDataStaff"
        '
        'mnuSetting
        '
        Me.mnuSetting.Caption = "Setting"
        Me.mnuSetting.Id = 8
        Me.mnuSetting.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuSettingDatabase), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuSettingUser), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.None, false, Me.BarButtonItem5, false)})
        Me.mnuSetting.Name = "mnuSetting"
        '
        'mnuSettingDatabase
        '
        Me.mnuSettingDatabase.Caption = "Database"
        Me.mnuSettingDatabase.Id = 10
        Me.mnuSettingDatabase.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuSettingDatabaseBackup), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuSettingDatabaseRestore), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuSettingDatabaseConnection)})
        Me.mnuSettingDatabase.Name = "mnuSettingDatabase"
        '
        'mnuSettingDatabaseBackup
        '
        Me.mnuSettingDatabaseBackup.Caption = "Backup"
        Me.mnuSettingDatabaseBackup.Id = 11
        Me.mnuSettingDatabaseBackup.Name = "mnuSettingDatabaseBackup"
        '
        'mnuSettingDatabaseRestore
        '
        Me.mnuSettingDatabaseRestore.Caption = "Restore"
        Me.mnuSettingDatabaseRestore.Id = 12
        Me.mnuSettingDatabaseRestore.Name = "mnuSettingDatabaseRestore"
        '
        'mnuSettingDatabaseConnection
        '
        Me.mnuSettingDatabaseConnection.Caption = "Connection"
        Me.mnuSettingDatabaseConnection.Id = 66
        Me.mnuSettingDatabaseConnection.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuSettingDatabaseConnectionUser)})
        Me.mnuSettingDatabaseConnection.Name = "mnuSettingDatabaseConnection"
        '
        'mnuSettingDatabaseConnectionUser
        '
        Me.mnuSettingDatabaseConnectionUser.Caption = "User"
        Me.mnuSettingDatabaseConnectionUser.Id = 68
        Me.mnuSettingDatabaseConnectionUser.Name = "mnuSettingDatabaseConnectionUser"
        '
        'mnuSettingUser
        '
        Me.mnuSettingUser.Caption = "User"
        Me.mnuSettingUser.Id = 59
        Me.mnuSettingUser.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuSettingUserUser), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuSettingUserOtority)})
        Me.mnuSettingUser.Name = "mnuSettingUser"
        '
        'mnuSettingUserUser
        '
        Me.mnuSettingUserUser.Caption = "User"
        Me.mnuSettingUserUser.Id = 60
        Me.mnuSettingUserUser.Name = "mnuSettingUserUser"
        '
        'mnuSettingUserOtority
        '
        Me.mnuSettingUserOtority.Caption = "Otority"
        Me.mnuSettingUserOtority.Id = 61
        Me.mnuSettingUserOtority.Name = "mnuSettingUserOtority"
        '
        'BarButtonItem5
        '
        Me.BarButtonItem5.Caption = "Open Cash Drawer"
        Me.BarButtonItem5.Id = 92
        Me.BarButtonItem5.Name = "BarButtonItem5"
        '
        'Bar3
        '
        Me.Bar3.BarName = "Status bar"
        Me.Bar3.CanDockStyle = DevExpress.XtraBars.BarCanDockStyle.Bottom
        Me.Bar3.DockCol = 0
        Me.Bar3.DockRow = 0
        Me.Bar3.DockStyle = DevExpress.XtraBars.BarDockStyle.Bottom
        Me.Bar3.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.statusKDUSER), New DevExpress.XtraBars.LinkPersistInfo(Me.statusDATE), New DevExpress.XtraBars.LinkPersistInfo(Me.statusLANGUAGE)})
        Me.Bar3.OptionsBar.AllowQuickCustomization = false
        Me.Bar3.OptionsBar.DrawDragBorder = false
        Me.Bar3.OptionsBar.UseWholeRow = true
        Me.Bar3.Text = "Status bar"
        '
        'statusKDUSER
        '
        Me.statusKDUSER.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Right
        Me.statusKDUSER.Border = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.statusKDUSER.Id = 80
        Me.statusKDUSER.Name = "statusKDUSER"
        Me.statusKDUSER.TextAlignment = System.Drawing.StringAlignment.Near
        '
        'statusDATE
        '
        Me.statusDATE.Border = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.statusDATE.Id = 81
        Me.statusDATE.Name = "statusDATE"
        Me.statusDATE.TextAlignment = System.Drawing.StringAlignment.Near
        '
        'statusLANGUAGE
        '
        Me.statusLANGUAGE.Border = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.statusLANGUAGE.Id = 86
        Me.statusLANGUAGE.Name = "statusLANGUAGE"
        Me.statusLANGUAGE.TextAlignment = System.Drawing.StringAlignment.Near
        '
        'barDockControlTop
        '
        Me.barDockControlTop.CausesValidation = false
        Me.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top
        Me.barDockControlTop.Location = New System.Drawing.Point(0, 0)
        Me.barDockControlTop.Size = New System.Drawing.Size(792, 22)
        '
        'barDockControlBottom
        '
        Me.barDockControlBottom.CausesValidation = false
        Me.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.barDockControlBottom.Location = New System.Drawing.Point(0, 548)
        Me.barDockControlBottom.Size = New System.Drawing.Size(792, 25)
        '
        'barDockControlLeft
        '
        Me.barDockControlLeft.CausesValidation = false
        Me.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left
        Me.barDockControlLeft.Location = New System.Drawing.Point(0, 22)
        Me.barDockControlLeft.Size = New System.Drawing.Size(0, 526)
        '
        'barDockControlRight
        '
        Me.barDockControlRight.CausesValidation = false
        Me.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right
        Me.barDockControlRight.Location = New System.Drawing.Point(792, 22)
        Me.barDockControlRight.Size = New System.Drawing.Size(0, 526)
        '
        'mnuReference
        '
        Me.mnuReference.Caption = "Reference"
        Me.mnuReference.Id = 2
        Me.mnuReference.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuReferenceVendor), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuReferenceCustomer, true), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuReferenceWarehouse, true), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuReferenceCOA, true), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuReferenceUOM, true), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.None, false, Me.mnuReferenceItem_L1, false), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuReferenceItem_L2), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuReferenceItem_L3), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.None, false, Me.mnuReferenceItem_L4, false), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.None, false, Me.mnuReferenceItem_L5, false), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuReferenceItem_L6), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuReferenceItem), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuReferenceItemMaterial), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuReferencePaymentType, true), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuReferencePaymentDetail), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuReferencePackingMethod, true), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuReferenceColors), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuReferenceCarton), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuReferenceProcess), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuReferenceRate), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuReferenceDepartment)})
        Me.mnuReference.Name = "mnuReference"
        '
        'mnuReferenceVendor
        '
        Me.mnuReferenceVendor.Caption = "Vendor"
        Me.mnuReferenceVendor.Id = 16
        Me.mnuReferenceVendor.Name = "mnuReferenceVendor"
        '
        'mnuReferenceCustomer
        '
        Me.mnuReferenceCustomer.Caption = "Customer"
        Me.mnuReferenceCustomer.Id = 3
        Me.mnuReferenceCustomer.Name = "mnuReferenceCustomer"
        '
        'mnuReferenceWarehouse
        '
        Me.mnuReferenceWarehouse.Caption = "Warehouse"
        Me.mnuReferenceWarehouse.Id = 17
        Me.mnuReferenceWarehouse.Name = "mnuReferenceWarehouse"
        '
        'mnuReferenceCOA
        '
        Me.mnuReferenceCOA.Caption = "Chart of Account"
        Me.mnuReferenceCOA.Id = 7
        Me.mnuReferenceCOA.Name = "mnuReferenceCOA"
        '
        'mnuReferenceUOM
        '
        Me.mnuReferenceUOM.Caption = "Unit of Meassure"
        Me.mnuReferenceUOM.Id = 18
        Me.mnuReferenceUOM.Name = "mnuReferenceUOM"
        '
        'mnuReferenceItem_L1
        '
        Me.mnuReferenceItem_L1.Caption = "Name Of Material"
        Me.mnuReferenceItem_L1.Id = 19
        Me.mnuReferenceItem_L1.Name = "mnuReferenceItem_L1"
        '
        'mnuReferenceItem_L2
        '
        Me.mnuReferenceItem_L2.Caption = "Spesification"
        Me.mnuReferenceItem_L2.Id = 20
        Me.mnuReferenceItem_L2.Name = "mnuReferenceItem_L2"
        '
        'mnuReferenceItem_L3
        '
        Me.mnuReferenceItem_L3.Caption = "Item_L3"
        Me.mnuReferenceItem_L3.Id = 21
        Me.mnuReferenceItem_L3.Name = "mnuReferenceItem_L3"
        Me.mnuReferenceItem_L3.Visibility = DevExpress.XtraBars.BarItemVisibility.Never
        '
        'mnuReferenceItem_L4
        '
        Me.mnuReferenceItem_L4.Caption = "Type"
        Me.mnuReferenceItem_L4.Id = 22
        Me.mnuReferenceItem_L4.Name = "mnuReferenceItem_L4"
        '
        'mnuReferenceItem_L5
        '
        Me.mnuReferenceItem_L5.Caption = "Item_L5"
        Me.mnuReferenceItem_L5.Id = 23
        Me.mnuReferenceItem_L5.Name = "mnuReferenceItem_L5"
        '
        'mnuReferenceItem_L6
        '
        Me.mnuReferenceItem_L6.Caption = "Item_L6"
        Me.mnuReferenceItem_L6.Id = 24
        Me.mnuReferenceItem_L6.Name = "mnuReferenceItem_L6"
        '
        'mnuReferenceItem
        '
        Me.mnuReferenceItem.Caption = "Item"
        Me.mnuReferenceItem.Id = 26
        Me.mnuReferenceItem.Name = "mnuReferenceItem"
        '
        'mnuReferenceItemMaterial
        '
        Me.mnuReferenceItemMaterial.Caption = "Item Material"
        Me.mnuReferenceItemMaterial.Id = 106
        Me.mnuReferenceItemMaterial.Name = "mnuReferenceItemMaterial"
        '
        'mnuReferencePaymentType
        '
        Me.mnuReferencePaymentType.Caption = "Payment Type"
        Me.mnuReferencePaymentType.Id = 25
        Me.mnuReferencePaymentType.Name = "mnuReferencePaymentType"
        '
        'mnuReferencePaymentDetail
        '
        Me.mnuReferencePaymentDetail.Caption = "Payment Detail"
        Me.mnuReferencePaymentDetail.Id = 105
        Me.mnuReferencePaymentDetail.Name = "mnuReferencePaymentDetail"
        '
        'mnuReferencePackingMethod
        '
        Me.mnuReferencePackingMethod.Caption = "Packing Method"
        Me.mnuReferencePackingMethod.Id = 99
        Me.mnuReferencePackingMethod.Name = "mnuReferencePackingMethod"
        '
        'mnuReferenceColors
        '
        Me.mnuReferenceColors.Caption = "Colors"
        Me.mnuReferenceColors.Id = 101
        Me.mnuReferenceColors.Name = "mnuReferenceColors"
        '
        'mnuReferenceCarton
        '
        Me.mnuReferenceCarton.Caption = "Carton"
        Me.mnuReferenceCarton.Id = 104
        Me.mnuReferenceCarton.Name = "mnuReferenceCarton"
        '
        'mnuReferenceProcess
        '
        Me.mnuReferenceProcess.Caption = "Process"
        Me.mnuReferenceProcess.Id = 114
        Me.mnuReferenceProcess.Name = "mnuReferenceProcess"
        '
        'mnuReferenceRate
        '
        Me.mnuReferenceRate.Caption = "Rate"
        Me.mnuReferenceRate.Id = 116
        Me.mnuReferenceRate.Name = "mnuReferenceRate"
        '
        'mnuReferenceDepartment
        '
        Me.mnuReferenceDepartment.Caption = "Department"
        Me.mnuReferenceDepartment.Id = 117
        Me.mnuReferenceDepartment.Name = "mnuReferenceDepartment"
        '
        'BarButtonItem1
        '
        Me.BarButtonItem1.Caption = "Syncron"
        Me.BarButtonItem1.Id = 9
        Me.BarButtonItem1.Name = "BarButtonItem1"
        '
        'BarButtonItem4
        '
        Me.BarButtonItem4.Caption = "Update Cloud"
        Me.BarButtonItem4.Id = 13
        Me.BarButtonItem4.Name = "BarButtonItem4"
        '
        'mnuPurchasing
        '
        Me.mnuPurchasing.Caption = "Purchasing"
        Me.mnuPurchasing.Id = 29
        Me.mnuPurchasing.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuPurchasingPPB), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuPurchasingPPBM), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuPurchasingPurchaseOrder, true), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuPurchasingPurchaseOrderMaklun), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuPurchasingBPB, true), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuPurchasingBPBR), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuPurchasingReport, true)})
        Me.mnuPurchasing.Name = "mnuPurchasing"
        '
        'mnuPurchasingPPB
        '
        Me.mnuPurchasingPPB.Caption = "PPB"
        Me.mnuPurchasingPPB.Id = 103
        Me.mnuPurchasingPPB.Name = "mnuPurchasingPPB"
        '
        'mnuPurchasingPPBM
        '
        Me.mnuPurchasingPPBM.Caption = "PPBM"
        Me.mnuPurchasingPPBM.Id = 118
        Me.mnuPurchasingPPBM.Name = "mnuPurchasingPPBM"
        '
        'mnuPurchasingPurchaseOrder
        '
        Me.mnuPurchasingPurchaseOrder.Caption = "Purchase Order"
        Me.mnuPurchasingPurchaseOrder.Id = 30
        Me.mnuPurchasingPurchaseOrder.Name = "mnuPurchasingPurchaseOrder"
        '
        'mnuPurchasingPurchaseOrderMaklun
        '
        Me.mnuPurchasingPurchaseOrderMaklun.Caption = "Purchase Order Maklun"
        Me.mnuPurchasingPurchaseOrderMaklun.Id = 119
        Me.mnuPurchasingPurchaseOrderMaklun.Name = "mnuPurchasingPurchaseOrderMaklun"
        '
        'mnuPurchasingBPB
        '
        Me.mnuPurchasingBPB.Caption = "BPB"
        Me.mnuPurchasingBPB.Id = 107
        Me.mnuPurchasingBPB.Name = "mnuPurchasingBPB"
        '
        'mnuPurchasingBPBR
        '
        Me.mnuPurchasingBPBR.Caption = "BPBR"
        Me.mnuPurchasingBPBR.Id = 111
        Me.mnuPurchasingBPBR.Name = "mnuPurchasingBPBR"
        '
        'mnuPurchasingReport
        '
        Me.mnuPurchasingReport.Caption = "Report"
        Me.mnuPurchasingReport.Id = 31
        Me.mnuPurchasingReport.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuPurchasingReportPurchaseOrder), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuPurchasingReportPurchaseInvoice), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuPurchasingReportPurchaseReturn)})
        Me.mnuPurchasingReport.Name = "mnuPurchasingReport"
        '
        'mnuPurchasingReportPurchaseOrder
        '
        Me.mnuPurchasingReportPurchaseOrder.Caption = "Purchase Order"
        Me.mnuPurchasingReportPurchaseOrder.Id = 32
        Me.mnuPurchasingReportPurchaseOrder.Name = "mnuPurchasingReportPurchaseOrder"
        '
        'mnuPurchasingReportPurchaseInvoice
        '
        Me.mnuPurchasingReportPurchaseInvoice.Caption = "Purchase Invoice"
        Me.mnuPurchasingReportPurchaseInvoice.Id = 39
        Me.mnuPurchasingReportPurchaseInvoice.Name = "mnuPurchasingReportPurchaseInvoice"
        '
        'mnuPurchasingReportPurchaseReturn
        '
        Me.mnuPurchasingReportPurchaseReturn.Caption = "Purchase Return"
        Me.mnuPurchasingReportPurchaseReturn.Id = 40
        Me.mnuPurchasingReportPurchaseReturn.Name = "mnuPurchasingReportPurchaseReturn"
        '
        'mnuSales
        '
        Me.mnuSales.Caption = "Sales"
        Me.mnuSales.Id = 33
        Me.mnuSales.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuSPO), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.None, false, Me.mnuSalesSalesOrder, true), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuSalesSalesInvoice), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuSalesSalesReturn), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuSalesReport, true)})
        Me.mnuSales.Name = "mnuSales"
        '
        'mnuSPO
        '
        Me.mnuSPO.Caption = "Surat Perintah Order"
        Me.mnuSPO.Id = 100
        Me.mnuSPO.Name = "mnuSPO"
        '
        'mnuSalesSalesOrder
        '
        Me.mnuSalesSalesOrder.Caption = "Sales Order"
        Me.mnuSalesSalesOrder.Id = 34
        Me.mnuSalesSalesOrder.Name = "mnuSalesSalesOrder"
        '
        'mnuSalesSalesInvoice
        '
        Me.mnuSalesSalesInvoice.Caption = "Sales Invoice"
        Me.mnuSalesSalesInvoice.Id = 41
        Me.mnuSalesSalesInvoice.Name = "mnuSalesSalesInvoice"
        '
        'mnuSalesSalesReturn
        '
        Me.mnuSalesSalesReturn.Caption = "Sales Return"
        Me.mnuSalesSalesReturn.Id = 42
        Me.mnuSalesSalesReturn.Name = "mnuSalesSalesReturn"
        '
        'mnuSalesReport
        '
        Me.mnuSalesReport.Caption = "Report"
        Me.mnuSalesReport.Id = 43
        Me.mnuSalesReport.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuSalesReportSPO), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.None, false, Me.mnuSalesReportSalesOrder, true), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuSalesReportSalesInvoice), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.None, false, Me.mnuSalesReportSalesReturn, false)})
        Me.mnuSalesReport.Name = "mnuSalesReport"
        '
        'mnuSalesReportSPO
        '
        Me.mnuSalesReportSPO.Caption = "Surat Perintah Order"
        Me.mnuSalesReportSPO.Id = 123
        Me.mnuSalesReportSPO.Name = "mnuSalesReportSPO"
        '
        'mnuSalesReportSalesOrder
        '
        Me.mnuSalesReportSalesOrder.Caption = "Sales Order"
        Me.mnuSalesReportSalesOrder.Id = 44
        Me.mnuSalesReportSalesOrder.Name = "mnuSalesReportSalesOrder"
        '
        'mnuSalesReportSalesInvoice
        '
        Me.mnuSalesReportSalesInvoice.Caption = "Sales Invoice"
        Me.mnuSalesReportSalesInvoice.Id = 45
        Me.mnuSalesReportSalesInvoice.Name = "mnuSalesReportSalesInvoice"
        '
        'mnuSalesReportSalesReturn
        '
        Me.mnuSalesReportSalesReturn.Caption = "Sales Return"
        Me.mnuSalesReportSalesReturn.Id = 46
        Me.mnuSalesReportSalesReturn.Name = "mnuSalesReportSalesReturn"
        '
        'mnuAccounting
        '
        Me.mnuAccounting.Caption = "Accounting"
        Me.mnuAccounting.Id = 35
        Me.mnuAccounting.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuAccountingJournal), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuAccountingRecalculate, true), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuAccountingReport, true)})
        Me.mnuAccounting.Name = "mnuAccounting"
        '
        'mnuAccountingJournal
        '
        Me.mnuAccountingJournal.Caption = "Journal"
        Me.mnuAccountingJournal.Id = 36
        Me.mnuAccountingJournal.Name = "mnuAccountingJournal"
        '
        'mnuAccountingRecalculate
        '
        Me.mnuAccountingRecalculate.Caption = "Recalculate"
        Me.mnuAccountingRecalculate.Id = 90
        Me.mnuAccountingRecalculate.Name = "mnuAccountingRecalculate"
        '
        'mnuAccountingReport
        '
        Me.mnuAccountingReport.Caption = "Report"
        Me.mnuAccountingReport.Id = 70
        Me.mnuAccountingReport.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuAccountingReportLedger, true), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuAccountingReportBalanceSheet), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuAccountingReportProfitLossStatement)})
        Me.mnuAccountingReport.Name = "mnuAccountingReport"
        '
        'mnuAccountingReportLedger
        '
        Me.mnuAccountingReportLedger.Caption = "Ledger"
        Me.mnuAccountingReportLedger.Id = 72
        Me.mnuAccountingReportLedger.Name = "mnuAccountingReportLedger"
        '
        'mnuAccountingReportBalanceSheet
        '
        Me.mnuAccountingReportBalanceSheet.Caption = "Balance Sheet"
        Me.mnuAccountingReportBalanceSheet.Id = 73
        Me.mnuAccountingReportBalanceSheet.Name = "mnuAccountingReportBalanceSheet"
        '
        'mnuAccountingReportProfitLossStatement
        '
        Me.mnuAccountingReportProfitLossStatement.Caption = "Profit Loss Statement"
        Me.mnuAccountingReportProfitLossStatement.Id = 74
        Me.mnuAccountingReportProfitLossStatement.Name = "mnuAccountingReportProfitLossStatement"
        '
        'mnuPurchasingPurchaseInvoice
        '
        Me.mnuPurchasingPurchaseInvoice.Caption = "Purchase Invoice"
        Me.mnuPurchasingPurchaseInvoice.Id = 37
        Me.mnuPurchasingPurchaseInvoice.Name = "mnuPurchasingPurchaseInvoice"
        '
        'mnuPurchasingPurchaseReturn
        '
        Me.mnuPurchasingPurchaseReturn.Caption = "Purchase Return"
        Me.mnuPurchasingPurchaseReturn.Id = 38
        Me.mnuPurchasingPurchaseReturn.Name = "mnuPurchasingPurchaseReturn"
        '
        'mnuInventory
        '
        Me.mnuInventory.Caption = "Inventory"
        Me.mnuInventory.Id = 47
        Me.mnuInventory.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuInventoryReportMonitoringItem), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuInventoryReqMaterial, true), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuInventoryMPBG, true), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuInventoryBPBG), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuInventoryProduction, true), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.None, false, Me.mnuInventoryMutation, true), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.None, false, Me.mnuInventoryOpname, false), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuInventoryReport, true)})
        Me.mnuInventory.Name = "mnuInventory"
        '
        'mnuInventoryReportMonitoringItem
        '
        Me.mnuInventoryReportMonitoringItem.Caption = "Monitoring Item"
        Me.mnuInventoryReportMonitoringItem.Id = 75
        Me.mnuInventoryReportMonitoringItem.Name = "mnuInventoryReportMonitoringItem"
        '
        'mnuInventoryReqMaterial
        '
        Me.mnuInventoryReqMaterial.Caption = "Request Material"
        Me.mnuInventoryReqMaterial.Id = 113
        Me.mnuInventoryReqMaterial.Name = "mnuInventoryReqMaterial"
        '
        'mnuInventoryMPBG
        '
        Me.mnuInventoryMPBG.Caption = "MPBG"
        Me.mnuInventoryMPBG.Id = 110
        Me.mnuInventoryMPBG.Name = "mnuInventoryMPBG"
        '
        'mnuInventoryBPBG
        '
        Me.mnuInventoryBPBG.Caption = "BPBG"
        Me.mnuInventoryBPBG.Id = 109
        Me.mnuInventoryBPBG.Name = "mnuInventoryBPBG"
        '
        'mnuInventoryProduction
        '
        Me.mnuInventoryProduction.Caption = "Production"
        Me.mnuInventoryProduction.Id = 112
        Me.mnuInventoryProduction.Name = "mnuInventoryProduction"
        '
        'mnuInventoryMutation
        '
        Me.mnuInventoryMutation.Caption = "Mutation"
        Me.mnuInventoryMutation.Id = 48
        Me.mnuInventoryMutation.Name = "mnuInventoryMutation"
        '
        'mnuInventoryOpname
        '
        Me.mnuInventoryOpname.Caption = "Opname"
        Me.mnuInventoryOpname.Id = 49
        Me.mnuInventoryOpname.Name = "mnuInventoryOpname"
        '
        'mnuInventoryReport
        '
        Me.mnuInventoryReport.Caption = "Report"
        Me.mnuInventoryReport.Id = 50
        Me.mnuInventoryReport.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.None, false, Me.mnuInventoryReportMutation, false), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.None, false, Me.mnuInventoryReportOpname, false), New DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.None, false, Me.mnuInventoryReportStandardCost, false)})
        Me.mnuInventoryReport.Name = "mnuInventoryReport"
        '
        'mnuInventoryReportMutation
        '
        Me.mnuInventoryReportMutation.Caption = "Mutation"
        Me.mnuInventoryReportMutation.Id = 51
        Me.mnuInventoryReportMutation.Name = "mnuInventoryReportMutation"
        '
        'mnuInventoryReportOpname
        '
        Me.mnuInventoryReportOpname.Caption = "Opname"
        Me.mnuInventoryReportOpname.Id = 52
        Me.mnuInventoryReportOpname.Name = "mnuInventoryReportOpname"
        '
        'mnuInventoryReportStandardCost
        '
        Me.mnuInventoryReportStandardCost.Caption = "Standard Cost"
        Me.mnuInventoryReportStandardCost.Id = 115
        Me.mnuInventoryReportStandardCost.Name = "mnuInventoryReportStandardCost"
        '
        'mnuFinance
        '
        Me.mnuFinance.Caption = "Finance"
        Me.mnuFinance.Id = 53
        Me.mnuFinance.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuFinanceCashBank, true), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuFinanceCashIn, true), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuFinanceCashOut), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuFinanceReport, true)})
        Me.mnuFinance.Name = "mnuFinance"
        '
        'mnuFinanceCashBank
        '
        Me.mnuFinanceCashBank.Caption = "Cash / Bank"
        Me.mnuFinanceCashBank.Id = 93
        Me.mnuFinanceCashBank.Name = "mnuFinanceCashBank"
        '
        'mnuFinanceCashIn
        '
        Me.mnuFinanceCashIn.Caption = "Cash In"
        Me.mnuFinanceCashIn.Id = 54
        Me.mnuFinanceCashIn.Name = "mnuFinanceCashIn"
        '
        'mnuFinanceCashOut
        '
        Me.mnuFinanceCashOut.Caption = "Cash Out"
        Me.mnuFinanceCashOut.Id = 55
        Me.mnuFinanceCashOut.Name = "mnuFinanceCashOut"
        '
        'mnuFinanceReport
        '
        Me.mnuFinanceReport.Caption = "Report"
        Me.mnuFinanceReport.Id = 56
        Me.mnuFinanceReport.LinksPersistInfo.AddRange(New DevExpress.XtraBars.LinkPersistInfo() {New DevExpress.XtraBars.LinkPersistInfo(Me.mnuFinanceReportCashBank, true), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuFinanceReportCashIn, true), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuFinanceReportCashOut), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuFinanceReportReceiveables, true), New DevExpress.XtraBars.LinkPersistInfo(Me.mnuFinanceReportPayables)})
        Me.mnuFinanceReport.Name = "mnuFinanceReport"
        '
        'mnuFinanceReportCashBank
        '
        Me.mnuFinanceReportCashBank.Caption = "Cash / Bank"
        Me.mnuFinanceReportCashBank.Id = 94
        Me.mnuFinanceReportCashBank.Name = "mnuFinanceReportCashBank"
        '
        'mnuFinanceReportCashIn
        '
        Me.mnuFinanceReportCashIn.Caption = "Cash In"
        Me.mnuFinanceReportCashIn.Id = 57
        Me.mnuFinanceReportCashIn.Name = "mnuFinanceReportCashIn"
        '
        'mnuFinanceReportCashOut
        '
        Me.mnuFinanceReportCashOut.Caption = "Cash Out"
        Me.mnuFinanceReportCashOut.Id = 58
        Me.mnuFinanceReportCashOut.Name = "mnuFinanceReportCashOut"
        '
        'mnuFinanceReportReceiveables
        '
        Me.mnuFinanceReportReceiveables.Caption = "Receiveables"
        Me.mnuFinanceReportReceiveables.Id = 78
        Me.mnuFinanceReportReceiveables.Name = "mnuFinanceReportReceiveables"
        '
        'mnuFinanceReportPayables
        '
        Me.mnuFinanceReportPayables.Caption = "Payables"
        Me.mnuFinanceReportPayables.Id = 79
        Me.mnuFinanceReportPayables.Name = "mnuFinanceReportPayables"
        '
        'BarButtonItem2
        '
        Me.BarButtonItem2.Caption = "Export"
        Me.BarButtonItem2.Id = 63
        Me.BarButtonItem2.Name = "BarButtonItem2"
        '
        'BarButtonItem3
        '
        Me.BarButtonItem3.Caption = "User Connection"
        Me.BarButtonItem3.Id = 65
        Me.BarButtonItem3.Name = "BarButtonItem3"
        '
        'mnuAccountingReportJournal
        '
        Me.mnuAccountingReportJournal.Caption = "Journal"
        Me.mnuAccountingReportJournal.Id = 71
        Me.mnuAccountingReportJournal.Name = "mnuAccountingReportJournal"
        '
        'mnuHumanResourceCategory
        '
        Me.mnuHumanResourceCategory.Caption = "Category"
        Me.mnuHumanResourceCategory.Id = 98
        Me.mnuHumanResourceCategory.Name = "mnuHumanResourceCategory"
        '
        'BarButtonItem6
        '
        Me.BarButtonItem6.Caption = "BarButtonItem6"
        Me.BarButtonItem6.Id = 102
        Me.BarButtonItem6.Name = "BarButtonItem6"
        '
        'mnuPurchasingPurchasePO
        '
        Me.mnuPurchasingPurchasePO.Caption = "Purchase PO"
        Me.mnuPurchasingPurchasePO.Id = 108
        Me.mnuPurchasingPurchasePO.Name = "mnuPurchasingPurchasePO"
        '
        'xtraTab
        '
        Me.xtraTab.HeaderLocation = DevExpress.XtraTab.TabHeaderLocation.Bottom
        Me.xtraTab.MdiParent = Me
        '
        'fileDialogSave
        '
        Me.fileDialogSave.Filter = "BAK Files|*.BAK|All Files|*.*"
        '
        'fileDialogOpen
        '
        Me.fileDialogOpen.Filter = "BAK Files|*.BAK|All files|*.*"
        '
        'frmMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6!, 13!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(792, 573)
        Me.Controls.Add(Me.barDockControlLeft)
        Me.Controls.Add(Me.barDockControlRight)
        Me.Controls.Add(Me.barDockControlBottom)
        Me.Controls.Add(Me.barDockControlTop)
        Me.IsMdiContainer = true
        Me.KeyPreview = true
        Me.Name = "frmMain"
        Me.ShowIcon = false
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        CType(Me.barManager,System.ComponentModel.ISupportInitialize).EndInit
        CType(Me.xtraTab,System.ComponentModel.ISupportInitialize).EndInit
        Me.ResumeLayout(false)
        Me.PerformLayout

End Sub
    Friend WithEvents barManager As DevExpress.XtraBars.BarManager
    Friend WithEvents Bar2 As DevExpress.XtraBars.Bar
    Friend WithEvents Bar3 As DevExpress.XtraBars.Bar
    Friend WithEvents barDockControlTop As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlBottom As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlLeft As DevExpress.XtraBars.BarDockControl
    Friend WithEvents barDockControlRight As DevExpress.XtraBars.BarDockControl
    Friend WithEvents mnuFile As DevExpress.XtraBars.BarSubItem
    Friend WithEvents mnuFileExit As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuReference As DevExpress.XtraBars.BarSubItem
    Friend WithEvents mnuReferenceCustomer As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuFileLanguage As DevExpress.XtraBars.BarSubItem
    Friend WithEvents mnuFileLanguageIndonesian As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuFileLanguageEnglish As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents xtraTab As DevExpress.XtraTabbedMdi.XtraTabbedMdiManager
    Friend WithEvents mnuReferenceCOA As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuSetting As DevExpress.XtraBars.BarSubItem
    Friend WithEvents mnuSettingDatabase As DevExpress.XtraBars.BarSubItem
    Friend WithEvents BarButtonItem1 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuSettingDatabaseBackup As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuSettingDatabaseRestore As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem4 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents fileDialogSave As System.Windows.Forms.SaveFileDialog
    Friend WithEvents fileDialogOpen As System.Windows.Forms.OpenFileDialog
    Friend WithEvents mnuReferenceVendor As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuReferenceWarehouse As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuReferenceUOM As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuReferenceItem_L1 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuReferenceItem_L2 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuReferenceItem_L3 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuReferenceItem_L4 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuReferenceItem_L5 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuReferenceItem_L6 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuReferencePaymentType As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuReferenceItem As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuPurchasing As DevExpress.XtraBars.BarSubItem
    Friend WithEvents mnuPurchasingPurchaseOrder As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuPurchasingReport As DevExpress.XtraBars.BarSubItem
    Friend WithEvents mnuPurchasingReportPurchaseOrder As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuSales As DevExpress.XtraBars.BarSubItem
    Friend WithEvents mnuSalesSalesOrder As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuAccounting As DevExpress.XtraBars.BarSubItem
    Friend WithEvents mnuAccountingJournal As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuPurchasingPurchaseInvoice As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuPurchasingPurchaseReturn As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuPurchasingReportPurchaseInvoice As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuPurchasingReportPurchaseReturn As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuSalesSalesInvoice As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuSalesSalesReturn As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuSalesReport As DevExpress.XtraBars.BarSubItem
    Friend WithEvents mnuSalesReportSalesOrder As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuSalesReportSalesInvoice As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuSalesReportSalesReturn As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuInventory As DevExpress.XtraBars.BarSubItem
    Friend WithEvents mnuInventoryMutation As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuInventoryOpname As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuInventoryReport As DevExpress.XtraBars.BarSubItem
    Friend WithEvents mnuInventoryReportMutation As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuInventoryReportOpname As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuFinance As DevExpress.XtraBars.BarSubItem
    Friend WithEvents mnuFinanceCashIn As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuFinanceCashOut As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuFinanceReport As DevExpress.XtraBars.BarSubItem
    Friend WithEvents mnuFinanceReportCashIn As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuFinanceReportCashOut As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuSettingUser As DevExpress.XtraBars.BarSubItem
    Friend WithEvents mnuSettingUserUser As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuSettingUserOtority As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem2 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem3 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuSettingDatabaseConnection As DevExpress.XtraBars.BarSubItem
    Friend WithEvents mnuSettingDatabaseConnectionUser As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuAccountingReport As DevExpress.XtraBars.BarSubItem
    Friend WithEvents mnuAccountingReportJournal As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuAccountingReportLedger As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuAccountingReportBalanceSheet As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuAccountingReportProfitLossStatement As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuInventoryReportMonitoringItem As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuFileLogOut As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuFileChangePassword As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuFinanceReportReceiveables As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuFinanceReportPayables As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents statusKDUSER As DevExpress.XtraBars.BarStaticItem
    Friend WithEvents statusDATE As DevExpress.XtraBars.BarStaticItem
    Friend WithEvents statusLANGUAGE As DevExpress.XtraBars.BarStaticItem
    Friend WithEvents mnuAccountingRecalculate As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem5 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuFinanceCashBank As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuFinanceReportCashBank As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuHumanResource As DevExpress.XtraBars.BarSubItem
    Friend WithEvents mnuHumanResourceStaff As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuHumanResourceAbsent As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuHumanResourceCategory As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuReferencePackingMethod As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuSPO As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuReferenceColors As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents BarButtonItem6 As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuPurchasingPPB As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuReferenceCarton As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuReferencePaymentDetail As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuReferenceItemMaterial As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuPurchasingBPB As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuPurchasingPurchasePO As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuInventoryBPBG As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuInventoryMPBG As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuPurchasingBPBR As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuInventoryProduction As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuInventoryReqMaterial As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuReferenceProcess As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuInventoryReportStandardCost As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuReferenceRate As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuReferenceDepartment As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuPurchasingPPBM As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuPurchasingPurchaseOrderMaklun As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuHumanResourceReport As DevExpress.XtraBars.BarSubItem
    Friend WithEvents mnuHumanResourceReportRekap As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuHumanResourcePermitAbsent As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuSalesReportSPO As DevExpress.XtraBars.BarButtonItem
    Friend WithEvents mnuHumanResourceImportDataStaff As DevExpress.XtraBars.BarButtonItem
End Class
