﻿Imports UI.WIN.MAIN.My.Resources
Imports DataAccess

Imports DevExpress.XtraSplashScreen
Imports System.Threading
Imports DevExpress.XtraWaitForm

Public Class frmMain
     
#Region "Declaration"
    
   
    Private isRestart As Boolean = False
#End Region
#Region "Function"
    Private Sub frmMain_Load(sender As Object, e As System.EventArgs) Handles Me.Load
        

        frmDashboard.MdiParent = Me
        frmDashboard.Show()

        fn_LoadLanguage("id")
        fn_LoadLogin()
        Me.Text = Caption.CodeSite & sKDSite & " - " & sNamaSite
    End Sub
    Private Sub frmMain_FormClosing(sender As Object, e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If (MessageBox.Show(Statement.ExitApplication, Caption.Title, MessageBoxButtons.YesNo, MessageBoxIcon.Question)) = Windows.Forms.DialogResult.Yes Then
            If isRestart = False Then
                Environment.Exit(1)
            Else
                Application.Restart()
            End If
        Else
            e.Cancel = True
        End If
    End Sub
    Private Sub fn_LoadLogin()
        frmLogin.ShowDialog()

        If sUserID = "ABSEN" Then
            Bar2.Visible = False
            For Each iLoop In Me.MdiChildren
                If iLoop.Name = frmAbsen.Name Then
                    iLoop.Activate()
                    Exit Sub
                End If
            Next

            frmAbsen.MdiParent = Me
            frmAbsen.Show()
        Else
            Bar2.Visible = True
        End If


        statusKDUSER.Caption = Caption.User & " : " & sUserID
        statusDATE.Caption = Caption.Tanggal & " : " & Now.ToString("dd/MM/yyyy")
    End Sub
    Private Sub fn_LoadLanguage(ByVal sCulture As String)
        Try
            If sCulture = "id" Then
                'System.Threading.Thread.CurrentThread.CurrentUICulture = System.Globalization.CultureInfo.GetCultureInfo("id-ID")
                System.Threading.Thread.CurrentThread.CurrentUICulture = New System.Globalization.CultureInfo("id-ID")

                statusLANGUAGE.Caption = "Bahasa : Indonesia"
            Else
                System.Threading.Thread.CurrentThread.CurrentUICulture = System.Globalization.CultureInfo.InstalledUICulture

                statusLANGUAGE.Caption = "Language : English"
            End If

            My.Settings.Save()

            For Each iLoop In Me.MdiChildren
                Dim child As ILanguage = TryCast(iLoop, ILanguage)

                If child IsNot Nothing Then
                    child.fn_LoadLanguage()
                End If
            Next
        Catch oErr As Exception

        End Try

        Me.Text = Caption.CodeSite & sKDSite & " - " & sNamaSite
      
        'File
        mnuFileLanguage.Caption = Caption.FileLanguage
        mnuFileLanguageIndonesian.Caption = Caption.FileLanguageIndonesian
        mnuFileLanguageEnglish.Caption = Caption.FileLanguageEnglish
        mnuFileChangePassword.Caption = Caption.FileChangePassword
        mnuFileExit.Caption = Caption.FileExit


        'Reference
        mnuReference.Caption = Caption.Reference
        mnuReferenceVendor.Caption = Caption.ReferenceVendor
        mnuReferenceCustomer.Caption = Caption.ReferenceCustomer
        mnuReferenceWarehouse.Caption = Caption.ReferenceWarehouse
        mnuReferenceCOA.Caption = Caption.ReferenceCOA
        mnuReferenceUOM.Caption = Caption.ReferenceUOM
        mnuReferenceItem_L1.Caption = Caption.ReferenceItem_L1
        mnuReferenceItem_L2.Caption = Caption.ReferenceItem_L2
        mnuReferenceItem_L3.Caption = Caption.ReferenceItem_L3
        mnuReferenceItem_L4.Caption = Caption.ReferenceItem_L4
        mnuReferenceItem_L5.Caption = Caption.ReferenceItem_L5
        mnuReferenceItem_L6.Caption = Caption.ReferenceItem_L6
        mnuReferenceItem.Caption = Caption.ReferenceItem
        mnuReferenceItemMaterial.Caption = Caption.ReferenceItemMaterial
        mnuReferencePaymentType.Caption = Caption.ReferencePaymentType
        mnuReferenceCarton.Caption = Caption.ReferenceCarton
        mnuReferencePaymentDetail.Caption = Caption.ReferencePaymentDetail
        mnuReferencePackingMethod.Caption = Caption.ReferencePackingMethod
        mnuReferenceColors.Caption = Caption.ReferenceColors
        mnuReferenceProcess.Caption = Caption.ReferenceProcess
        mnuReferenceRate.Caption = Caption.ReferenceRate
        mnuReferenceDepartment.Caption = Caption.ReferenceDepartment


        'Purchasing
        mnuPurchasing.Caption = Caption.Purchasing
        mnuPurchasingPurchaseOrder.Caption = Caption.PurchasingPurchaseOrder
        mnuPurchasingPurchaseOrderMaklun.Caption = Caption.PurchasingPurchaseOrderMaklun
        mnuPurchasingPurchaseInvoice.Caption = Caption.PurchasingPurchaseInvoice
        mnuPurchasingPurchaseReturn.Caption = Caption.PurchasingPurchaseReturn
        mnuPurchasingReport.Caption = Caption.PurchasingReport
        mnuPurchasingReportPurchaseOrder.Caption = Caption.PurchasingPurchaseOrder
        mnuPurchasingReportPurchaseInvoice.Caption = Caption.PurchasingPurchaseInvoice
        mnuPurchasingReportPurchaseReturn.Caption = Caption.PurchasingPurchaseReturn
        mnuPurchasingPPB.Caption = Caption.PurchasingPPB
        mnuPurchasingPPBM.Caption = Caption.PurchasingPPBM
        mnuPurchasingBPB.Caption = Caption.PurchasingBPB
        mnuPurchasingPurchasePO.Caption = Caption.PurchasingPurchasePO
        mnuPurchasingBPBR.Caption = Caption.PurchasingBPBR



        'Sales
        mnuSales.Caption = Caption.Sales
        mnuSalesSalesOrder.Caption = Caption.SalesSalesOrder
        mnuSalesSalesInvoice.Caption = Caption.SalesSalesInvoice
        mnuSalesSalesReturn.Caption = Caption.SalesSalesReturn
        mnuSalesReport.Caption = Caption.SalesReport
        mnuSalesReportSalesOrder.Caption = Caption.SalesSalesOrder
        mnuSalesReportSalesInvoice.Caption = Caption.SalesSalesInvoice
        mnuSalesReportSalesReturn.Caption = Caption.SalesSalesReturn
        mnuSPO.Caption = Caption.SPO

        'Inventory
        mnuInventory.Caption = Caption.Inventory
        mnuInventoryMutation.Caption = Caption.InventoryMutation
        mnuInventoryOpname.Caption = Caption.InventoryOpname
        mnuInventoryReport.Caption = Caption.InventoryReport
        mnuInventoryReportMutation.Caption = Caption.InventoryMutation
        mnuInventoryReportOpname.Caption = Caption.InventoryOpname
        mnuInventoryReportStandardCost.Caption = Caption.InventoryStandardCost
        mnuInventoryReportMonitoringItem.Caption = Caption.InventoryReportMonitoringItem
        mnuInventoryMPBG.Caption = Caption.InventoryMPBG
        mnuInventoryBPBG.Caption = Caption.InventoryBPBG
        mnuInventoryReqMaterial.Caption = Caption.InventoryREQMATERIAL
        mnuInventoryProduction.Caption = Caption.InventoryPRODUCTION

        'Finance
        mnuFinance.Caption = Caption.Finance
        mnuFinanceCashIn.Caption = Caption.FinanceCashIn
        mnuFinanceCashOut.Caption = Caption.FinanceCashOut
        mnuFinanceReport.Caption = Caption.FinanceReport
        mnuFinanceReportCashIn.Caption = Caption.FinanceCashIn
        mnuFinanceReportCashOut.Caption = Caption.FinanceCashOut
        mnuFinanceReportReceiveables.Caption = Caption.FinanceReportReceiveables
        mnuFinanceReportPayables.Caption = Caption.FinanceReportPayables

        'Accounting
        mnuAccounting.Caption = Caption.Accounting
        mnuAccountingJournal.Caption = Caption.AccountingJournal
        mnuAccountingReport.Caption = Caption.AccountingReport
        mnuAccountingReportJournal.Caption = Caption.AccountingJournal
        mnuAccountingReportLedger.Caption = Caption.AccountingReportLedger
        mnuAccountingReportBalanceSheet.Caption = Caption.AccountingReportBalanceSheet
        mnuAccountingReportProfitLossStatement.Caption = Caption.AccountingReportProfitLossStatement

        'Setting
        mnuSetting.Caption = Caption.Setting
        mnuSettingDatabase.Caption = Caption.SettingDatabase
        mnuSettingDatabaseBackup.Caption = Caption.SettingDatabaseBackup
        mnuSettingDatabaseRestore.Caption = Caption.SettingDatabaseRestore
        mnuSettingDatabaseConnection.Caption = Caption.SettingDatabaseConnection
        mnuSettingUser.Caption = Caption.SettingUser
        mnuSettingUserUser.Caption = Caption.SettingUserUser
        mnuSettingUserOtority.Caption = Caption.SettingUserOtority
        mnuSettingDatabaseConnectionUser.Caption = Caption.SettingDatabaseConnectionUser
    End Sub
    Private Sub mnuFileLogOut_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuFileLogOut.ItemClick
        fn_LoadLogin()
    End Sub
    Private Sub mnuFileChangePassword_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuFileChangePassword.ItemClick
        frmChangePassword.ShowDialog()
    End Sub
#End Region
#Region "File"
    Private Sub mnuFileLanguageIndonesian_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuFileLanguageIndonesian.ItemClick
        fn_LoadLanguage("id")
    End Sub
    Private Sub mnuFileLanguageEnglish_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuFileLanguageEnglish.ItemClick
        fn_LoadLanguage("en")
    End Sub
    Private Sub mnuFileExit_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuFileExit.ItemClick
        isRestart = False
        Application.Exit()
    End Sub
#End Region




#Region "Human Resource"

    Private Sub mnuHumanResourceStaff_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuHumanResourceStaff.ItemClick
        For Each iLoop In Me.MdiChildren
            If iLoop.Name = frmStaffList.Name Then
                iLoop.Activate()
                Exit Sub
            End If
        Next

        frmStaffList.MdiParent = Me
        frmStaffList.Show()
    End Sub

    Private Sub mnuHumanResourcePermitAbsent_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuHumanResourcePermitAbsent.ItemClick
        For Each iLoop In Me.MdiChildren
            If iLoop.Name = frmPermitAbsenList.Name Then
                iLoop.Activate()
                Exit Sub
            End If
        Next

        frmPermitAbsenList.MdiParent = Me
        frmPermitAbsenList.Show()
    End Sub
    Private Sub mnuHumanResourceAbsent_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuHumanResourceAbsent.ItemClick
        For Each iLoop In Me.MdiChildren
            If iLoop.Name = frmAbsen.Name Then
                iLoop.Activate()
                Exit Sub
            End If
        Next

        'frmAbsen.MdiParent = Me
        frmAbsen.Show()
    End Sub

    Private Sub mnuHumanResourceReportRekap_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuHumanResourceReportRekap.ItemClick
        For Each iLoop In Me.MdiChildren
            If iLoop.Name = frmAbsenLaporanRekap.Name Then
                iLoop.Activate()
                Exit Sub
            End If
        Next

        frmAbsenLaporanRekap.MdiParent = Me
        frmAbsenLaporanRekap.Show()
    End Sub

#End Region
#Region "Setting"
    Private Sub mnuSettingDatabaseBackup_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuSettingDatabaseBackup.ItemClick
        Dim oConnection As New Setting.clsConnectionMain

        Try
            Dim arrMain() As String = Decrypt(My.Computer.Registry.GetValue("HKEY_CURRENT_USER\Software\BUDIJAYAGROUP\MAIN\", "Database", "").ToString()).Split(";")

            For iLoop As Integer = 0 To arrMain.Length - 1
                Dim arrMainResult() As String = arrMain(iLoop).Split("=")

                For xLoop As Integer = 0 To arrMainResult.Length - 1
                    If arrMainResult(xLoop) = "Initial Catalog" Then
                        If fileDialogSave.ShowDialog() = Windows.Forms.DialogResult.Cancel Then

                        Else
                            If fileDialogSave.FileName <> String.Empty Then
                                Try
                                    If oConnection.BackupData(arrMainResult(xLoop + 1), fileDialogSave.FileName) = True Then
                                        MsgBox(Statement.BackupSuccess, MsgBoxStyle.Information, Caption.Title)
                                    Else
                                        MsgBox(Statement.BackupFail, MsgBoxStyle.Exclamation, Caption.Title)
                                    End If
                                Catch oErr As Exception
                                    MsgBox(Statement.BackupFail, MsgBoxStyle.Exclamation, Caption.Title)
                                End Try
                            End If
                        End If
                    End If
                Next
            Next
        Catch oErr As Exception
            MsgBox(Statement.BackupFail, MsgBoxStyle.Exclamation, Caption.Title)
        End Try
    End Sub
    Private Sub mnuSettingDatabaseRestore_ItemClick(ByVal sender As System.Object, ByVal e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuSettingDatabaseRestore.ItemClick
        Dim oConnection As New Setting.clsConnectionMain

        Try
            Dim arrMain() As String = Decrypt(My.Computer.Registry.GetValue("HKEY_CURRENT_USER\Software\BUDIJAYAGROUP\MAIN\", "Database", "").ToString()).Split(";")

            For iLoop As Integer = 0 To arrMain.Length - 1
                Dim arrMainResult() As String = arrMain(iLoop).Split("=")

                For xLoop As Integer = 0 To arrMainResult.Length - 1
                    If arrMainResult(xLoop) = "Initial Catalog" Then
                        If fileDialogOpen.ShowDialog() = Windows.Forms.DialogResult.Cancel Then

                        Else
                            If fileDialogOpen.FileName <> String.Empty Then
                                Try
                                    If oConnection.RestoreData(arrMainResult(xLoop + 1), fileDialogOpen.FileName) = True Then
                                        MsgBox(Statement.RestoreSuccess, MsgBoxStyle.Information, Caption.Title)

                                        isRestart = True
                                        Application.Exit()
                                    Else
                                        MsgBox(Statement.RestoreFail, MsgBoxStyle.Exclamation, Caption.Title)
                                    End If
                                Catch oErr As Exception
                                    MsgBox(Statement.RestoreFail, MsgBoxStyle.Exclamation, Caption.Title)
                                End Try
                            End If
                        End If
                    End If
                Next
            Next
        Catch oErr As Exception
            MsgBox(Statement.RestoreFail, MsgBoxStyle.Exclamation, Caption.Title)
        End Try
    End Sub
    Private Sub mnuSettingDatabaseConnectionUser_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuSettingDatabaseConnectionUser.ItemClick
        frmDatabaseUser.ShowDialog(Me)
    End Sub
    Private Sub mnuSettingUserOtority_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuSettingUserOtority.ItemClick
        For Each iLoop In Me.MdiChildren
            If iLoop.Name = frmOtorityList.Name Then
                iLoop.Activate()
                Exit Sub
            End If
        Next

        frmOtorityList.MdiParent = Me
        frmOtorityList.Show()
    End Sub
    Private Sub mnuSettingUserUser_ItemClick(sender As System.Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuSettingUserUser.ItemClick
        For Each iLoop In Me.MdiChildren
            If iLoop.Name = frmUserList.Name Then
                iLoop.Activate()
                Exit Sub
            End If
        Next

        frmUserList.MdiParent = Me
        frmUserList.Show()
    End Sub

    Private Sub mnuHumanResourceImportDataStaff_ItemClick(sender As Object, e As DevExpress.XtraBars.ItemClickEventArgs) Handles mnuHumanResourceImportDataStaff.ItemClick
       For Each iLoop In Me.MdiChildren
            If iLoop.Name = FrmImportExcel.Name Then
                iLoop.Activate()
                Exit Sub
            End If
        Next

        FrmImportExcel.MdiParent = Me
        FrmImportExcel.Show()
    End Sub

#End Region 
End Class