﻿Namespace Setting
Public Class clsSite
        #Region "Declaratio"
        Public oConnection As Setting.clsConnectionMain = Nothing
        Public sMODUL As String = ""
        Public sREFERENCE As String = ""
        Public sSTATUS As String = ""
        Public sLASTNUMBER As Integer = 0
        Public oError As Setting.clsError = Nothing
#End Region

        Public Sub New(Optional ByVal sConnection As String = "")
            If sConnection = "" Then
                oConnection = New Setting.clsConnectionMain
                oError = New Setting.clsError
            Else
                oConnection = New Setting.clsConnectionMain("TAX")
                oError = New Setting.clsError("TAX")
            End If

            sMODUL = "STAFF"
        End Sub
        Protected Overrides Sub Finalize()
            MyBase.Finalize()
            oConnection = Nothing
            GC.SuppressFinalize(Me)
        End Sub
    Public Function GetSite()  As  M_SITE
            If Not oConnection.GetConnection() Then
                GetSite = Nothing
                Exit Function
            End If

           GetSite = oConnection.db.M_SITEs.FirstOrDefault()
        End Function
End Class
End Namespace 

