﻿
Namespace Setting
   
    Public Class clsImport
         Public oConnection As Setting.clsConnectionMain = Nothing
        Public oError As Setting.clsError = Nothing
        
        
       

        Public sMODUL As String = ""
        Public sREFERENCE As String = ""
        Public sSTATUS As String = ""
        Public sLASTNUMBER As Integer = 0

        Public Sub New(Optional ByVal sConnection As String = "")
            If sConnection = "" Then
                oConnection = New Setting.clsConnectionMain
                oError = New Setting.clsError
            Else
                oConnection = New Setting.clsConnectionMain("TAX")
                oError = New Setting.clsError("TAX")
            End If

            sMODUL = "STAFF"
        End Sub
        Protected Overrides Sub Finalize()
            MyBase.Finalize()
            oConnection = Nothing
            GC.SuppressFinalize(Me)
        End Sub
        Public Function GetStructureHeader() As M_STAFF
            If Not oConnection.GetConnection() Then
                GetStructureHeader = Nothing
            End If
            GetStructureHeader = New M_STAFF
        End Function
        Public Function GetData() As List(Of M_STAFF)
            If Not oConnection.GetConnection() Then
                GetData = Nothing
                Exit Function
            End If
            GetData = oConnection.db.M_STAFFs.OrderBy(Function(x) x.NAME_DISPLAY).ToList()
        End Function
        Public Function GetData(ByVal sKDSTAFF As String) As M_STAFF
            If Not oConnection.GetConnection() Then
                GetData = Nothing
                Exit Function
            End If
            GetData = oConnection.db.M_STAFFs.FirstOrDefault(Function(x) x.KDSTAFF = sKDSTAFF)
        End Function

        Public Function GetStructureDetail() As M_STAFF
            If Not oConnection.GetConnection() Then
                GetStructureDetail = Nothing
            End If
            GetStructureDetail = New M_STAFF
        End Function
        Public Function GetStructureDetailList() As List(Of M_STAFF)
            If Not oConnection.GetConnection() Then
                GetStructureDetailList = Nothing
            End If
            GetStructureDetailList = New List(Of M_STAFF)
        End Function

        Public Function GetDataSync() As List(Of M_STAFF)
            If Not oConnection.GetConnection() Then
                GetDataSync = Nothing
                Exit Function
            End If
            GetDataSync = oConnection.db.M_STAFFs.OrderBy(Function(x) x.NAME_DISPLAY).ToList()
        End Function
        Public Function IsExist(ByVal sNAME_DISPLAY As String) As Boolean
            If Not oConnection.GetConnection() Then
                IsExist = False
                Exit Function
            End If

            Dim ds = oConnection.db.M_STAFFs.FirstOrDefault(Function(x) x.NAME_DISPLAY = sNAME_DISPLAY)

            If ds IsNot Nothing Then
                IsExist = True
            Else
                IsExist = False
            End If
        End Function

       
       
        Public Function ImportData_Staff(ByVal entity As List(Of M_STAFF)) As Boolean
            Try
                If Not oConnection.GetConnection() Then
                    ImportData_Staff = False
                    Exit Function
                End If

                sSTATUS = "INSERT"

                For Each iLoop In entity
                     Try
                         Dim ds = oConnection.db.M_STAFFs.FirstOrDefault(Function(x) x.KDSTAFF = iLoop.KDSTAFF)
                         oConnection.db.M_STAFFs.DeleteOnSubmit(ds)
                     Catch ex As Exception

                     End Try
              
                Next iloop

                
                Try   
                 oConnection.db.M_STAFFs.InsertAllOnSubmit(Entity)

                Catch ex As Exception
                    oError.InsertData(sMODUL, sSTATUS, ex.ToString, sREFERENCE)
                    Throw ex
                End Try
                Try
                    oConnection.db.SubmitChanges()
                Catch ex As Exception
                    oError.InsertData(sMODUL, sSTATUS, ex.ToString, sREFERENCE)
                    Throw ex
                End Try

                ImportData_Staff = True
            Catch ex As Exception
                ImportData_Staff = False
                oError.InsertData(sMODUL, sSTATUS, ex.ToString, sREFERENCE)
                Throw ex
            End Try
        End Function

        Public Function ImportData_StaffJari(ByVal entity As List(Of M_STAFF_JARI)) As Boolean
            Try
                If Not oConnection.GetConnection() Then
                    ImportData_StaffJari = False
                    Exit Function
                End If


                sSTATUS = "INSERT"

                For Each iLoop In entity
                     Try

                           Dim ds = oConnection.db.M_STAFF_JARIs.FirstOrDefault(Function(x) x.KDSTAFF = iLoop.KDSTAFF)
                     
                            Try
                              ds.NAME_DISPLAY = iLoop.NAME_DISPLAY 

                            Catch ex As Exception
                                oError.InsertData(sMODUL, sSTATUS, ex.ToString, sREFERENCE)
                                Throw ex
                            End Try
                            Try
                                oConnection.db.SubmitChanges()
                            Catch ex As Exception
                                oError.InsertData(sMODUL, sSTATUS, ex.ToString, sREFERENCE)
                            Throw ex
                            End Try


                    Catch ex As Exception
                               Try
                   
                                  oConnection.db.M_STAFF_JARIs.InsertAllOnSubmit(entity)
                              Catch ex_1 As Exception
                                   oError.InsertData(sMODUL, sSTATUS, ex_1.ToString, sREFERENCE)
                              Throw ex_1
                              End Try
                              Try
                                oConnection.db.SubmitChanges()
                              Catch ex_2 As Exception
                                oError.InsertData(sMODUL, sSTATUS, ex_2.ToString, sREFERENCE)
                              Throw ex
                              End Try


                    End try
                   
                   
                 
                Next iLoop
               

                ImportData_StaffJari = True
            Catch ex As Exception
                ImportData_StaffJari = False
                oError.InsertData(sMODUL, sSTATUS, ex.ToString, sREFERENCE)
                Throw ex
            End Try
        End Function



        
        Public Function DeleteData(ByVal sKDSTAFF As String) As Boolean
            Try
                If Not oConnection.GetConnection() Then
                    DeleteData = False
                    Exit Function
                End If

                sREFERENCE = sKDSTAFF
                sSTATUS = "DELETE"

                Dim ds = oConnection.db.M_STAFFs.FirstOrDefault(Function(x) x.KDSTAFF = sKDSTAFF)

                Try
                    oConnection.db.M_STAFFs.DeleteOnSubmit(ds)
                Catch ex As Exception
                    oError.InsertData(sMODUL, sSTATUS, ex.ToString, sREFERENCE)
                    Throw ex
                End Try
                Try
                    oConnection.db.SubmitChanges()
                Catch ex As Exception
                    oError.InsertData(sMODUL, sSTATUS, ex.ToString, sREFERENCE)
                    Throw ex
                End Try

                DeleteData = True
            Catch ex As Exception
                DeleteData = False
                oError.InsertData(sMODUL, sSTATUS, ex.ToString, sREFERENCE)
                Throw ex
            End Try
        End Function
    End Class
End Namespace