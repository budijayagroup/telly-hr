﻿Imports System.Text

Module clsGlobals
      
    Public Enum FORM_MODE
        FORM_MODE_VIEW = 0
        FORM_MODE_ADD = 1
        FORM_MODE_EDIT = 2
    End Enum
    Public Function AutoNumber(ByVal sKDCOUNTER As String, ByVal sLASTNUMBER As Integer, ByVal sDATE As DateTime) As String
        Dim sMonth As String = String.Empty

        Select Case Month(sDATE)
            Case 1
                sMonth = "A"
            Case 2
                sMonth = "B"
            Case 3
                sMonth = "C"
            Case 4
                sMonth = "D"
            Case 5
                sMonth = "E"
            Case 6
                sMonth = "F"
            Case 7
                sMonth = "G"
            Case 8
                sMonth = "H"
            Case 9
                sMonth = "I"
            Case 10
                sMonth = "J"
            Case 11
                sMonth = "K"
            Case 12
                sMonth = "L"
        End Select

        If sLASTNUMBER < 10 Then
            AutoNumber = sKDCOUNTER & Year(sDATE) & sMonth & "00000" & sLASTNUMBER.ToString
        ElseIf sLASTNUMBER < 100 Then
            AutoNumber = sKDCOUNTER & Year(sDATE) & sMonth & "0000" & sLASTNUMBER.ToString
        ElseIf sLASTNUMBER < 1000 Then
            AutoNumber = sKDCOUNTER & Year(sDATE) & sMonth & "000" & sLASTNUMBER.ToString
        ElseIf sLASTNUMBER < 10000 Then
            AutoNumber = sKDCOUNTER & Year(sDATE) & sMonth & "00" & sLASTNUMBER.ToString
        ElseIf sLASTNUMBER < 100000 Then
            AutoNumber = sKDCOUNTER & Year(sDATE) & sMonth & "0" & sLASTNUMBER.ToString
        Else
            AutoNumber = sKDCOUNTER & Year(sDATE) & sMonth & sLASTNUMBER.ToString
        End If
    End Function

    Public Function AutoNumberYear(ByVal sKDCOUNTER As String, ByVal sLASTNUMBER As Integer, ByVal sDATE As DateTime) As String

        If sLASTNUMBER < 10 Then
            AutoNumberYear = sKDCOUNTER & Year(sDATE) & "00000" & sLASTNUMBER.ToString
        ElseIf sLASTNUMBER < 100 Then
            AutoNumberYear = sKDCOUNTER & Year(sDATE) & "0000" & sLASTNUMBER.ToString
        ElseIf sLASTNUMBER < 1000 Then
            AutoNumberYear = sKDCOUNTER & Year(sDATE) & "000" & sLASTNUMBER.ToString
        ElseIf sLASTNUMBER < 10000 Then
            AutoNumberYear = sKDCOUNTER & Year(sDATE) & "00" & sLASTNUMBER.ToString
        ElseIf sLASTNUMBER < 100000 Then
            AutoNumberYear = sKDCOUNTER & Year(sDATE) & "0" & sLASTNUMBER.ToString
        Else
            AutoNumberYear = sKDCOUNTER & Year(sDATE) & sLASTNUMBER.ToString
        End If
    End Function

    Public Function AutoNumberSI(ByVal sKDCOUNTER As String, ByVal sLASTNUMBER As Integer, ByVal sDATE As DateTime) As String

        If sLASTNUMBER < 10 Then
            AutoNumberSI = "AMD-" & sDATE.ToString("yy") & Month(sDATE).ToString().PadLeft(2, "0") & "/" & "000" & sLASTNUMBER.ToString
        ElseIf sLASTNUMBER < 100 Then
            AutoNumberSI = "AMD-" & sDATE.ToString("yy") & Month(sDATE).ToString().PadLeft(2, "0") & "/" & "00" & sLASTNUMBER.ToString
        ElseIf sLASTNUMBER < 1000 Then
            AutoNumberSI = "AMD-" & sDATE.ToString("yy") & Month(sDATE).ToString().PadLeft(2, "0") & "/" & "0" & sLASTNUMBER.ToString
        Else
            AutoNumberSI = "AMD-" & sDATE.ToString("yy") & Month(sDATE).ToString().PadLeft(2, "0") & "/" & sLASTNUMBER.ToString
        End If
    End Function

    Public Function AutoNumberSPO(ByVal sKDCOUNTER As String, ByVal sLASTNUMBER As Integer, ByVal sINISIAL As String) As String

        If sLASTNUMBER < 1000 Then
            AutoNumberSPO = sKDCOUNTER & sINISIAL & "000" & sLASTNUMBER.ToString
        ElseIf sLASTNUMBER < 10000 Then
            AutoNumberSPO = sKDCOUNTER & sINISIAL & "00" & sLASTNUMBER.ToString
        ElseIf sLASTNUMBER < 100000 Then
            AutoNumberSPO = sKDCOUNTER & sINISIAL & "0" & sLASTNUMBER.ToString
        Else
            AutoNumberSPO = sKDCOUNTER & sINISIAL & sLASTNUMBER.ToString
        End If
    End Function

    Public Function AutoNumberPO(ByVal sKDCOUNTER As String, ByVal sLASTNUMBER As Integer, ByVal sDATE As DateTime) As String
        Dim sMonth As String = String.Empty

        Select Case Month(sDATE)
            Case 1
                sMonth = "I"
            Case 2
                sMonth = "II"
            Case 3
                sMonth = "III"
            Case 4
                sMonth = "IV"
            Case 5
                sMonth = "V"
            Case 6
                sMonth = "VI"
            Case 7
                sMonth = "VII"
            Case 8
                sMonth = "VIII"
            Case 9
                sMonth = "IX"
            Case 10
                sMonth = "X"
            Case 11
                sMonth = "XI"
            Case 12
                sMonth = "XII"
        End Select

        If sLASTNUMBER < 10 Then
            AutoNumberPO = sKDCOUNTER & "/" & "00000" & sLASTNUMBER.ToString & "/" & sMonth & "/" & Year(sDATE)
        ElseIf sLASTNUMBER < 100 Then
            AutoNumberPO = sKDCOUNTER & "/" & "0000" & sLASTNUMBER.ToString & "/" & sMonth & "/" & Year(sDATE)
        ElseIf sLASTNUMBER < 1000 Then
            AutoNumberPO = sKDCOUNTER & "/" & "000" & sLASTNUMBER.ToString & "/" & sMonth & "/" & Year(sDATE)
        ElseIf sLASTNUMBER < 10000 Then
            AutoNumberPO = sKDCOUNTER & "/" & "00" & sLASTNUMBER.ToString & "/" & sMonth & "/" & Year(sDATE)
        ElseIf sLASTNUMBER < 100000 Then
            AutoNumberPO = sKDCOUNTER & "/" & "0" & sLASTNUMBER.ToString & "/" & sMonth & "/" & Year(sDATE)
        Else
            AutoNumberPO = sKDCOUNTER & "/" & sLASTNUMBER.ToString & "/" & sMonth & "/" & Year(sDATE)
        End If
    End Function

    Public Function AutoNumberCASH(ByVal sKDCOUNTER As String, ByVal sLASTNUMBER As Integer, ByVal sDATE As DateTime, ByVal sCURRENCY As String) As String
        Dim sMonth As String = String.Empty

        Select Case Month(sDATE)
            Case 1
                sMonth = "I"
            Case 2
                sMonth = "II"
            Case 3
                sMonth = "III"
            Case 4
                sMonth = "IV"
            Case 5
                sMonth = "V"
            Case 6
                sMonth = "VI"
            Case 7
                sMonth = "VII"
            Case 8
                sMonth = "VIII"
            Case 9
                sMonth = "IX"
            Case 10
                sMonth = "X"
            Case 11
                sMonth = "XI"
            Case 12
                sMonth = "XII"
        End Select

        If sLASTNUMBER < 10 Then
            AutoNumberCASH = "00000" & sLASTNUMBER.ToString & "/" & sCURRENCY & "/" & sMonth & "/" & sDATE.ToString("yy")
        ElseIf sLASTNUMBER < 100 Then
            AutoNumberCASH = "0000" & sLASTNUMBER.ToString & "/" & sCURRENCY & "/" & sMonth & "/" & sDATE.ToString("yy")
        ElseIf sLASTNUMBER < 1000 Then
            AutoNumberCASH = "000" & sLASTNUMBER.ToString & "/" & sCURRENCY & "/" & sMonth & "/" & sDATE.ToString("yy")
        ElseIf sLASTNUMBER < 10000 Then
            AutoNumberCASH = "00" & sLASTNUMBER.ToString & "/" & sCURRENCY & "/" & sMonth & "/" & sDATE.ToString("yy")
        ElseIf sLASTNUMBER < 100000 Then
            AutoNumberCASH = "0" & sLASTNUMBER.ToString & "/" & sCURRENCY & "/" & sMonth & "/" & sDATE.ToString("yy")
        Else
            AutoNumberCASH = sLASTNUMBER.ToString & "/" & sCURRENCY & "/" & sMonth & "/" & sDATE.ToString("yy")
        End If
    End Function

    Public Function AutoNumberCASHIN(ByVal sKDCOUNTER As String, ByVal sLASTNUMBER As Integer, ByVal sDATE As DateTime, ByVal sCURRENCY As String) As String

        If sLASTNUMBER < 10 Then
            AutoNumberCASHIN = sKDCOUNTER & sDATE.ToString("yy") & "/" & sCURRENCY & "/" & "00000" & sLASTNUMBER.ToString
        ElseIf sLASTNUMBER < 100 Then
            AutoNumberCASHIN = sKDCOUNTER & sDATE.ToString("yy") & "/" & sCURRENCY & "/" & "0000" & sLASTNUMBER.ToString
        ElseIf sLASTNUMBER < 1000 Then
            AutoNumberCASHIN = sKDCOUNTER & sDATE.ToString("yy") & "/" & sCURRENCY & "/" & "000" & sLASTNUMBER.ToString
        ElseIf sLASTNUMBER < 10000 Then
            AutoNumberCASHIN = sKDCOUNTER & sDATE.ToString("yy") & "/" & sCURRENCY & "/" & "00" & sLASTNUMBER.ToString
        ElseIf sLASTNUMBER < 100000 Then
            AutoNumberCASHIN = sKDCOUNTER & sDATE.ToString("yy") & "/" & sCURRENCY & "/" & "0" & sLASTNUMBER.ToString
        Else
            AutoNumberCASHIN = sKDCOUNTER & sDATE.ToString("yy") & "/" & sCURRENCY & "/" & sLASTNUMBER.ToString
        End If
    End Function


    Public Function AutoNumberPPBM(ByVal sKDCOUNTER As String, ByVal sLASTNUMBER As Integer, ByVal sDATE As DateTime) As String
        Dim sMonth As String = String.Empty

        Select Case Month(sDATE)
            Case 1
                sMonth = "I"
            Case 2
                sMonth = "II"
            Case 3
                sMonth = "III"
            Case 4
                sMonth = "IV"
            Case 5
                sMonth = "V"
            Case 6
                sMonth = "VI"
            Case 7
                sMonth = "VII"
            Case 8
                sMonth = "VIII"
            Case 9
                sMonth = "IX"
            Case 10
                sMonth = "X"
            Case 11
                sMonth = "XI"
            Case 12
                sMonth = "XII"
        End Select

        If sLASTNUMBER < 10 Then
            AutoNumberPPBM = "00000" & sLASTNUMBER.ToString & "/" & sMonth & "/" & sKDCOUNTER & "/" & Year(sDATE)
        ElseIf sLASTNUMBER < 100 Then
            AutoNumberPPBM = "0000" & sLASTNUMBER.ToString & "/" & sMonth & "/" & sKDCOUNTER & "/" & Year(sDATE)
        ElseIf sLASTNUMBER < 1000 Then
            AutoNumberPPBM = "000" & sLASTNUMBER.ToString & "/" & sMonth & "/" & sKDCOUNTER & "/" & Year(sDATE)
        ElseIf sLASTNUMBER < 10000 Then
            AutoNumberPPBM = "00" & sLASTNUMBER.ToString & "/" & sMonth & "/" & sKDCOUNTER & "/" & Year(sDATE)
        ElseIf sLASTNUMBER < 100000 Then
            AutoNumberPPBM = "0" & sLASTNUMBER.ToString & "/" & sMonth & "/" & sKDCOUNTER & "/" & Year(sDATE)
        Else
            AutoNumberPPBM = sLASTNUMBER.ToString & "/" & sMonth & "/" & sKDCOUNTER & "/" & Year(sDATE)
        End If
    End Function

    Public Function AutoNumberCode(ByVal LastNumber As Integer) As String
        If LastNumber < 10 Then
            AutoNumberCode = "000000000" & LastNumber.ToString
        ElseIf LastNumber < 100 Then
            AutoNumberCode = "00000000" & LastNumber.ToString
        ElseIf LastNumber < 1000 Then
            AutoNumberCode = "0000000" & LastNumber.ToString
        ElseIf LastNumber < 10000 Then
            AutoNumberCode = "000000" & LastNumber.ToString
        ElseIf LastNumber < 100000 Then
            AutoNumberCode = "00000" & LastNumber.ToString
        ElseIf LastNumber < 1000000 Then
            AutoNumberCode = "0000" & LastNumber.ToString
        ElseIf LastNumber < 10000000 Then
            AutoNumberCode = "000" & LastNumber.ToString
        ElseIf LastNumber < 100000000 Then
            AutoNumberCode = "00" & LastNumber.ToString
        ElseIf LastNumber < 100000000 Then
            AutoNumberCode = "0" & LastNumber.ToString
        Else
            AutoNumberCode = LastNumber.ToString
        End If
    End Function

    

#Region "Encrypt / Decrypt"
    Private Const INT_lens As Integer = 1
    Public str As StringBuilder
    Public searchStr As String
    Public b As Integer = 6
    Public p() As Integer = {2, 4, 7, 9, 3, INT_lens}
    Public i As Integer
    Public j As Integer
    Public k As Integer
    Public c As Integer
    Public lens As Integer

    Public Function Encrypt(ByVal inputstr As String) As String
        str = New StringBuilder(inputstr)
        lens = str.Length
        While (lens < b) OrElse (lens Mod b)
            str.Append(" ")
            lens += INT_lens
        End While
        For i As Integer = 0 To ((lens / b) - INT_lens)
            For j As Integer = 0 To (b - INT_lens)
                k = p(j) + 100
                c = (6 * i + j)
                str.Replace(str.Chars(c), Chr(Asc(str.Chars(c)) + k), c, INT_lens)
            Next
        Next
        Return str.ToString
        str = Nothing
    End Function
    Public Function Decrypt(ByVal inputstr As String) As String

        str = New StringBuilder(inputstr)
        lens = str.Length
        While (lens < b) OrElse (lens Mod b)
            str.Append(" ")
            lens += INT_lens
        End While

        For i As Integer = 0 To ((lens / b) - INT_lens)
            For j As Integer = 0 To (b - INT_lens)
                k = p(j) + 100
                c = (6 * i + j)
                str.Replace(str.Chars(c), Chr(Asc(str.Chars(c)) - k), c, INT_lens)
            Next
        Next
        Return str.ToString
        str = Nothing
    End Function
#End Region
End Module

