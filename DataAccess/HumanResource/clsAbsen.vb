﻿Namespace HumanResource
    Public Class clsAbsen
        Public oConnection As Setting.clsConnectionMain = Nothing
        Public oError As Setting.clsError = Nothing
        Public sMODUL As String = ""
        Public sREFERENCE As String = ""
        Public sSTATUS As String = ""
        Public sLASTNUMBER As Integer = 0

        Public oCounter As Setting.clsCounter = Nothing

        Public Sub New()
            oConnection = New Setting.clsConnectionMain
            oError = New Setting.clsError
            sMODUL = "ABSEN"

            oCounter = New Setting.clsCounter
        End Sub
        Protected Overrides Sub Finalize()
            MyBase.Finalize()
            oConnection = Nothing
            GC.SuppressFinalize(Me)
        End Sub
        Public Function GetStructureHeader() As H_ABSEN
            If Not oConnection.GetConnection Then
                GetStructureHeader = Nothing
            End If
            GetStructureHeader = New H_ABSEN
        End Function
        Public Function GetData() As List(Of H_ABSEN)
            If Not oConnection.GetConnection Then
                GetData = Nothing
                Exit Function
            End If
            GetData = oConnection.db.H_ABSENs.OrderByDescending(Function(x) x.KDABSEN).ToList()
        End Function
        Public Function GetData(ByVal Parameter As String) As List(Of H_ABSEN)
            If Not oConnection.GetConnection Then
                GetData = Nothing
                Exit Function
            End If
            GetData = oConnection.db.H_ABSENs.Where(Function(x) x.KDSTAFF = Parameter).ToList()
        End Function

        Public Function GetData2(ByVal sKDABSEN As String) As H_ABSEN
            If Not oConnection.GetConnection() Then
                GetData2 = Nothing
                Exit Function
            End If

            GetData2 = oConnection.db.H_ABSENs.FirstOrDefault(Function(x) x.KDABSEN = sKDABSEN)

        End Function

        Public Function IsExist(ByVal sDATE As Date, ByVal sSTAFF As String) As Boolean
            If Not oConnection.GetConnection() Then
                IsExist = False
                Exit Function
            End If

            Dim ds = oConnection.db.H_ABSENs.FirstOrDefault(Function(x) x.DATE = sDATE.ToString("dd/MM/yyyy") And x.KDSTAFF = sSTAFF)

            If ds IsNot Nothing Then
                IsExist = True
            Else
                IsExist = False
            End If
        End Function

        Public Function InsertData(ByVal entity As H_ABSEN,ByVal sKDSite As String) As Boolean
            Try
                If Not oConnection.GetConnection Then
                    InsertData = False
                    Exit Function
                End If

                'Generate Auto Number
                Try
                    sLASTNUMBER = oCounter.GetLastNumber(sMODUL, entity.DATE)
                    If sLASTNUMBER = 0 Then
                        Try
                            oCounter.InsertData(sMODUL, entity.DATE)
                            sLASTNUMBER = oCounter.GetLastNumber(sMODUL, entity.DATE)
                        Catch ex As Exception
                            sLASTNUMBER = 0
                        End Try
                    End If

                    entity.KDABSEN =  sKDSite  & AutoNumber(sMODUL, sLASTNUMBER + 1, entity.DATE)
                Catch ex As Exception
                    oError.InsertData("ABSEN", "INSERTDATA", ex.ToString, "")
                    Throw ex
                End Try
                Try
                    oCounter.UpdateData(sMODUL, sLASTNUMBER + 1, Month(entity.DATE), Year(entity.DATE))
                Catch ex As Exception
                    oError.InsertData("ABSEN", "INSERTDATA", ex.ToString, "")
                    Throw ex
                End Try
                'End Generate

                Try
                    oConnection.db.H_ABSENs.InsertOnSubmit(entity)
                Catch ex As Exception
                    oError.InsertData("ABSEN", "INSERTDATA", ex.ToString, "")
                    Throw ex
                End Try
                Try
                    oConnection.db.SubmitChanges()
                Catch ex As Exception
                    oError.InsertData("ABSEN", "INSERTDATA", ex.ToString, "")
                    Throw ex
                End Try

                InsertData = True
            Catch ex As Exception
                InsertData = False
                oError.InsertData("ABSEN", "INSERTDATA", ex.ToString, "")
                Throw ex
            End Try
        End Function
        Public Function UpdateData(ByVal entity As H_ABSEN) As Boolean
            Try
                If Not oConnection.GetConnection Then
                    UpdateData = False
                    Exit Function
                End If

                Dim ds = oConnection.db.H_ABSENs.FirstOrDefault(Function(x) x.KDABSEN = entity.KDABSEN)

                Try
                    oConnection.db.H_ABSENs.DeleteOnSubmit(ds)
                    oConnection.db.H_ABSENs.InsertOnSubmit(entity)
                Catch ex As Exception
                    oError.InsertData("ABSEN", "UPDATEDATA", ex.ToString, "")
                    Throw ex
                End Try
                Try
                    oConnection.db.SubmitChanges()
                Catch ex As Exception
                    oError.InsertData("ABSEN", "UPDATEDATA", ex.ToString, "")
                    Throw ex
                End Try

                UpdateData = True
            Catch ex As Exception
                UpdateData = False
                oError.InsertData("ABSEN", "UPDATEDATA", ex.ToString, "")
                Throw ex
            End Try
        End Function
        Public Function DeleteData(ByVal Parameter As String) As Boolean
            Try
                If Not oConnection.GetConnection Then
                    DeleteData = False
                    Exit Function
                End If

                Dim ds = oConnection.db.H_ABSENs.FirstOrDefault(Function(x) x.KDABSEN = Parameter)

                Try
                    oConnection.db.H_ABSENs.DeleteOnSubmit(ds)
                Catch ex As Exception
                    oError.InsertData("ABSEN", "DELETEDATA", ex.ToString, "")
                    Throw ex
                End Try
                Try
                    oConnection.db.SubmitChanges()
                Catch ex As Exception
                    oError.InsertData("ABSEN", "DELETEDATA", ex.ToString, "")
                    Throw ex
                End Try

                DeleteData = True
            Catch ex As Exception
                DeleteData = False
                oError.InsertData("ABSEN", "DELETEDATA", ex.ToString, "")
                Throw ex
            End Try
        End Function
    End Class
End Namespace